import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { MenuItem } from './models/menu-item';
import { UserService } from './services/user.service';
import { User } from './models/user';
import { RouteService } from './services/route.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'app';
  menu: MenuItem[];
  user: User;
  @ViewChild('toolbarContent', { read: ElementRef, static: false }) toolbarContent: ElementRef;
  constructor(
    private userService: UserService,
    private previousRouteService: RouteService
  ) {
    this.previousRouteService.init();
    const token = localStorage.getItem('token');
    if (token && token !== 'undefined') {
      this.menuUser();
    } else {
      localStorage.removeItem('token');
      this.menu = [];
      this.menu.push(new MenuItem({ name: 'Inicio', link: '/', icon: 'home' }));
      this.menu.push(new MenuItem({ name: 'Registrarme', link: '/registro', icon: 'user-plus' }));
      this.menu.push(new MenuItem({ name: 'Entrar', link: '/login', icon: 'sign-in-alt'}));
    }
    this.userService.eventLogin.subscribe(message => {
      if (message === 'LOGIN') {
        this.menuUser();
      }
    });
  }

  ngOnInit() {
    
  }
  onActivate(event: any){
    this.menu.forEach(item => {
      let path = location.pathname.replace("actividades_culturales/","");
      if (item.name === 'Entrar' && path !== "/" && path !== "/login" && path !== "/logout") {
        item.queryParams = { next: path };
      }
    });
  }

  menuUser() {
    this.menu = [];
    this.menu.push(new MenuItem({ name: 'Inicio', link: '/', icon: 'home' }));
    this.userService.getActive().subscribe((user: User) => {
      this.user = user;
      const perfilMenu = [
        new MenuItem({ name: 'Mi Perfil', link: '/perfil', icon: 'user' })
      ];
      switch (this.user.student_type) {
        case 'admin':
          this.menu.push(new MenuItem({ name: 'Administrador', link: '/admin', icon: 'cogs'}));
          break;
        case 'talent':
          const talentMenu = [
            new MenuItem({ name: 'Panel', link: '/admin', icon: 'cogs' }),
            new MenuItem({ name: 'Talento', link: '/admin/talento-aragones', icon: 'archive ' })
          ];
          this.menu.push(new MenuItem({ name: 'Administrador', link: '/admin', icon: 'cogs', children: talentMenu }));
          break;
        case 'socialservice':
          break;
        default:
          this.menu.push(new MenuItem({ name: 'Mis Talleres', link: '/mis-talleres', icon: 'projector', type: 'acfa' }));
          // perfilMenu.push();
          break;
      }
      this.menu.push(new MenuItem({ name: this.user.name, link: '/perfil', icon: 'user-circle', children: perfilMenu }));
      this.menu.push(new MenuItem({ name: 'Cerrar Sesión', link: '/logout', icon: 'sign-out-alt' }));

    });
  }

  onScrollBody($event) {
    if (this.toolbarContent.nativeElement !== undefined) {
      const range = 400;
      const stopOpacity = 0.596;
      let calc = $event.target.firstElementChild.scrollTop <= range ?
      Math.abs(($event.target.firstElementChild.scrollTop - range) / ($event.target.firstElementChild.scrollTop + range)) : stopOpacity;
      if (calc > 1) {
        calc = 1;
      } else if (calc < stopOpacity) {
        calc = stopOpacity;
      }
      this.toolbarContent.nativeElement.style.background = 'rgba(245,245,245,' + calc + ')';
    }
  }
}
