import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isLogin: boolean;
  errorMessage: string;
  loadLogin: boolean;
  next: string;
  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.loadLogin = false;
  }

  ngOnInit() {
    this.route.queryParams.subscribe( query => {
      this.createFormLogin();
      this.next = query.next || '/';
      if (localStorage.getItem('token')) {
        this.router.navigate([this.next]);
      }
    });
  }

  private createFormLogin() {
    this.loginForm = new FormGroup({
      user: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  public onSubmit() {
    this.loadLogin = true;
    this.isLogin = undefined;
    this.userService.login(this.loginForm.value.user, this.loginForm.value.password).subscribe(response => {
        this.router.navigate([this.next]);
      }, response => {
        this.errorMessage = response.error.message;
        this.isLogin = false;
        this.loadLogin = false;
    });
  }

}
