import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardUnamComponent } from './card-unam.component';

describe('CardUnamComponent', () => {
  let component: CardUnamComponent;
  let fixture: ComponentFixture<CardUnamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardUnamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardUnamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
