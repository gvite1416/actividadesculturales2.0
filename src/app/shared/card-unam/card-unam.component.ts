import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-unam',
  templateUrl: './card-unam.component.html',
  styleUrls: ['./card-unam.component.scss']
})
export class CardUnamComponent implements OnInit {
  @Input() icon: string;
  @Input() typeStudent: string;
  @Input() url: string;
  @Input() textUrl: string;
  @Input() color: string;
  constructor() { }

  ngOnInit() {
  }

}
