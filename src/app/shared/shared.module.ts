import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogContentComponent } from './dialog-content/dialog-content.component';
import { CardUnamComponent } from './card-unam/card-unam.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DialogContentComponent,
    CardUnamComponent
  ],
  exports: [
    CardUnamComponent
  ],
  entryComponents: [
  ]
})
export class SharedModule { }
