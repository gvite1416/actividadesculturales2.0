import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserWorkshopsRoutingModule } from './user-workshops-routing.module';
import { UserWorkshopsComponent } from './user-workshops.component';
import { UserWorkshopComponent } from './user-workshop/user-workshop.component';
import { PipesModule } from '../pipes/pipes.module';
import { MaterialModule } from '../material/material.module';
import { DialogsModule } from '../dialogs/dialogs.module';
import { UserFilesComponent } from './user-files/user-files.component';
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  imports: [
    CommonModule,
    UserWorkshopsRoutingModule,
    PipesModule,
    MaterialModule,
    DialogsModule,
    FileUploadModule
  ],
  declarations: [UserWorkshopsComponent, UserWorkshopComponent, UserFilesComponent]
})
export class UserWorkshopsModule { }
