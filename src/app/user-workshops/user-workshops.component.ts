import { Component, OnInit } from '@angular/core';
import { WorkshopPeriodService } from '../services/workshop-period.service';
import { Inscription } from '../models/inscription';

@Component({
  selector: 'app-user-workshops',
  templateUrl: './user-workshops.component.html',
  styleUrls: ['./user-workshops.component.scss']
})
export class UserWorkshopsComponent implements OnInit {
  inscriptions: Inscription[];
  constructor(
    private workshopPeriodService: WorkshopPeriodService
  ) {
    this.inscriptions = [];
  }

  ngOnInit() {
    this.workshopPeriodService.getAllByUser().subscribe(inscriptions => {
      this.inscriptions = inscriptions;
    });
  }

  uploadFiles(){
    
  }

}
