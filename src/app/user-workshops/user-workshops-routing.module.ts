import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserWorkshopsComponent } from './user-workshops.component';
import { UserWorkshopComponent } from './user-workshop/user-workshop.component';
import { UserFilesComponent } from './user-files/user-files.component';

const routes: Routes = [
  {
    path: '',
    component: UserWorkshopsComponent
  },
  {
    path: 'archivos',
    component: UserFilesComponent
  },
  {
    path: ':id',
    component: UserWorkshopComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserWorkshopsRoutingModule { }
