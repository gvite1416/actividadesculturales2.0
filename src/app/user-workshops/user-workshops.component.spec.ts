import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserWorkshopsComponent } from './user-workshops.component';

describe('UserWorkshopsComponent', () => {
  let component: UserWorkshopsComponent;
  let fixture: ComponentFixture<UserWorkshopsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserWorkshopsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserWorkshopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
