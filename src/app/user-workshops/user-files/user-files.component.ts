import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Period } from '../../models/period';
import { RequirementUser } from '../../models/requirement-user';
import { GLOBALS } from '../../services/globals';
import { InscriptionService } from '../../services/inscription.service';
import { PeriodService } from '../../services/period.service';

@Component({
  selector: 'app-user-files',
  templateUrl: './user-files.component.html',
  styleUrls: ['./user-files.component.scss']
})
export class UserFilesComponent implements OnInit {
  token: string = localStorage.getItem('token');
  period: Period;
  requirementsPeriod: any[];
  requirementsWorkshop: any[];
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/requirement',
    itemAlias: 'filereq',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderArray: FileUploader[];
  uploaderPeriod: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/requirement',
    itemAlias: 'filereq',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  requirementsPeriodFiles: any[];
  requirementsWorkshopFiles: any[];
  allFilesPeriod: boolean;
  constructor(private periodService: PeriodService, private inscriptionService: InscriptionService) {
    this.requirementsPeriod = [];
    this.requirementsWorkshop = [];
    this.requirementsWorkshopFiles = [];
    this.requirementsPeriodFiles = [];
    this.allFilesPeriod = false;
  }

  ngOnInit() {
    this.getRequirements();
    
    this.uploaderPeriod.onBuildItemForm = (fileItem: any, form: any) => {
      this.requirementsPeriodFiles.forEach(element => {
        if(element.file_id == fileItem.file.name + fileItem.file.rawFile.lastModified + fileItem.file.size){
          if(element.user_period_requirement_id != "")
            form.append("user_period_requirement_id", element.user_period_requirement_id);
          form.append("period_requirement_id", element.period_requirement_id);
        }
      });
    };
    this.uploaderPeriod.onCompleteAll = () => {
        this.getRequirements();
    };
  }
  uploadFilesPeriod() {
    this.uploaderPeriod.uploadAll();
  }
  uploadFilesWorkshop(index) {
    this.uploaderArray[index].uploadAll();
  }
  onFilePeriodSelected($e, requirement){
    let hasFile = false;
    this.requirementsPeriodFiles.forEach(element => {
      if(element.user_period_requirement_id == requirement.user_period_requirement_id &&
        element.period_requirement_id == requirement.period_requirement_id){
        element.file_id = $e[0].name + $e[0].lastModified + $e[0].size
        hasFile = true;
      }
    });
    if(!hasFile){
      this.requirementsPeriodFiles.push({
        user_period_requirement_id : requirement.user_period_requirement_id,
        period_requirement_id : requirement.period_requirement_id,
        file_id : $e[0].name + $e[0].lastModified + $e[0].size
      });
    }
  }
  onFileWorkshopSelected($e, requirement){
    let hasFile = false;
    this.requirementsWorkshopFiles.forEach(element => {
      if(element.user_period_requirement_id == requirement.user_period_requirement_id &&
        element.inscription_id == requirement.inscription_id &&
        element.period_requirement_id == requirement.period_requirement_id){
        element.file_id = $e[0].name + $e[0].lastModified + $e[0].size
        hasFile = true;
      }
    });
    if(!hasFile){
      this.requirementsWorkshopFiles.push({
        user_period_requirement_id : requirement.user_period_requirement_id,
        inscription_id : requirement.inscription_id,
        period_requirement_id : requirement.period_requirement_id,
        file_id : $e[0].name + $e[0].lastModified + $e[0].size
      });
    }
  }

  private getRequirements() {
    this.periodService.getUserPeriodRequirements().subscribe(requirements => {
      if (requirements.success) {
        this.period = new Period(requirements.period);
        this.requirementsPeriod = requirements.requirementsPeriod;
        this.requirementsWorkshop = requirements.requirementsWorkshop;
        this.uploaderArray = [];
        this.requirementsWorkshop.forEach((r) => {
          let uploader = new FileUploader({
            url: GLOBALS.apiUrl + 'upload/requirement',
            itemAlias: 'filereq',
            authToken: `Bearer ${localStorage.getItem('token')}`
          })
          
          uploader.onBuildItemForm = (fileItem: any, form: any) => {
            this.requirementsWorkshopFiles.forEach(element => {
              if(element.file_id == fileItem.file.name + fileItem.file.rawFile.lastModified + fileItem.file.size){
                if(element.user_period_requirement_id != "")
                  form.append("user_period_requirement_id", element.user_period_requirement_id);
                form.append("period_requirement_id", element.period_requirement_id);
                form.append("inscription_id", element.inscription_id);
              }
            });
          };
          uploader.onCompleteAll = () => {
            this.inscriptionService.filesUploaded(r.inscription_id).subscribe((resp) => {
              this.getRequirements();
            });
          };
          this.uploaderArray.push(uploader);
        });
        this.allFilesPeriod = true;
        this.requirementsPeriod.forEach((r) => {
          if(!r.user_period_requirement_id){
            this.allFilesPeriod = false;
          }
        });
      }
    });
  }

}
