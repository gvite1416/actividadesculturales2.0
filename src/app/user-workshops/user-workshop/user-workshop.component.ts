import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Inscription } from '../../models/inscription';
import { WorkshopPeriod } from '../../models/workshop-period';
import { Workshop } from '../../models/workshop';
import { GLOBALS } from '../../services/globals';
import { User } from '../../models/user';
import { Period } from '../../models/period';
import { InscriptionService } from '../../services/inscription.service';
import { Requirement } from '../../models/requirement';
import { MatDialog } from '@angular/material';
import { UploadRequirementDialogComponent } from '../../dialogs/upload-requirement-dialog/upload-requirement-dialog.component';
import { RequirementUser } from '../../models/requirement-user';

@Component({
  selector: 'app-user-workshop',
  templateUrl: './user-workshop.component.html',
  styleUrls: ['./user-workshop.component.scss']
})
export class UserWorkshopComponent implements OnInit {
  inscription: Inscription;
  workshop_period: WorkshopPeriod;
  user: User;
  urlPdf: string;
  urlPdfCard: string;
  urlCaptureline: string;
  storageUrl: string = GLOBALS.storageUrl;
  requirementsUser: RequirementUser[];
  constructor(
    private inscriptionService: InscriptionService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.inscription = new Inscription();
    this.workshop_period = new WorkshopPeriod();
    this.workshop_period.workshop = new Workshop();
    this.workshop_period.period = new Period();
    this.user = new User();
    this.urlPdf = '';
    this.urlPdfCard = '';
    this.requirementsUser = [];
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.inscriptionService.getByUser(params['id']).subscribe(response => {
        this.inscription = response.inscription;
        this.workshop_period = response.workshop_period;
        this.user = response.user;
        this.urlPdf = GLOBALS.apiUrl + 'inscription/inscribe-pdf/' + params['id'] + '?token=' + localStorage.getItem('token');
        this.urlPdfCard = GLOBALS.apiUrl + 'users/profile-id-card/' +
          params['id'] + '?token=' + localStorage.getItem('token');
        this.urlCaptureline = GLOBALS.storageFiles + 'captureline-inscription/' +
          params['id'] + '?token=' + localStorage.getItem('token');
        this.workshop_period.period.requirements.forEach((req) => {
          this.requirementsUser.push(new RequirementUser({
            name: req.name
          }));
        });
      });
    });
  }

  uploadFile(requirement: Requirement) {
    const dialogRef = this.dialog.open(UploadRequirementDialogComponent, {
      height: '300px',
      width: '600px',
      data: {requirement: requirement},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      if (result !== undefined) {
      }
    });
  }

}
