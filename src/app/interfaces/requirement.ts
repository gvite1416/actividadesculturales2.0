export interface IRequirement {
    id: number;
    name: string;
    has_file: boolean;
    role_id: number;
    upload_type: string;
}
