export interface IUser {
    id: number;
    name: string;
    lastname: string;
    surname: string;
    email: string;
    phone: number;
    photo: string;
    gender: string;
    qr: string;
    celphone: number;
    is_verified: number;
    is_verified_dept: number;
}
