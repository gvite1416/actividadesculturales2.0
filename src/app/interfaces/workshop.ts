export interface IWorkshop {
    id: number;
    name: string;
    slug: string;
    objective: string;
    requirement: string;
    information: string;
    image: string;
    image_url: string;
    one_more_time: boolean;
}
