export interface ISlider {
    id: number;
    title: string;
    img_desktop: string;
    img_mobile: string;
    img_desktop_url: string;
    img_mobile_url: string;
    link: string;
    order: number;
}
