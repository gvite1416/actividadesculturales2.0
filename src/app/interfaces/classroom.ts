export interface IClassroom {
    id: number;
    name: string;
    quota: number;
}
