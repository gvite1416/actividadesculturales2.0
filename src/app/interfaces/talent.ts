export interface ITalent {
    id: number;
    name: string;
    career_id: number;
    semester: number;
    number_id: string;
    email: string;
    phone: string;
    arts: string;
    art_activity: string;
    band: string;
    gender: string;
    members: string;
    duration: number;
    celphone: string;
    equipment: string;
    scenography: string;
    audio_video: string;
    illumination: string;
    assembly_duration: number;
    deassembly_duration: number;
}
