export interface IDataEmployee {
    id: number;
    campus_id: number;
    workshift: string;
    area: string;
}
