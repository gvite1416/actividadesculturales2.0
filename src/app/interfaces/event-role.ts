import { Role } from '../models/role';

export interface IEventRole {
    role: Role;
    role_id: number;
    limit: number;
    price: number;
    event_id: number;
}
