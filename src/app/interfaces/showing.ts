import * as moment from 'moment';
export interface IShowing {
    id: number;
    start: moment.Moment;
    finish: moment.Moment;
}
