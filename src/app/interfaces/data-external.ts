export interface IDataExternal {
    id:  number;
    address: string;
    occupation_id: number;
}
