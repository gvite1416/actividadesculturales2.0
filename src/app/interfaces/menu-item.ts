export interface IMenuItem {
    name: string;
    link: string;
    queryParams: object;
    icon: string;
    type: string;
}
