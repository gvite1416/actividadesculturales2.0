import { Role } from '../models/role';

export interface IWorkshopPeriodRole {
    role: Role;
    role_id: number;
    limit: number;
    price: number;
    workshop_period_id: number;
}
