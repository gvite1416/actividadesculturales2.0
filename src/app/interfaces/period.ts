import * as moment from 'moment';
export interface IPeriod {
    id: number;
    name: string;
    start: moment.Moment;
    finish: moment.Moment;
    inscription_start: moment.Moment;
    inscription_finish: moment.Moment;
}
