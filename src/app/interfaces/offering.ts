export interface IOffering {
    id: number;
    title: string;
    description: string;
    image: string;
    number: number;
    event_id: number;
}
