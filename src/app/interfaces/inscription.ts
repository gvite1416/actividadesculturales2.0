import * as moment from 'moment';
export interface IInscription {
    id: number;
    workshop_period_id: number;
    user_id: number;
    folio: number;
    ticket_folio: string;
    ticket_date: moment.Moment;
    quota: number;
    extra: number;
    scholarship: number;
    created_at: moment.Moment;
    qr: string;
}
