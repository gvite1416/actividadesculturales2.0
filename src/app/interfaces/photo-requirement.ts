export interface IPhotoRequirement {
    id: number;
    title: string;
    checked: boolean;
}
