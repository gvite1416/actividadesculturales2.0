export interface ICareer {
    id: number;
    name: string;
    code: string;
}
