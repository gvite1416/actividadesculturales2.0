export interface IEventCategory {
    id: number;
    name: string;
    slug: string;
}
