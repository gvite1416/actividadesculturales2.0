export interface IDataStudent {
    id: number;
    year: number;
    career_id: number;
    campus_id: number;
    semester: number;
    system: string;
}
