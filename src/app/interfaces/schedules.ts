import * as moment from 'moment';
export interface ISchedules {
    id: number;
    workshop_period_id: number;
    day: number;
    start: moment.Moment;
    finish: moment.Moment;
}
