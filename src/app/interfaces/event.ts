import * as moment from 'moment';
export interface IEventAC {
    id: number;
    title: string;
    slug: string;
    image: string;
    image_url: string;
    thumbnail: string;
    thumbnail_url: string;
    description: string;
    all_campus: boolean;
    inscription_start: moment.Moment;
    inscription_finish: moment.Moment;
    quota: number;
    event_category_id: number;
    classroom_id: number;
}
