export interface IWorkshopPeriod {
    id: number;
    period_id: number;
    workshop_id: number;
    teacher_id: number;
    classroom_id: number;
    quota: number;
    group: string;
    notes: string;
    all_campus: boolean;
}
