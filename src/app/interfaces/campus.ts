export interface ICampus {
    id: number;
    name: string;
    code: string;
}
