import * as moment from 'moment';
export interface ICaptureline {
    id: number;
    pathname: string;
    inscription_id: number;
    agreement: string;
    reference: string;
    date_valid: moment.Moment;
    value: number;
    img_url: string;
}
