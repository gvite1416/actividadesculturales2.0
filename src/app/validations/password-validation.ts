import {ValidatorFn, AbstractControl} from '@angular/forms';
export function matchPasswordValidator(AC: AbstractControl): ValidatorFn {
    const password = AC.get('password').value; // to get value in input tag
    const confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    return (control: AbstractControl): {[key: string]: any} => {
        const forbidden = password === confirmPassword;
        return forbidden ? {'confirmPassword': {value: control.value}} : null;
    };
}
