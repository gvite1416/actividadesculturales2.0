import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkshopsComponent } from './workshops.component';
import { WorkshopComponent } from './workshop/workshop.component';

const routes: Routes = [
  {
    path: '',
    component: WorkshopsComponent
  },
  {
    path: ':slug',
    component: WorkshopComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkshopsRoutingModule { }
