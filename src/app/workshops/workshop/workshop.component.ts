import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkshopService } from '../../services/workshop.service';
import { Workshop } from '../../models/workshop';
import { GLOBALS } from '../../services/globals';
import { WorkshopPeriod } from '../../models/workshop-period';
import { MatDialog, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { CountdownComponent } from 'ngx-countdown';
import { PeriodService } from '../../services/period.service';
import { Period } from '../../models/period';
import * as moment from 'moment';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { CollegeDegreeDialogComponent } from '../../dialogs/college-degree-dialog/college-degree-dialog.component';
@Component({
  selector: 'app-workshop',
  templateUrl: './workshop.component.html',
  styleUrls: ['./workshop.component.scss']
})
export class WorkshopComponent implements OnInit {
  workshop: Workshop;
  storageUrl: string = GLOBALS.storageUrl;
  disableAllBtn: boolean;
  myStatus: number;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  configCountdown: any;
  currentPeriod: Period;
  user: User;
  @ViewChild('counter', { static: false }) counter: CountdownComponent;
  constructor(
    private route: ActivatedRoute,
    private workshopService: WorkshopService,
    private snackBar: MatSnackBar,
    private router: Router,
    private periodService: PeriodService,
    private userService: UserService,
    public dialog: MatDialog,
  ) {
    this.workshop = new Workshop();
    this.disableAllBtn = false;
    this.myStatus = -1;
    this.configCountdown = {
      demand: true,
      leftTime: 0,
      template: '$!d!:$!h!:$!m!:$!s!'
    };
  }

  ngOnInit() {
    this.userService.getActive().subscribe(user => {
      this.user = user;
      this.periodService.getCurrent().subscribe(period => {
        if (period) {
          this.currentPeriod = period;
          const now = moment();
          this.configCountdown.leftTime = period.inscription_start.diff(now, 'seconds');
          if (this.configCountdown.leftTime > 0) {
            setTimeout(() => {
              this.counter.begin();
            }, 0);
          }
        }
      });
    });
    this.route.params.subscribe(params => {
      this.workshopService.getBySlugCurrentPeriod(params['slug']).subscribe(workshop => {
        this.workshop = workshop;
      });
    });
  }
  validateCollegeDegree(workshop_period: WorkshopPeriod){
    if(this.user.role.slug === 'student' && this.user.data_student.campus.college_degree_option && this.user.data_student.career.college_degree_option){
      const dialogRef = this.dialog.open(CollegeDegreeDialogComponent, {
        width: '500px'
      });
      dialogRef.afterClosed().subscribe(response => {
        this.inscribe(workshop_period, response);
      });
    } else {
      this.inscribe(workshop_period, false);
    }
  }
  inscribe(workshop_period: WorkshopPeriod, collegeDegreeOption: boolean) {
    this.disableAllBtn = true;
    this.workshopService.inscribe(workshop_period.id, collegeDegreeOption).subscribe(response => {
      if ( response.success ) {
        this.snackBar.open('Se ha inscrito al taller correctamente', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/mis-talleres/' + response.id]);
      } else {
        this.snackBar.open('Ocurrió un error: ' + response.message, '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
      this.disableAllBtn = false;
    });
  }

  onFinished() {
    window.location.reload();
  }

}
