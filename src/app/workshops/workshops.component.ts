import { Component, OnInit } from '@angular/core';
import { WorkshopService } from '../services/workshop.service';
import { Workshop } from '../models/workshop';
import { GLOBALS } from '../services/globals';
@Component({
  selector: 'app-workshops',
  templateUrl: './workshops.component.html',
  styleUrls: ['./workshops.component.scss']
})
export class WorkshopsComponent implements OnInit {
  workshops: Workshop[];
  storageUrl: string = GLOBALS.storageUrl;
  constructor(private workshopService: WorkshopService) {
    this.workshops = [];
  }

  ngOnInit() {
    this.workshopService.getAllByCurrentPeriod().subscribe(workshops => {
      this.workshops = workshops;
      this.workshops.forEach(workshop => {
        if (workshop.workshop_period) {
        }
      });
    });
  }

}
