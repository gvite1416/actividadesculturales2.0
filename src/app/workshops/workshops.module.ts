import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkshopsRoutingModule } from './workshops-routing.module';
import { WorkshopsComponent } from './workshops.component';
import { WorkshopComponent } from './workshop/workshop.component';
import { MaterialModule } from '../material/material.module';
import { PipesModule } from '../pipes/pipes.module';
import { CountdownModule } from 'ngx-countdown';
import { DialogsModule } from '../dialogs/dialogs.module';

@NgModule({
  imports: [
    CommonModule,
    WorkshopsRoutingModule,
    MaterialModule,
    PipesModule,
    CountdownModule,
    DialogsModule
  ],
  declarations: [
    WorkshopsComponent,
    WorkshopComponent
  ]
})
export class WorkshopsModule { }
