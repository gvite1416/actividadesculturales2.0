import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'registro',
    loadChildren: 'app/register/register.module#RegisterModule'
  },
  {
    path: 'login',
    loadChildren: 'app/login/login.module#LoginModule'
  },
  {
    path: 'logout',
    loadChildren: 'app/logout/logout.module#LogoutModule'
  },
  {
    path: 'talleres',
    loadChildren: 'app/workshops/workshops.module#WorkshopsModule'
  },
  {
    path: 'eventos',
    loadChildren: 'app/events/events.module#EventsModule'
  },
  {
    path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule'
  },
  {
    path: 'ayuda',
    loadChildren: 'app/help/help.module#HelpModule'
  },
  {
    path: 'perfil',
    loadChildren: 'app/profile/profile.module#ProfileModule'
  },
  {
    path: 'mis-talleres',
    loadChildren: 'app/user-workshops/user-workshops.module#UserWorkshopsModule'
  },
  {
    path: 'contra',
    loadChildren: 'app/password/password.module#PasswordModule'
  },
  {
    path: 'talento-aragones',
    loadChildren: 'app/talent/talent.module#TalentModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const RoutableComponents = [
  HomeComponent
];
