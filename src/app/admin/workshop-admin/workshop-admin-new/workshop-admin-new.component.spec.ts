import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopAdminNewComponent } from './workshop-admin-new.component';

describe('WorkshopAdminNewComponent', () => {
  let component: WorkshopAdminNewComponent;
  let fixture: ComponentFixture<WorkshopAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
