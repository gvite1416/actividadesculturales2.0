import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Workshop } from '../../../models/workshop';
import { WorkshopService } from '../../../services/workshop.service';
import { MatSnackBar, MatSnackBarVerticalPosition, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { GLOBALS } from '../../../services/globals';
import {  FileUploader } from 'ng2-file-upload';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';
import { ImageFile } from '../../../models/image-file';
@Component({
  selector: 'app-workshop-admin-new',
  templateUrl: './workshop-admin-new.component.html',
  styleUrls: ['./workshop-admin-new.component.scss'],
})
export class WorkshopAdminNewComponent implements OnInit {
  createFormGroup: FormGroup;
  workshop: Workshop;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  fileName: string;
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(private workshopService: WorkshopService,
    public snackBar: MatSnackBar,
    private router: Router,
    public dialog: MatDialog
    ) { }

  ngOnInit() {
    this.createForm();
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(new ImageFile(response));
      this.uploader.clearQueue();
    };
  }

  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required], checkWorkshopValidator.bind(this)),
      objective: new FormControl('', []),
      requirement: new FormControl('', []),
      information: new FormControl('', []),
      image: new FormControl('', []),
      one_more_time: new FormControl(false, [])
    });
    function checkWorkshopValidator(control: AbstractControl) {
      return this.workshopService.checkName(control.value);
    }
  }

  add() {
    this.workshop = new Workshop(this.createFormGroup.value);
    this.workshop.image = this.fileName;
    this.workshopService.create(this.workshop).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Taller creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/talleres']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }
  openModalImage(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '800px',
      height: '800px',
      data: {image, widthRatio: 600, heightRatio: 461}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileName = result || null;
    });
  }
}
