import { Component, OnInit, ViewChild } from '@angular/core';
import { WorkshopService } from '../../services/workshop.service';
import { Workshop } from '../../models/workshop';
import { MatDialog, MatPaginator, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';
@Component({
  selector: 'app-workshop-admin',
  templateUrl: './workshop-admin.component.html',
  styleUrls: ['./workshop-admin.component.scss'],
  providers: [WorkshopService]
})
export class WorkshopAdminComponent implements OnInit {
  workshops: Workshop[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(private workshopService: WorkshopService, public dialog: MatDialog, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.chargeData();
  }
  delete(workshop) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: workshop , type: 'workshop'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  restore(workshop) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: workshop , type: 'workshop'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }

  onChangeEvent($event){
    this.workshopService.getAll($event.pageSize, $event.pageIndex + 1, 'name', true).subscribe((workshops) => {
      this.workshops = workshops;
      this.totalRegisters = this.workshopService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.workshopService.getAll(this.pageSize, 1, 'name', true).subscribe((workshops) => {
      this.workshops = workshops;
      this.totalRegisters = this.workshopService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }

}
