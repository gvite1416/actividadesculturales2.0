import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopAdminEditComponent } from './workshop-admin-edit.component';

describe('WorkshopAdminEditComponent', () => {
  let component: WorkshopAdminEditComponent;
  let fixture: ComponentFixture<WorkshopAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
