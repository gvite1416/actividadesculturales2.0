import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkshopService } from '../../../services/workshop.service';
import { Workshop } from '../../../models/workshop';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBar, MatSnackBarVerticalPosition, MatDialog } from '@angular/material';
import { GLOBALS } from '../../../services/globals';
import { FileUploader } from 'ng2-file-upload';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';
import { ImageFile } from '../../../models/image-file';

@Component({
  selector: 'app-workshop-admin-edit',
  templateUrl: './workshop-admin-edit.component.html',
  styleUrls: ['./workshop-admin-edit.component.scss']
})
export class WorkshopAdminEditComponent implements OnInit {
  public slug: string;
  public workshop: Workshop;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  imageWorkshop: string;
  fileName: string;
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(private route: ActivatedRoute,
    private workshopService: WorkshopService,
    public snackBar: MatSnackBar,
    private router: Router,
    public dialog: MatDialog
    ) {
    this.workshop = new Workshop();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.slug = params['slug'];
      this.workshopService.getBySlug(this.slug).subscribe( (workshop: Workshop) => {
        this.workshop = workshop;
        if (this.workshop.image) {
          this.imageWorkshop = GLOBALS.storageUrl + 'img/workshops/' + this.workshop.image;
        }
        this.editFormGroup.setValue({
          name: this.workshop.name,
          objective: this.workshop.objective,
          requirement: this.workshop.requirement,
          information: this.workshop.information,
          one_more_time: this.workshop.one_more_time,
          image: ''
        });
      });
    });
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(new ImageFile(response));
      this.uploader.clearQueue();
    };
    this.createForm();
  }
  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required], checkWorkshopValidator.bind(this)),
      objective: new FormControl('', []),
      requirement: new FormControl('', []),
      information: new FormControl('', []),
      image: new FormControl('', []),
      one_more_time: new FormControl(false, [])
    });
    function checkWorkshopValidator(control: AbstractControl) {
      return this.workshopService.checkName(control.value, this.workshop.id);
    }
  }

  edit() {
    this.workshop.name = this.editFormGroup.value.name;
    this.workshop.objective = this.editFormGroup.value.objective;
    this.workshop.requirement = this.editFormGroup.value.requirement;
    this.workshop.information = this.editFormGroup.value.information;
    this.workshop.image = this.fileName;
    this.workshop.one_more_time = this.editFormGroup.value.one_more_time;
    this.workshopService.update(this.workshop).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Taller editado correctamente', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/talleres/']);
      } else {
        for (const error in response.error) {
          if (error) {
            this.snackBar.open('Error: ' + response.error[error], '', {
              duration: 3000,
              verticalPosition: this.positionVertical
            });
          }
        }
      }
    });
  }

  openModalImage(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '800px',
      height: '800px',
      data: {image, widthRatio: 600, heightRatio: 461}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      console.log(result);
      this.fileName = result || null;
    });
  }

}
