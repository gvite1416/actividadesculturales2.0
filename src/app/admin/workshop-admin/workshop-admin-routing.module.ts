import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkshopAdminComponent } from './workshop-admin.component';
import { WorkshopAdminEditComponent } from './workshop-admin-edit/workshop-admin-edit.component';
import { WorkshopAdminNewComponent } from './workshop-admin-new/workshop-admin-new.component';


const routes: Routes = [
  {
    path: '',
    component: WorkshopAdminComponent
  },
  {
    path: 'editar/:slug',
    component: WorkshopAdminEditComponent
  },
  {
    path: 'nuevo',
    component: WorkshopAdminNewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkshopAdminRoutingModule { }
