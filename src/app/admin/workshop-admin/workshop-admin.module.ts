import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkshopAdminRoutingModule } from './workshop-admin-routing.module';
import { WorkshopAdminComponent } from './workshop-admin.component';
import { WorkshopAdminEditComponent } from './workshop-admin-edit/workshop-admin-edit.component';
import { MaterialModule } from '../../material/material.module';
import { WorkshopAdminNewComponent } from './workshop-admin-new/workshop-admin-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { DialogsModule } from '../../dialogs/dialogs.module';


@NgModule({
  imports: [
    CommonModule,
    WorkshopAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    DialogsModule
  ],
  declarations: [WorkshopAdminComponent, WorkshopAdminEditComponent, WorkshopAdminNewComponent]
})
export class WorkshopAdminModule { }
