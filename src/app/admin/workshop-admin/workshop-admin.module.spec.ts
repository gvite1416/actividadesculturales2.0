import { WorkshopAdminModule } from './workshop-admin.module';

describe('WorkshopAdminModule', () => {
  let workshopAdminModule: WorkshopAdminModule;

  beforeEach(() => {
    workshopAdminModule = new WorkshopAdminModule();
  });

  it('should create an instance', () => {
    expect(workshopAdminModule).toBeTruthy();
  });
});
