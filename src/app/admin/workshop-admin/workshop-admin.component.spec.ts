import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopAdminComponent } from './workshop-admin.component';

describe('WorkshopAdminComponent', () => {
  let component: WorkshopAdminComponent;
  let fixture: ComponentFixture<WorkshopAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
