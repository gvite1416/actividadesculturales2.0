import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportAdminRoutingModule } from './report-admin-routing.module';
import { ReportAdminComponent } from './report-admin.component';
import { MaterialModule } from '../../material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReportAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ReportAdminComponent]
})
export class ReportAdminModule { }
