import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarVerticalPosition } from '@angular/material';
import { Role } from '../../models/role';
import { CareerService } from '../../services/career.service';
import { Career } from '../../models/career';
import { PeriodService } from '../../services/period.service';
import { Period } from '../../models/period';
import { ReportService } from '../../services/report.service';
import { GLOBALS } from '../../services/globals';
import saveAs from 'file-saver';
import * as moment from 'moment';
@Component({
  selector: 'app-report-admin',
  templateUrl: './report-admin.component.html',
  styleUrls: ['./report-admin.component.scss']
})
export class ReportAdminComponent implements OnInit {
  oneFormGroup: FormGroup;
  twoFormGroup: FormGroup;
  threeFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  students: Role[];
  studentsComplete: any[];
  formatFiles: any[];
  careers: Career[];
  periods: Period[];
  pwa: any;
  urlReportOne: string;
  disableBtn: boolean;
  dateStart: moment.Moment;
  dateFinish: moment.Moment;
  constructor(
    private careerService: CareerService,
    private periodService: PeriodService,
    private reportService: ReportService
  ) {
    this.disableBtn = false;
    this.students = [];
    this.students.push(new Role({
      id: 2,
      name: 'Alumno UNAM',
      slug: 'student'
    }));
    this.students.push(new Role({
      id: 3,
      name: 'Ex Alumno UNAM',
      slug: 'exstudent'
    }));
    this.studentsComplete = [{slug: 'yes', name: 'Si'}, {slug: 'no' , name: 'No'}];
    this.formatFiles = [{slug: 'csv', name: 'CSV(Excel)', type: 'text/csv'}, {slug: 'pdf' , name: 'PDF', type: 'application/pdf'}];

    this.careerService.getAll().subscribe(careers => {
      this.careers = careers;
    });
    this.periodService.getAll().subscribe( (periods) => {
      this.periods = periods;
    });
    this.urlReportOne = GLOBALS.apiUrl + 'report/one' + '/?token=' + localStorage.getItem('token');
  }

  ngOnInit() {
    this.createOneForm();
    this.createTwoForm();
    this.createThreeForm();

  }

  private createOneForm() {
    this.oneFormGroup = new FormGroup({
      student: new FormControl('', [Validators.required]),
      career: new FormControl('', [Validators.required]),
      period: new FormControl('', [Validators.required]),
      complete: new FormControl('', [Validators.required]),
      typeFile: new FormControl('', [Validators.required])
    });
  }
  private createTwoForm() {
    this.twoFormGroup = new FormGroup({
      period: new FormControl('', [Validators.required]),
      start: new FormControl('' , [Validators.required]),
      finish: new FormControl('' , [Validators.required]),
      typeFile: new FormControl('', [Validators.required])
    });
  }
  private createThreeForm() {
    this.threeFormGroup = new FormGroup({
      period: new FormControl('', [Validators.required]),
      start: new FormControl('' , [Validators.required]),
      finish: new FormControl('' , [Validators.required]),
      typeFile: new FormControl('', [Validators.required])
    });
  }
  changePeriod() {
    if (this.twoFormGroup.value.period) {
      this.periods.forEach(p => {
        if (p.id === this.twoFormGroup.value.period) {
          this.twoFormGroup.setValue({
            period: p.id,
            start: p.start,
            finish: p.finish,
            typeFile: ''
          });
          this.dateStart = p.start;
          this.dateFinish = p.finish;
        }
      });
    }
  }
  changePeriodThree() {
    if (this.threeFormGroup.value.period) {
      this.periods.forEach(p => {
        if (p.id === this.threeFormGroup.value.period) {
          this.threeFormGroup.setValue({
            period: p.id,
            start: p.start,
            finish: p.finish,
            typeFile: ''
          });
          this.dateStart = p.start;
          this.dateFinish = p.finish;
        }
      });
    }
  }
  generateOne() {
    let contentType = '';
    this.formatFiles.forEach(f => {
      if (f.slug === this.oneFormGroup.value.typeFile) {
        contentType = f.type;
      }
    });
    this.disableBtn = true;
    this.reportService.one(this.oneFormGroup.value).subscribe(response => {
      this.downLoadFile(response, contentType, 'reporte1.' + this.oneFormGroup.value.typeFile);
      this.disableBtn = false;
    });
  }
  generateTwo() {
    let contentType = '';
    this.formatFiles.forEach(f => {
      if (f.slug === this.twoFormGroup.value.typeFile) {
        contentType = f.type;
      }
    });
    this.disableBtn = true;
    this.reportService.two(this.twoFormGroup.value).subscribe(response => {
      this.downLoadFile(response, contentType, 'reporte2.' + this.twoFormGroup.value.typeFile);
      this.disableBtn = false;
    });
  }
  generateThree() {
    let contentType = '';
    this.formatFiles.forEach(f => {
      if (f.slug === this.threeFormGroup.value.typeFile) {
        contentType = f.type;
      }
    });
    this.disableBtn = true;
    this.reportService.three(this.threeFormGroup.value).subscribe(response => {
      this.downLoadFile(response, contentType, 'reporte3.' + this.threeFormGroup.value.typeFile);
      this.disableBtn = false;
    });
  }
  downLoadFile(data: any, type: string, name: string) {
    const blob = new Blob([data], { type: type});
    saveAs(blob, name);
  }

}
