import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportAdminComponent } from './report-admin.component';

const routes: Routes = [
  {
    path: '',
    component: ReportAdminComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportAdminRoutingModule { }
