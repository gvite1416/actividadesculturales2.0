import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CampusAdminRoutingModule } from './campus-admin-routing.module';
import { CampusAdminComponent } from './campus-admin.component';
import { CampusAdminNewComponent } from './campus-admin-new/campus-admin-new.component';
import { CampusAdminEditComponent } from './campus-admin-edit/campus-admin-edit.component';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { CampusService } from '../../services/campus.service';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    CampusAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [CampusAdminComponent, CampusAdminNewComponent, CampusAdminEditComponent],
  providers: [CampusService]
})
export class CampusAdminModule { }
