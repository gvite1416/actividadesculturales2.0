import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog, MatPaginator } from '@angular/material';
import { Campus } from '../../models/campus';
import { CampusService } from '../../services/campus.service';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { EventAC } from '../../models/event';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-campus-admin',
  templateUrl: './campus-admin.component.html',
  styleUrls: ['./campus-admin.component.scss']
})
export class CampusAdminComponent implements OnInit {
  campus: Campus[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(
    private campusService: CampusService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.chargeData();
  }
  onChangeEvent($event){
    this.campusService.getAll($event.pageSize, $event.pageIndex + 1, 'name', true).subscribe((campus) => {
      this.campus = campus;
      this.totalRegisters = this.campusService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.campusService.getAll(this.pageSize, 1, 'name', true).subscribe((campus) => {
      this.campus = campus;
      this.totalRegisters = this.campusService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }
  delete(camp) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: camp , type: 'campus'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  restore(camp) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: camp , type: 'campus'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }

}
