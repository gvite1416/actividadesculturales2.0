import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusAdminEditComponent } from './campus-admin-edit.component';

describe('CampusAdminEditComponent', () => {
  let component: CampusAdminEditComponent;
  let fixture: ComponentFixture<CampusAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampusAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
