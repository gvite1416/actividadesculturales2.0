import { Component, OnInit } from '@angular/core';
import { Campus } from '../../../models/campus';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { CampusService } from '../../../services/campus.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-campus-admin-edit',
  templateUrl: './campus-admin-edit.component.html',
  styleUrls: ['./campus-admin-edit.component.scss']
})
export class CampusAdminEditComponent implements OnInit {
  editFormGroup: FormGroup;
  campus: Campus;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private campusService: CampusService,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.campus = new Campus();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.campusService.getById(params['id']).subscribe((campus: Campus) => {
        this.campus = campus;
        this.editFormGroup.setValue({
          name: this.campus.name,
          code: this.campus.code,
          college_degree_option: this.campus.college_degree_option
        });
      });
    });
    this.createForm();
  }
  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)],
        checkCampusValidator.bind(this)),
      code: new FormControl('', []),
      college_degree_option: new FormControl(false, [])
    });
    function checkCampusValidator(control: AbstractControl) {
      return this.campusService.checkName(control.value, this.campus.id);
    }
  }
  edit() {
    this.campus.name = this.editFormGroup.value.name;
    this.campus.code = this.editFormGroup.value.code;
    this.campus.college_degree_option = this.editFormGroup.value.college_degree_option;
    this.campusService.update(this.campus).subscribe((response) => {
      this.snackBar.open('Facultad editada', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
      this.router.navigate(['/admin/facultades/']);
    }, response => {
      if (response.status == 400){
        for (const index in response.error.message) {
          if (index) {
            response.error.message[index].forEach(error => {
              this.snackBar.open(error, '', {
                duration: 3000,
              });
            });
          }
        }
      }
    });
  }

}
