import { Component, OnInit } from '@angular/core';
import { Campus } from '../../../models/campus';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { CampusService } from '../../../services/campus.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-campus-admin-new',
  templateUrl: './campus-admin-new.component.html',
  styleUrls: ['./campus-admin-new.component.scss']
})
export class CampusAdminNewComponent implements OnInit {
  createFormGroup: FormGroup;
  campus: Campus;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private campusService: CampusService,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)],
        checkCampusValidator.bind(this)),
      code: new FormControl('', []),
      college_degree_option: new FormControl(false,[])
    });
    function checkCampusValidator(control: AbstractControl) {
      return this.campusService.checkName(control.value);
    }
  }
  add() {
    this.campus = new Campus(this.createFormGroup.value);
    this.campusService.create(this.campus).subscribe( (response) => {
        this.snackBar.open('Facultad creada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/facultades/']);
    }, response => {
      if (response.status == 400){
        for (const index in response.error.message) {
          if (index) {
            response.error.message[index].forEach(error => {
              this.snackBar.open(error, '', {
                duration: 3000,
              });
            });
          }
        }
      }
    });
  }

}
