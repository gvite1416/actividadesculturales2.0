import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusAdminNewComponent } from './campus-admin-new.component';

describe('CampusAdminNewComponent', () => {
  let component: CampusAdminNewComponent;
  let fixture: ComponentFixture<CampusAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampusAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
