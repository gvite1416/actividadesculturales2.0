import { CampusAdminModule } from './campus-admin.module';

describe('CampusAdminModule', () => {
  let campusAdminModule: CampusAdminModule;

  beforeEach(() => {
    campusAdminModule = new CampusAdminModule();
  });

  it('should create an instance', () => {
    expect(campusAdminModule).toBeTruthy();
  });
});
