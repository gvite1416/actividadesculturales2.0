import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampusAdminComponent } from './campus-admin.component';
import { CampusAdminNewComponent } from './campus-admin-new/campus-admin-new.component';
import { CampusAdminEditComponent } from './campus-admin-edit/campus-admin-edit.component';

const routes: Routes = [
  {
    path: '',
    component: CampusAdminComponent
  },
  {
    path: 'nuevo',
    component: CampusAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: CampusAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampusAdminRoutingModule { }
