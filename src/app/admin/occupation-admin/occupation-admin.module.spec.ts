import { OccupationAdminModule } from './occupation-admin.module';

describe('OccupationAdminModule', () => {
  let occupationAdminModule: OccupationAdminModule;

  beforeEach(() => {
    occupationAdminModule = new OccupationAdminModule();
  });

  it('should create an instance', () => {
    expect(occupationAdminModule).toBeTruthy();
  });
});
