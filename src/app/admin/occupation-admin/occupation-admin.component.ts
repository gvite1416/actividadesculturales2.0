import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBarVerticalPosition, MatDialog, MatSnackBar, MatPaginator } from '@angular/material';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { OccupationService } from '../../services/occupation.service';
import { Occupation } from '../../models/occupation';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-occupation-admin',
  templateUrl: './occupation-admin.component.html',
  styleUrls: ['./occupation-admin.component.scss']
})
export class OccupationAdminComponent implements OnInit {
  occupations: Occupation[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(
    private occupationService: OccupationService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.chargeData();
  }
  onChangeEvent($event){
    this.occupationService.getAll($event.pageSize, $event.pageIndex + 1, 'name', true).subscribe((occupations) => {
      this.occupations = occupations;
      this.totalRegisters = this.occupationService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.occupationService.getAll(this.pageSize, 1, 'name', true).subscribe((occupations) => {
      this.occupations = occupations;
      this.totalRegisters = this.occupationService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }

  delete(occupation) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: occupation , type: 'occupation'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  restore(occupation) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: occupation , type: 'occupation'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
}
