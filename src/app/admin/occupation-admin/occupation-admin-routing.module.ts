import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OccupationAdminComponent } from './occupation-admin.component';
import { OccupationAdminNewComponent } from './occupation-admin-new/occupation-admin-new.component';
import { OccupationAdminEditComponent } from './occupation-admin-edit/occupation-admin-edit.component';

const routes: Routes = [
  {
    path: '',
    component: OccupationAdminComponent
  },
  {
    path: 'nuevo',
    component: OccupationAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: OccupationAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OccupationAdminRoutingModule { }
