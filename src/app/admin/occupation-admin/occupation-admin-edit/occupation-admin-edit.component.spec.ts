import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationAdminEditComponent } from './occupation-admin-edit.component';

describe('OccupationAdminEditComponent', () => {
  let component: OccupationAdminEditComponent;
  let fixture: ComponentFixture<OccupationAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupationAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
