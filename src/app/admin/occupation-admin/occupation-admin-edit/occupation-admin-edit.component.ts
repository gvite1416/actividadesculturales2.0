import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Occupation } from '../../../models/occupation';
import { OccupationService } from '../../../services/occupation.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-occupation-admin-edit',
  templateUrl: './occupation-admin-edit.component.html',
  styleUrls: ['./occupation-admin-edit.component.scss']
})
export class OccupationAdminEditComponent implements OnInit {
  editFormGroup: FormGroup;
  occupation: Occupation;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private occupationService: OccupationService,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.occupation = new Occupation();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.occupationService.getById(params['id']).subscribe( (occupation: Occupation) => {
        this.occupation = occupation;
        this.editFormGroup.setValue({
          name: this.occupation.name
        });
      });
    });
    this.createForm();
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)], checkOccupationValidator.bind(this))
    });
    function checkOccupationValidator(control: AbstractControl) {
      return this.occupationService.checkName(control.value, this.occupation.id);
    }
  }
  edit() {
    this.occupation.name = this.editFormGroup.value.name;
    this.occupationService.update(this.occupation).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Ocupación editada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/ocupaciones/']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
