import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OccupationAdminRoutingModule } from './occupation-admin-routing.module';
import { OccupationAdminComponent } from './occupation-admin.component';
import { OccupationAdminNewComponent } from './occupation-admin-new/occupation-admin-new.component';
import { OccupationAdminEditComponent } from './occupation-admin-edit/occupation-admin-edit.component';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '../../material/material.module';
import { OccupationService } from '../../services/occupation.service';

@NgModule({
  imports: [
    CommonModule,
    OccupationAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule
  ],
  declarations: [OccupationAdminComponent, OccupationAdminNewComponent, OccupationAdminEditComponent],
  providers: [OccupationService]
})
export class OccupationAdminModule { }
