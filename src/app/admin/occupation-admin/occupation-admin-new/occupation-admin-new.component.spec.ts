import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationAdminNewComponent } from './occupation-admin-new.component';

describe('OccupationAdminNewComponent', () => {
  let component: OccupationAdminNewComponent;
  let fixture: ComponentFixture<OccupationAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupationAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
