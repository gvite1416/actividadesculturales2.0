import { Component, OnInit } from '@angular/core';
import { Occupation } from '../../../models/occupation';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { OccupationService } from '../../../services/occupation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-occupation-admin-new',
  templateUrl: './occupation-admin-new.component.html',
  styleUrls: ['./occupation-admin-new.component.scss']
})
export class OccupationAdminNewComponent implements OnInit {
  createFormGroup: FormGroup;
  occupation: Occupation;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private occupationService: OccupationService,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)], checkPeriodValidator.bind(this))
    });
    function checkPeriodValidator(control: AbstractControl) {
      return this.occupationService.checkName(control.value);
    }
  }
  add() {
    this.occupation = new Occupation(this.createFormGroup.value);
    this.occupationService.create(this.occupation).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Ocupación creada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/ocupaciones/']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
