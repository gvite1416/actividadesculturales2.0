import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationAdminComponent } from './occupation-admin.component';

describe('OccupationAdminComponent', () => {
  let component: OccupationAdminComponent;
  let fixture: ComponentFixture<OccupationAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupationAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
