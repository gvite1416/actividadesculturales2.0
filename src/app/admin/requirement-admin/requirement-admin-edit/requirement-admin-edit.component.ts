import { Component, OnInit } from '@angular/core';
import { Role } from '../../../models/role';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Requirement } from '../../../models/requirement';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { RequirementService } from '../../../services/requirement.service';
import { RoleService } from '../../../services/role.service';

@Component({
  selector: 'app-requirement-admin-edit',
  templateUrl: './requirement-admin-edit.component.html',
  styleUrls: ['./requirement-admin-edit.component.scss']
})
export class RequirementAdminEditComponent implements OnInit {
  role: Role;
  editFormGroup: FormGroup;
  requirement: Requirement;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private route: ActivatedRoute,
    public snackBar: MatSnackBar,
    private router: Router,
    private requirementService: RequirementService,
    private roleService: RoleService,
  ) {
    this.role = new Role();
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.requirementService.getById(params['id']).subscribe( (requirement: Requirement) => {
        this.requirement = requirement;
        this.editFormGroup.setValue({
          name: this.requirement.name,
          has_file: this.requirement.has_file,
          upload_type: this.requirement.upload_type
        });
      });
      this.roleService.getByIdStudent(params['role_id']).subscribe(role => {
        this.role = role;
      });
    });
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required]),
      has_file: new FormControl('', []),
      upload_type: new FormControl('', [])
    });
  }

  edit() {
    this.requirement.name =  this.editFormGroup.value.name;
    this.requirement.has_file =  this.editFormGroup.value.has_file;
    this.requirement.upload_type =  this.editFormGroup.value.upload_type;
    this.requirementService.update(this.requirement).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Requerimiento editado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/requisitos/']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
