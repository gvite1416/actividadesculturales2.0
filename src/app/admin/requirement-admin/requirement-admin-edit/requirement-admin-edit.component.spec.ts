import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementAdminEditComponent } from './requirement-admin-edit.component';

describe('RequirementAdminEditComponent', () => {
  let component: RequirementAdminEditComponent;
  let fixture: ComponentFixture<RequirementAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
