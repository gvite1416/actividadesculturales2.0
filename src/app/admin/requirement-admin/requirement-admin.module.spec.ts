import { RequirementAdminModule } from './requirement-admin.module';

describe('RequirementAdminModule', () => {
  let requirementAdminModule: RequirementAdminModule;

  beforeEach(() => {
    requirementAdminModule = new RequirementAdminModule();
  });

  it('should create an instance', () => {
    expect(requirementAdminModule).toBeTruthy();
  });
});
