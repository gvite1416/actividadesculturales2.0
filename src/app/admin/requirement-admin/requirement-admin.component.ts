import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../services/role.service';
import { Role } from '../../models/role';
import { RequirementService } from '../../services/requirement.service';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { MatSnackBarVerticalPosition, MatDialog, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-requirement-admin',
  templateUrl: './requirement-admin.component.html',
  styleUrls: ['./requirement-admin.component.scss']
})
export class RequirementAdminComponent implements OnInit {
  roles: Role[];
  activeRole: Role;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private roleService: RoleService,
    private requirementService: RequirementService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.roles = [];
    this.activeRole = new Role();
  }

  ngOnInit() {
    this.requirementService.getAll().subscribe(requirements => {
      this.roleService.getStudents().subscribe(roles => {
        this.roles = roles;
        this.roles.forEach(role => {
          role.requirements = [];
        });
        requirements.forEach(requirement => {
          this.roles.forEach((role, index) => {
            if (role.id === requirement.role_id) {
              this.roles[index].requirements.push(requirement);
            }
          });
        });
        this.activeRole = this.roles[0];
      });
    });
  }
  delete(requirement) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: requirement , type: 'requirement'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        } else {
          this.snackBar.open('Requerimiento Borrado', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.requirementService.getAll().subscribe(requirements => {
          this.roles.forEach(role => {
            role.requirements = [];
          });
          requirements.forEach(requirementAux => {
            this.roles.forEach((role, index) => {
              if (role.id === requirementAux.role_id) {
                this.roles[index].requirements.push(requirementAux);
              }
            });
          });
        });
      }
    });
  }

}
