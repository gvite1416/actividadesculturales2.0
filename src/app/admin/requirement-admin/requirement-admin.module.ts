import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequirementAdminComponent } from './requirement-admin.component';
import { RequirementAdminRoutingModule } from './requirement-admin-routing.module';
import { MaterialModule } from '../../material/material.module';
import { RequirementAdminNewComponent } from './requirement-admin-new/requirement-admin-new.component';
import { RequirementAdminEditComponent } from './requirement-admin-edit/requirement-admin-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';

@NgModule({
  imports: [
    CommonModule,
    RequirementAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule
  ],
  declarations: [RequirementAdminComponent, RequirementAdminNewComponent, RequirementAdminEditComponent]
})
export class RequirementAdminModule { }
