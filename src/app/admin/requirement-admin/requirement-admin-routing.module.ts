import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequirementAdminComponent } from './requirement-admin.component';
import { RequirementAdminNewComponent } from './requirement-admin-new/requirement-admin-new.component';
import { RequirementAdminEditComponent } from './requirement-admin-edit/requirement-admin-edit.component';

const routes: Routes = [
    {
        path: '',
        component: RequirementAdminComponent
    },
    {
        path: ':role_id/nuevo',
        component: RequirementAdminNewComponent
    },
    {
        path: ':role_id/nuevo/:period_id',
        component: RequirementAdminNewComponent
    },
    {
        path: ':role_id/editar/:id',
        component: RequirementAdminEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequirementAdminRoutingModule { }
