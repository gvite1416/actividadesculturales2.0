import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementAdminComponent } from './requirement-admin.component';

describe('RequirementAdminComponent', () => {
  let component: RequirementAdminComponent;
  let fixture: ComponentFixture<RequirementAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
