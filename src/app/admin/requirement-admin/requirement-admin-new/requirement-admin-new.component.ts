import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService } from '../../../services/role.service';
import { Role } from '../../../models/role';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Requirement } from '../../../models/requirement';
import { RequirementService } from '../../../services/requirement.service';

@Component({
  selector: 'app-requirement-admin-new',
  templateUrl: './requirement-admin-new.component.html',
  styleUrls: ['./requirement-admin-new.component.scss']
})
export class RequirementAdminNewComponent implements OnInit {
  role: Role;
  createFormGroup: FormGroup;
  requirement: Requirement;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  period_id: number;
  constructor(
    private route: ActivatedRoute,
    private roleService: RoleService,
    private requirementService: RequirementService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {
    this.role = new Role();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.roleService.getByIdStudent(params['role_id']).subscribe(role => {
        this.role = role;
      });
      this.period_id = params['period_id'];
    });
    this.createForm();
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required]),
      has_file: new FormControl('', []),
      upload_type: new FormControl('', [])
    });
  }

  add() {
    this.requirement = new Requirement({
      name: this.createFormGroup.value.name,
      has_file: this.createFormGroup.value.has_file,
      upload_type: this.createFormGroup.value.upload_type,
      role_id: this.role.id
    });
    this.requirementService.create(this.requirement).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Requerimiento creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        if (this.period_id) {
          this.router.navigate(['/admin/requisitos-periodo/' + this.period_id]);
        } else {
          this.router.navigate(['/admin/requisitos/']);
        }
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
