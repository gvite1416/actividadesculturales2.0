import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementAdminNewComponent } from './requirement-admin-new.component';

describe('RequirementAdminNewComponent', () => {
  let component: RequirementAdminNewComponent;
  let fixture: ComponentFixture<RequirementAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
