import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersAdminRoutingModule } from './users-admin-routing.module';
import { UsersAdminComponent } from './users-admin.component';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { UsersAdminNewComponent } from './users-admin-new/users-admin-new.component';
import { UsersAdminEditComponent } from './users-admin-edit/users-admin-edit.component';
import { NgxMaskModule } from 'ngx-mask';
import { PipesModule } from '../../pipes/pipes.module';
import { UsersAdminWorkshopsComponent } from './users-admin-workshops/users-admin-workshops.component';
import { UsersAdminValidateComponent } from './users-admin-validate/users-admin-validate.component';
import { UsersAdminExportComponent } from './users-admin-export/users-admin-export.component';
import { FileUploadModule } from 'ng2-file-upload';
import { UsersAdminFilesComponent } from './users-admin-files/users-admin-files.component';

@NgModule({
  imports: [
    CommonModule,
    UsersAdminRoutingModule,
    MaterialModule,
    FormsModule,
    DialogsModule,
    ReactiveFormsModule,
    PipesModule,
    NgxMaskModule.forRoot(),
    FileUploadModule
  ],
  declarations: [UsersAdminComponent, UsersAdminNewComponent, UsersAdminEditComponent, UsersAdminWorkshopsComponent, UsersAdminValidateComponent, UsersAdminExportComponent, UsersAdminFilesComponent]
})
export class UsersAdminModule { }
