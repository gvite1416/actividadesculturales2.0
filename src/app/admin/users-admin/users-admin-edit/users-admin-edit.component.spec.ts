import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersAdminEditComponent } from './users-admin-edit.component';

describe('UsersAdminEditComponent', () => {
  let component: UsersAdminEditComponent;
  let fixture: ComponentFixture<UsersAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
