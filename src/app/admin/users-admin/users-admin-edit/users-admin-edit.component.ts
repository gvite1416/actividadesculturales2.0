import { Component, OnInit } from '@angular/core';
import { Role } from '../../../models/role';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { RoleService } from '../../../services/role.service';
import { User } from '../../../models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { CampusService } from '../../../services/campus.service';
import { CareerService } from '../../../services/career.service';
import { OccupationService } from '../../../services/occupation.service';
import { Campus } from '../../../models/campus';
import { Career } from '../../../models/career';
import { Occupation } from '../../../models/occupation';
import { DataStudent } from '../../../models/data-student';
import { DataExternal } from '../../../models/data-external';
import { DataEmployee } from '../../../models/data-employee';

@Component({
  selector: 'app-users-admin-edit',
  templateUrl: './users-admin-edit.component.html',
  styleUrls: ['./users-admin-edit.component.scss']
})
export class UsersAdminEditComponent implements OnInit {

  roles: Role[];
  editFormGroup: FormGroup;
  dataStudentForm: FormGroup;
  dataExstudentForm: FormGroup;
  dataEmployeeForm: FormGroup;
  dataExternalForm: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  user: User;
  campus: Campus[];
  careers: Career[];
  occupations: Occupation[];
  genders: any;
  systems: any;
  constructor(
    private roleService: RoleService,
    public snackBar: MatSnackBar,
    private router: Router,
    private userService: UserService,
    private route: ActivatedRoute,
    private campusService: CampusService,
    private careerService: CareerService,
    private occupationService: OccupationService
  ) {
    this.user = new User();
    this.campusService.getAll().subscribe(campus => {
      this.campus = campus;
    });
    this.careerService.getAll().subscribe(careers => {
      this.careers = careers;
    });
    this.occupationService.getAll().subscribe( occupations => {
      this.occupations = occupations;
    });
    this.genders = [];
    this.genders.push({id: 1, name: 'Masculino'});
    this.genders.push({id: 2, name: 'Femenino'});
    this.genders.push({id: 3, name: 'Otro'});
    this.systems = [];
    this.systems.push({id: 'ESC', name: 'Escolarizado'});
    this.systems.push({id: 'SUA', name: 'SUA'});
  }

  ngOnInit() {
    this.createForm();
    this.createFormData();
    this.route.params.subscribe(params => {
      this.roleService.getAll().subscribe(roles => {
        this.roles = roles;
        this.userService.getById(params['id']).subscribe(user => {
          this.user = user;
          this.editFormGroup.setValue({
            name: this.user.name,
            lastname: this.user.lastname,
            surname: this.user.surname,
            email: this.user.email,
            phone: this.user.phone,
            celphone: this.user.celphone,
            role: this.user.student_type,
            gender: (this.user.gender === 'Masculino' || this.user.gender === 'Femenino') ? this.user.gender : 'Otro',
            genderOther: (!(this.user.gender === 'Masculino' || this.user.gender === 'Femenino')) ? this.user.gender : ''
          });
          if (this.user.data_external) {
            this.dataExternalForm.setValue({
              occupation: this.user.data_external.occupation_id,
              address: this.user.data_external.address
            });
          }
          if (this.user.data_student && this.user.student_type === 'student') {
            this.dataStudentForm.setValue({
              numberId: this.user.number_id,
              year: this.user.data_student.year,
              semester: this.user.data_student.semester,
              campus: this.user.data_student.campus_id,
              career: this.user.data_student.career_id,
              system: this.user.data_student.system
            });
          }
          if (this.user.data_student && this.user.student_type === 'exstudent') {
            this.dataExstudentForm.setValue({
              numberId: this.user.number_id,
              year: this.user.data_student.year,
              campus: this.user.data_student.campus_id,
              career: this.user.data_student.career_id,
              system: this.user.data_student.system
            });
          }
          if (this.user.data_employee) {
            this.dataEmployeeForm.setValue({
              workshift: this.user.data_employee.workshift,
              area: this.user.data_employee.area,
              campus: this.user.data_employee.campus_id,
              numberId: this.user.number_id,
            });
          }
        });
      });
    });
  }
  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      lastname: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      surname: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.required, Validators.email], checkEmailValidator.bind(this)),
      phone: new FormControl('', []),
      celphone: new FormControl('', []),
      role: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      genderOther: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
    });
    function checkEmailValidator(control: AbstractControl) {
      return this.userService.checkEmail(control.value, this.user.id);
    }
  }

  private createFormData() {
    this.dataStudentForm = new FormGroup({
      numberId: new FormControl('', [Validators.required], checkNumberIdValidator.bind(this)),
      year: new FormControl('', [Validators.required, Validators.min(1900)]),
      semester: new FormControl('', [Validators.required, Validators.min(1)]),
      campus: new FormControl('', [Validators.required]),
      career: new FormControl('', [Validators.required]),
      system: new FormControl('', [Validators.required])
    });
    this.dataExstudentForm = new FormGroup({
      numberId: new FormControl('', [Validators.required], checkNumberIdValidator.bind(this)),
      year: new FormControl('', [Validators.required, Validators.min(1900)]),
      campus: new FormControl('', [Validators.required]),
      career: new FormControl('', [Validators.required]),
      system: new FormControl('', [Validators.required])
    });
    this.dataEmployeeForm = new FormGroup({
      workshift: new FormControl('', [Validators.required]),
      area: new FormControl('', [Validators.required]),
      campus: new FormControl('', [Validators.required]),
      numberId: new FormControl('', [Validators.required], checkNumberIdValidator.bind(this))
    });
    this.dataExternalForm = new FormGroup({
      occupation: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required])
    });
    function checkNumberIdValidator(control: AbstractControl) {
      if (control.value.length >= 5) {
        return this.userService.checkNumberId(control.value, this.user.id);
      }
    }

  }

  update() {
    this.user.name = this.editFormGroup.value.name ;
    this.user.lastname = this.editFormGroup.value.lastname ;
    this.user.surname = this.editFormGroup.value.surname ;
    this.user.email = this.editFormGroup.value.email ;
    this.user.student_type = this.editFormGroup.value.role;
    this.user.phone = +this.editFormGroup.value.phone;
    this.user.celphone = +this.editFormGroup.value.celphone;
    this.roles.forEach(role => {
      if (role.slug == this.editFormGroup.value.role)
      this.user.role = role;
    });
    this.user.gender = (this.editFormGroup.value.gender === 'Masculino' ||
      this.editFormGroup.value.gender === 'Femenino') ? this.editFormGroup.value.gender : this.editFormGroup.value.genderOther;
    this.userService.update(this.user).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Usuario actualizado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/usuarios']);
      } else {
        for (const index in response.error) {
          if (index) {
            const errors = response.error[index];
            errors.forEach((error) => {
              this.snackBar.open(error, '', {
                duration: 3000,
                verticalPosition: this.positionVertical
              });
            });
          }
        }
      }
    });
  }

  onSubmitExternal() {
    if (this.user.data_external === undefined) {
      this.user.data_external = new DataExternal();
    }
    this.user.data_external.occupation_id = +this.dataExternalForm.value.occupation;
    this.user.data_external.address = this.dataExternalForm.value.address;
    this.update();
  }

  onSubmitStudent() {
    if (this.user.data_student === undefined) {
      this.user.data_student = new DataStudent();
    }
    this.user.data_student.campus_id = +this.dataStudentForm.value.campus;
    this.user.data_student.year = +this.dataStudentForm.value.year;
    this.user.data_student.semester = +this.dataStudentForm.value.semester;
    this.user.data_student.career_id = +this.dataStudentForm.value.career;
    this.user.data_student.system = this.dataStudentForm.value.system;
    this.user.number_id = +this.dataStudentForm.value.numberId;
    this.update();
  }
  onSubmitExstudent() {
    if (this.user.data_student === undefined) {
      this.user.data_student = new DataStudent();
    }
    this.user.data_student.campus_id = +this.dataExstudentForm.value.campus;
    this.user.data_student.year = +this.dataExstudentForm.value.year;
    this.user.data_student.career_id = +this.dataExstudentForm.value.career;
    this.user.data_student.semester = 0;
    this.user.data_student.system = this.dataExstudentForm.value.system;
    this.user.number_id = +this.dataExstudentForm.value.numberId;
    this.update();
  }
  onSubmitEmployee() {
    if (this.user.data_employee === undefined) {
      this.user.data_employee = new DataEmployee();
    }
    this.user.data_employee.campus_id = +this.dataEmployeeForm.value.campus;
    this.user.data_employee.area = this.dataEmployeeForm.value.area;
    this.user.data_employee.workshift = this.dataEmployeeForm.value.workshift;
    this.user.number_id = +this.dataEmployeeForm.value.numberId;
    this.update();
  }

  beforeForm() {
    this.router.navigate(['/admin/usuarios']);
  }

}
