import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersAdminWorkshopsComponent } from './users-admin-workshops.component';

describe('UsersAdminWorkshopsComponent', () => {
  let component: UsersAdminWorkshopsComponent;
  let fixture: ComponentFixture<UsersAdminWorkshopsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersAdminWorkshopsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAdminWorkshopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
