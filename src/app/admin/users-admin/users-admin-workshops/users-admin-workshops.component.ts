import { Component, OnInit } from '@angular/core';
import { Inscription } from '../../../models/inscription';
import { WorkshopPeriodService } from '../../../services/workshop-period.service';
import { ActivatedRoute } from '@angular/router';
import { GLOBALS } from '../../../services/globals';
import { InscriptionService } from '../../../services/inscription.service';

@Component({
  selector: 'app-users-admin-workshops',
  templateUrl: './users-admin-workshops.component.html',
  styleUrls: ['./users-admin-workshops.component.scss']
})
export class UsersAdminWorkshopsComponent implements OnInit {
  token: string = localStorage.getItem('token');
  inscriptions: Inscription[];
  apiUrl: string;
  blockButtons: boolean;
  userId: number;
  constructor(
    private workshopPeriodService: WorkshopPeriodService,
    private route: ActivatedRoute,
    private inscriptionService: InscriptionService,
  ) {
    this.inscriptions = [];
    this.apiUrl = GLOBALS.apiUrl;
    this.blockButtons = false;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params['user_id'];
      this.workshopPeriodService.getAllByUser(this.userId).subscribe(inscriptions => {
        this.inscriptions = inscriptions;
      });
    });
  }

  sendCaptureline(inscription: Inscription) {
    this.blockButtons = true;
    this.inscriptionService.sendCaptureline(inscription).subscribe(() => {
      this.workshopPeriodService.getAllByUser(this.userId).subscribe(inscriptions => {
        this.inscriptions = inscriptions;
        this.blockButtons = false;
      });
    });
  }
  removeCaptureline(inscription: Inscription) {
    this.blockButtons = true;
    this.inscriptionService.removeCaptureline(inscription).subscribe(() => {
      this.workshopPeriodService.getAllByUser(this.userId).subscribe(inscriptions => {
        this.inscriptions = inscriptions;
        this.blockButtons = false;
      });
    });
  }

}
