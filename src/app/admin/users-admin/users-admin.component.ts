import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarVerticalPosition, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { ChangePasswordDialogComponent } from '../../dialogs/change-password-dialog/change-password-dialog.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.scss']
})
export class UsersAdminComponent implements OnInit {
  searchFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  users: User[];
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  pageIndex: number;
  params: any;
  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    public dialog: MatDialog
  ) {
    this.pageSize = 10;
    this.totalRegisters = 0;
    this.params = {
      name: "",
      email: "",
      number_id: "",
      limit: 10,
      page: 1
    };
    this.pageIndex = 0;
  }

  ngOnInit() {
    this.createForm();
    this.route.queryParams.subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.params.name = params['name'] ? params['name'] : '';
      this.params.email = params['email'] ? params['email'] : '';
      this.params.number_id = params['numberId'] ? params['numberId'] : '';
      this.params.limit = params['limit'] ? params['limit'] : this.params.limit;
      this.params.page = params['page'] ? params['page'] : this.params.page;
      this.pageIndex = this.params.page - 1;
      this.searchFormGroup.setValue({
        name: this.params.name,
        email: this.params.email,
        numberId: this.params.number_id
      });
      this.userService.searchUsers(this.params).subscribe(users => {
        this.users = users;
        this.totalRegisters = this.userService.xTotalRegister;
      });
    });
  }

  onChangeEvent($event){
    this.router.navigate(['/admin/usuarios/'], {
      queryParams: {
        name: this.searchFormGroup.value.name,
        email: this.searchFormGroup.value.email,
        numberId: this.searchFormGroup.value.numberId,
        limit: $event.pageSize,
        page: $event.pageIndex + 1
    }});
  }
  private createForm() {
    this.searchFormGroup = new FormGroup({
      name: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.email]),
      numberId: new FormControl('', [])
    });
  }

  search() {
    this.router.navigate(['/admin/usuarios/'], {
      queryParams: {
        name: this.searchFormGroup.value.name,
        email: this.searchFormGroup.value.email,
        numberId: this.searchFormGroup.value.numberId,
        limit: this.params.limit,
        page: this.params.page
    }});
  }
  delete(user: User) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: user , type: 'user'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.route.queryParams.subscribe(params => {
          // Defaults to 0 if no query param provided.
          this.userService.searchUsers({
            name: params['name'],
            email: params['email'],
            number_id: params['numberId']
          }).subscribe(users => {
            this.users = users;
          });
        });
      }
    });
  }
  restore(user: User) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: user , type: 'user'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.route.queryParams.subscribe(params => {
          // Defaults to 0 if no query param provided.
          this.userService.searchUsers({
            name: params['name'],
            email: params['email'],
            number_id: params['numberId']
          }).subscribe(users => {
            this.users = users;
          });
        });
      }
    });
  }
  changePassword(user: User) {
    const dialogRef = this.dialog.open(ChangePasswordDialogComponent, {
      width: '600px',
      data: {user}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false && result.status === 1) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        } else if (result.success === true) {
          this.snackBar.open('Contraseña cambiada', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
      }
    });
  }
  verify(user: User) {
    this.userService.verify(user.id).subscribe(response => {
      if ( response.success ) {
        this.snackBar.open('Usuario validado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        user.is_verified = 1;
      } else {
        for (const index in response.error) {
          if (index) {
            const errors = response.error[index];
            errors.forEach((error) => {
              this.snackBar.open(error, '', {
                duration: 3000,
                verticalPosition: this.positionVertical
              });
            });
          }
        }
      }
    });
  }
}
