import { Component, OnInit } from '@angular/core';
import { GLOBALS } from '../../../services/globals';
import { FileUploader } from 'ng2-file-upload';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-users-admin-export',
  templateUrl: './users-admin-export.component.html',
  styleUrls: ['./users-admin-export.component.scss']
})
export class UsersAdminExportComponent implements OnInit {
  element: any;
  fileName: string;
  storageUrl: string = GLOBALS.storageUrl;
  rowsToImport: number;
  rowsImported: number;
  rowsNew: number;
  rowsUpdate: number;
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/excel',
    itemAlias: 'excel',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(public snackBar: MatSnackBar, private userService: UserService) {
    this.fileName = '';
    this.rowsToImport = 0.00001;
    this.rowsImported = 0;
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      response = JSON.parse(response);
      if (response.success) {
        this.fileName = response.file;
        this.rowsToImport = response.count_rows;
        this.exportExcel();
      } else {
        this.fileName = '';
        this.rowsToImport = 0;
        for (const index in response.error) {
          if (index) {
            const errors = response.error[index];
            errors.forEach((error) => {
              this.snackBar.open(error, '', {
                duration: 3000,
                verticalPosition: this.positionVertical
              });
            });
          }
        }
      }
      this.uploader.clearQueue();
    };
  }
  exportExcel() {
    this.exportByPage(1, 50, () => {
      console.log('se terminó');
    });
  }
  exportByPage(page, limit, callback) {
    this.userService.exportExcel(this.fileName, limit, page).subscribe(response => {
      this.rowsNew = response.new;
      this.rowsUpdate = response.update;
      this.rowsImported += response.new + response.update;
      if (this.rowsImported < this.rowsToImport) {
        this.exportByPage(page + 1, limit, callback);
      } else {
        callback();
      }
    });
  }

}
