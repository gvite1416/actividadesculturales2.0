import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersAdminExportComponent } from './users-admin-export.component';

describe('UsersAdminExportComponent', () => {
  let component: UsersAdminExportComponent;
  let fixture: ComponentFixture<UsersAdminExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersAdminExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAdminExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
