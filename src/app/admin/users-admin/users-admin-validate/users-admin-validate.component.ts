import { Component, OnInit } from '@angular/core';
import { Inscription } from '../../../models/inscription';
import { WorkshopPeriod } from '../../../models/workshop-period';
import { User } from '../../../models/user';
import { Workshop } from '../../../models/workshop';
import { Period } from '../../../models/period';
import { WorkshopPeriodService } from '../../../services/workshop-period.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBALS } from '../../../services/globals';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { InscriptionService } from '../../../services/inscription.service';
import { MatSnackBar, MatSnackBarVerticalPosition, MatDialog } from '@angular/material';
import { RouteService } from '../../../services/route.service';
import { InscriptionDialogComponent } from '../../../dialogs/inscription-dialog/inscription-dialog.component';

@Component({
  selector: 'app-users-admin-validate',
  templateUrl: './users-admin-validate.component.html',
  styleUrls: ['./users-admin-validate.component.scss']
})
export class UsersAdminValidateComponent implements OnInit {

  inscription: Inscription;
  workshop_period: WorkshopPeriod;
  user: User;
  urlPdf: string;
  urlPdfCard: string;
  editFormGroup: FormGroup;
  btnDisable: boolean;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private inscriptionService: InscriptionService,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar,
    private router: Router,
    private previousRouteService: RouteService,
    public dialog: MatDialog
  ) {
    this.btnDisable = false;
    this.inscription = new Inscription();
    this.workshop_period = new WorkshopPeriod();
    this.workshop_period.workshop = new Workshop();
    this.workshop_period.period = new Period();
    this.user = new User();
    this.urlPdf = '';
    this.urlPdfCard = '';
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.inscriptionService.getByUser(params['inscription_id'], params['user_id']).subscribe(response => {
        this.inscription = response.inscription;
        this.workshop_period = response.workshop_period;
        this.user = response.user;
        this.urlPdf = GLOBALS.apiUrl + 'inscription/inscribe-pdf/' + params['inscription_id'] + '/?token=' + localStorage.getItem('token');
        this.urlPdfCard = GLOBALS.apiUrl + 'users/profile-id-card/' +
          params['inscription_id'] + '/?token=' + localStorage.getItem('token');
        this.editFormGroup.setValue({
          ticket_folio: this.inscription.ticket_folio ? this.inscription.ticket_folio : '',
          ticket_date: this.inscription.ticket_date ? this.inscription.ticket_date : '',
          extra: this.inscription.extra ? this.inscription.extra : '',
          scholarship: this.inscription.scholarship ? this.inscription.scholarship : ''
        });
      });
    });
  }
  private createForm() {
    this.editFormGroup = new FormGroup({
      ticket_folio: new FormControl('', []),
      ticket_date: new FormControl('', []),
      extra: new FormControl('', []),
      scholarship: new FormControl('', [])
    });
    function checkTicketValidator(control: AbstractControl) {
      if (control.value.length > 5) {
        return this.inscriptionService.checkTicketFolio(control.value, this.inscription.id);
      }
    }
  }

  update() {
    this.inscription.ticket_folio = this.editFormGroup.value.ticket_folio ;
    this.inscription.ticket_date = this.editFormGroup.value.ticket_date ;
    this.inscription.extra = this.editFormGroup.value.extra ;
    this.inscription.scholarship = this.editFormGroup.value.scholarship ;
    this.btnDisable = true;
    this.inscriptionService.validate(this.inscription).subscribe( (response) => {
      this.btnDisable = false;
      if ( response.success ) {
        this.snackBar.open('Inscripción validada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.back();
      } else {
        for (const index in response.error) {
          if (index) {
            const errors = response.error[index];
            errors.forEach((error) => {
              let msgAction = '';
              if (error === 'Ya existe el folio de ticket') {
                msgAction = 'Ver Detalle';
              }
              const snackBarRef = this.snackBar.open(error, msgAction, {
                duration: 10000,
                verticalPosition: this.positionVertical
              });
              snackBarRef.onAction().subscribe(() => {
                const dialogRef = this.dialog.open(InscriptionDialogComponent, {
                  width: '600px',
                  data: {inscriptionId: response.inscription_id}
                });
              });
            });
          }
        }
      }
    });
  }

  back() {
    const routeBack = this.previousRouteService.getPreviousUrl();
    if (routeBack) {
      this.router.navigate([routeBack]);
    } else {
      this.router.navigate(['/admin/usuarios/talleres/' + this.user.id]);
    }
  }

}
