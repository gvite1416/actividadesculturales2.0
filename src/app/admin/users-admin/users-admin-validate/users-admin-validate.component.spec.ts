import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersAdminValidateComponent } from './users-admin-validate.component';

describe('UsersAdminValidateComponent', () => {
  let component: UsersAdminValidateComponent;
  let fixture: ComponentFixture<UsersAdminValidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersAdminValidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAdminValidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
