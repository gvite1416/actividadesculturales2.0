import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersAdminComponent } from './users-admin.component';
import { UsersAdminNewComponent } from './users-admin-new/users-admin-new.component';
import { UsersAdminEditComponent } from './users-admin-edit/users-admin-edit.component';
import { UsersAdminWorkshopsComponent } from './users-admin-workshops/users-admin-workshops.component';
import { UsersAdminValidateComponent } from './users-admin-validate/users-admin-validate.component';
import { UsersAdminExportComponent } from './users-admin-export/users-admin-export.component';
import { UsersAdminFilesComponent } from './users-admin-files/users-admin-files.component';

const routes: Routes = [
  {
    path: '',
    component: UsersAdminComponent
  },
  {
    path: 'nuevo',
    component: UsersAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: UsersAdminEditComponent
  },
  {
    path: 'talleres/:user_id',
    component: UsersAdminWorkshopsComponent
  },
  {
    path: 'talleres/:user_id/archivos',
    component: UsersAdminFilesComponent
  },
  {
    path: 'validar/:user_id/:inscription_id',
    component: UsersAdminValidateComponent
  },
  {
    path: 'exportar',
    component: UsersAdminExportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersAdminRoutingModule { }
