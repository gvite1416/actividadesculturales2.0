import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersAdminNewComponent } from './users-admin-new.component';

describe('UsersAdminNewComponent', () => {
  let component: UsersAdminNewComponent;
  let fixture: ComponentFixture<UsersAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
