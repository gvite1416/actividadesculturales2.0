import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../../services/role.service';
import { Role } from '../../../models/role';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';
import { UserAdmin } from '../../../models/user-admin';
import { GenpassPipe } from '../../../pipes/genpass.pipe';

@Component({
  selector: 'app-users-admin-new',
  templateUrl: './users-admin-new.component.html',
  styleUrls: ['./users-admin-new.component.scss'],
  providers: [GenpassPipe]
})
export class UsersAdminNewComponent implements OnInit {
  roles: Role[];
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  userAdmin: UserAdmin;
  genders: any;
  constructor(
    private roleService: RoleService,
    public snackBar: MatSnackBar,
    private router: Router,
    private userService: UserService,
    private genpassPipe: GenpassPipe
  ) {
    this.genders = [];
    this.genders.push({id: 1, name: 'Masculino'});
    this.genders.push({id: 2, name: 'Femenino'});
    this.genders.push({id: 3, name: 'Otro'});
  }

  ngOnInit() {
    this.roleService.getByGroup('depto').subscribe(roles => {
      this.roles = roles;
    });
    this.createForm();
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      lastname: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      surname: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.required, Validators.email], checkEmailValidator.bind(this)),
      phone: new FormControl('', []),
      celphone: new FormControl('', []),
      role: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      genderOther: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
    });
    function checkEmailValidator(control: AbstractControl) {
      return this.userService.checkEmail(control.value);
    }
  }

  add() {
    this.userAdmin = new UserAdmin(this.createFormGroup.value);
    this.userAdmin.user_type = this.createFormGroup.value.role;
    this.userAdmin.password = this.genpassPipe.transform(this.createFormGroup.value.name);
    this.userAdmin.password_confirmation = this.genpassPipe.transform(this.createFormGroup.value.name);
    this.userAdmin.gender = (this.createFormGroup.value.gender === 'Masculino' ||
      this.createFormGroup.value.gender === 'Femenino') ? this.createFormGroup.value.gender : this.createFormGroup.value.genderOther;
    this.userService.create(this.userAdmin).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Usuario creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/usuarios']);
      } else {
        for (const index in response.error) {
          if (index) {
            const errors = response.error[index];
            errors.forEach((error) => {
              this.snackBar.open(error, '', {
                duration: 3000,
                verticalPosition: this.positionVertical
              });
            });
          }
        }
      }
    });
  }

}
