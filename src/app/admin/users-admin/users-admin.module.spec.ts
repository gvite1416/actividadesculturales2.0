import { UsersAdminModule } from './users-admin.module';

describe('UsersAdminModule', () => {
  let usersAdminModule: UsersAdminModule;

  beforeEach(() => {
    usersAdminModule = new UsersAdminModule();
  });

  it('should create an instance', () => {
    expect(usersAdminModule).toBeTruthy();
  });
});
