import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersAdminFilesComponent } from './users-admin-files.component';

describe('UsersAdminFilesComponent', () => {
  let component: UsersAdminFilesComponent;
  let fixture: ComponentFixture<UsersAdminFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersAdminFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAdminFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
