import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodRequirementsAdminComponent } from './period-requirements-admin.component';

describe('PeriodRequirementsAdminComponent', () => {
  let component: PeriodRequirementsAdminComponent;
  let fixture: ComponentFixture<PeriodRequirementsAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodRequirementsAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodRequirementsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
