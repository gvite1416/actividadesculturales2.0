import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeriodRequirementsAdminRoutingModule } from './period-requirements-admin-routing.module';
import { PeriodRequirementsAdminComponent } from './period-requirements-admin.component';
import { MaterialModule } from '../../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    PeriodRequirementsAdminRoutingModule,
    MaterialModule
  ],
  declarations: [PeriodRequirementsAdminComponent]
})
export class PeriodRequirementsAdminModule { }
