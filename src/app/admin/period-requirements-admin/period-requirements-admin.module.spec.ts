import { PeriodRequirementsAdminModule } from './period-requirements-admin.module';

describe('PeriodRequirementsAdminModule', () => {
  let periodRequirementsAdminModule: PeriodRequirementsAdminModule;

  beforeEach(() => {
    periodRequirementsAdminModule = new PeriodRequirementsAdminModule();
  });

  it('should create an instance', () => {
    expect(periodRequirementsAdminModule).toBeTruthy();
  });
});
