import { Component, OnInit } from '@angular/core';
import { Role } from '../../models/role';
import { MatSnackBarVerticalPosition, MatSnackBar, MatSelectionListChange } from '@angular/material';
import { RoleService } from '../../services/role.service';
import { RequirementService } from '../../services/requirement.service';
import { Period } from '../../models/period';
import { ActivatedRoute } from '@angular/router';
import { PeriodService } from '../../services/period.service';
import { PeriodRequirementService } from '../../services/period-requirement.service';
import { Requirement } from '../../models/requirement';

@Component({
  selector: 'app-period-requirements-admin',
  templateUrl: './period-requirements-admin.component.html',
  styleUrls: ['./period-requirements-admin.component.scss']
})
export class PeriodRequirementsAdminComponent implements OnInit {
  roles: Role[];
  period: Period;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private roleService: RoleService,
    private requirementService: RequirementService,
    public snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private periodService: PeriodService,
    private periodRequirementService: PeriodRequirementService
  ) {
    this.roles = [];
    this.period = new Period();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.periodService.getById(params['period_id']).subscribe( (period: Period) => {
        this.period = period;
        this.requirementService.getAll().subscribe(requirements => {
          this.periodRequirementService.getAllByPeriod(this.period.id).subscribe( periodReqs => {
            requirements.forEach( req => {
              periodReqs.forEach(periodReq => {
                if (req.id === periodReq.id) {
                  req.active = true;
                }
              });
            });
            this.roleService.getStudents().subscribe(roles => {
              this.roles = roles;
              this.roles.forEach(role => {
                role.requirements = [];
              });
              requirements.forEach(requirement => {
                this.roles.forEach((role, index) => {
                  if (role.id === requirement.role_id) {
                    this.roles[index].requirements.push(requirement);
                  }
                });
              });
            });
          });
        });
      });
    });
  }
  requirementSelect(event: MatSelectionListChange) {
    this.roles.forEach((role, index) => {
      this.roles[index].requirements.forEach(requirement => {
        if (requirement.id === event.option.value) {
          requirement.active = event.option.selected;
        }
      });
    });
    if (event.option.selected) {
      this.periodRequirementService.enable(this.period.id, event.option.value).subscribe(response => {
        if ( response.success ) {
          this.snackBar.open('Requerimiento agregado', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        } else {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
      });
    } else {
      this.periodRequirementService.disable(this.period.id, event.option.value).subscribe(response => {
        if ( response.success ) {
          this.snackBar.open('Requerimiento removido', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        } else {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
      });
    }
  }

  countActive(requirements: Requirement[]) {
    let count = 0;
    requirements.forEach(element => {
      if (element.active) {
        count++;
      }
    });
    return count;
  }

}
