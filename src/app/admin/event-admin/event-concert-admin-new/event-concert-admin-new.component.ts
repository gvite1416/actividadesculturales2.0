import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder, FormArray } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog } from '@angular/material';
import { EventAC } from '../../../models/event';
import { Router } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { Classroom } from '../../../models/classroom';
import { ClassroomService } from '../../../services/classroom.service';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../../services/globals';
import { RoleService } from '../../../services/role.service';
import { Role } from '../../../models/role';
import { EventRole } from '../../../models/event-role';
import { Showing } from '../../../models/showing';
import { CampusService } from '../../../services/campus.service';
import { Campus } from '../../../models/campus';
import { EventCampus } from '../../../models/event-campus';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';
import { ImageFile } from '../../../models/image-file';

@Component({
  selector: 'app-event-concert-admin-new',
  templateUrl: './event-concert-admin-new.component.html',
  styleUrls: ['./event-concert-admin-new.component.scss']
})
export class EventConcertAdminNewComponent implements OnInit {
  concert: EventAC;
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  classrooms: Classroom[];
  fileName: string;
  fileNameThumbnail: string;
  fileNameTicket: string;
  roleStudents: Role[];
  campus: Campus[];
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderThumbnail: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderTicket: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private eventService: EventService,
    private classroomService: ClassroomService,
    private formBuilder: FormBuilder,
    private roleService: RoleService,
    private campusService: CampusService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.classroomService.getAll().subscribe( classrooms => this.classrooms = classrooms);
    this.createForm();
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderThumbnail.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderTicket.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(new ImageFile(response));
      this.uploader.clearQueue();
    };
    this.uploaderThumbnail.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImageThumbnail(new ImageFile(response));
      this.uploaderThumbnail.clearQueue();
    };
    this.uploaderTicket.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImageTicket(new ImageFile(response));
      this.uploaderTicket.clearQueue();
    };
    this.campusService.getAll().subscribe(campus => {
      this.campus = campus;
      const campusArray = this.createFormGroup.get('campus') as FormArray;
      this.campus.forEach(x => {
        campusArray.push(this.formBuilder.group({
          camp: new FormControl(false),
          id: new FormControl(x.id)
        }));
      });
    });
    this.roleService.getStudents().subscribe( roles => {
      this.roleStudents = roles;
      const studentsArray = this.createFormGroup.get('students') as FormArray;
      this.roleStudents.forEach(x => {
        studentsArray.push(this.formBuilder.group({
          student: new FormControl(false),
          id: new FormControl(x.id),
          price: new FormControl(''),
          limit: new FormControl('')
        }));
      });
    });
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required], checkEventValidator.bind(this)),
      inscription_finish: new FormControl('' , [Validators.required]),
      inscription_start: new FormControl('' , [Validators.required]),
      inscription_finish_hour: new FormControl('', [Validators.required]),
      inscription_start_hour: new FormControl('', [Validators.required]),
      quota: new FormControl('' , [Validators.required]),
      classroom: new FormControl('' , [Validators.required]),
      description: new FormControl('' , [Validators.required]),
      image: new FormControl('' , []),
      thumbnail: new FormControl('' , []),
      ticketimage: new FormControl('', []),
      all_campus: new FormControl(true , []),
      students: this.formBuilder.array([]),
      campus: this.formBuilder.array([]),
      showings: this.formBuilder.array([this.createShowing()])
    });
    function checkEventValidator(control: AbstractControl) {
      return this.eventService.checkName(control.value);
    }
  }
  private createShowing(): FormGroup {
    return this.formBuilder.group({
      start: new FormControl('' , [Validators.required]),
      finish: new FormControl('' , [Validators.required]),
      start_hour: new FormControl('', [Validators.required]),
      finish_hour: new FormControl('', [Validators.required])
    });
  }
  addShowing() {
    const showingsArray = this.createFormGroup.get('showings') as FormArray;
    showingsArray.push(this.createShowing());
  }
  deleteShowing(index) {
    const showingsArray = this.createFormGroup.get('showings') as FormArray;
    showingsArray.removeAt(index);
  }
  add() {
    let formatDateHelper = new FormatDateHelper();
    this.createFormGroup.value.inscription_start.hour(formatDateHelper.getHourNum(this.createFormGroup.value.inscription_start_hour, 'HHmm'));
    this.createFormGroup.value.inscription_start.minutes(formatDateHelper.getMinNum(this.createFormGroup.value.inscription_start_hour, 'HHmm'));
    this.createFormGroup.value.inscription_finish.hour(formatDateHelper.getHourNum(this.createFormGroup.value.inscription_finish_hour, 'HHmm'));
    this.createFormGroup.value.inscription_finish.minutes(formatDateHelper.getMinNum(this.createFormGroup.value.inscription_finish_hour, 'HHmm'));
    
    this.concert = new EventAC();
    this.concert.title = this.createFormGroup.value.title;
    this.concert.description = this.createFormGroup.value.description;
    this.concert.showings = this.createFormGroup.get('showings').value.map(showing => {
      showing.start.hour(formatDateHelper.getHourNum(showing.start_hour, 'HHmm'));
      showing.start.minutes(formatDateHelper.getMinNum(showing.start_hour, 'HHmm'));
      showing.finish.hour(formatDateHelper.getHourNum(showing.finish_hour, 'HHmm'));
      showing.finish.minutes(formatDateHelper.getMinNum(showing.finish_hour, 'HHmm'));
      return new Showing(showing)
    });
    this.concert.inscription_start = this.createFormGroup.value.inscription_start.format();
    this.concert.inscription_finish = this.createFormGroup.value.inscription_finish.format();
    this.concert.all_campus = this.createFormGroup.value.all_campus;
    this.concert.image = this.fileName;
    this.concert.thumbnail = this.fileNameThumbnail;
    this.concert.ticketimage = this.fileNameTicket;
    this.concert.classroom_id = this.createFormGroup.value.classroom;
    this.concert.quota = this.createFormGroup.value.quota;
    this.concert.event_category_id = 1;
    const students = this.createFormGroup.get('students').value;
    this.concert.students = [];
    students.forEach((student) => {
      if (student.student) {
        this.concert.students.push( new EventRole({
          id: student.id,
          pivot: {
            limit: student.limit ? student.limit : -1,
            price: student.price ? student.price : 0
          }
        }));
      }
    });
    const campus = this.createFormGroup.get('campus').value;
    this.concert.campus = [];
    campus.forEach((camp) => {
      if (camp.camp) {
        this.concert.campus.push( new EventCampus({
          id: camp.id
        }));
      }
    });
    this.eventService.create(this.concert).subscribe( (response) => {
        this.snackBar.open('Evento de concierto creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/eventos']);
    },error => {
      this.snackBar.open('Ocurrió un error', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
    });
  }
  openModalImage(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 1200, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileName = result || null;
    });
  }
  openModalImageThumbnail(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 600, heightRatio: 300}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameThumbnail = result || null;
    });
  }
  openModalImageTicket(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 550, heightRatio: 1000}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameTicket = result || null;
    });
  }
}
