import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventConcertAdminNewComponent } from './event-concert-admin-new.component';

describe('ConcertAdminNewComponent', () => {
  let component: EventConcertAdminNewComponent;
  let fixture: ComponentFixture<EventConcertAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventConcertAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventConcertAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
