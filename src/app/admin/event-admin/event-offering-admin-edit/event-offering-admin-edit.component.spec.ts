import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventOfferingAdminEditComponent } from './event-offering-admin-edit.component';

describe('OfferingAdminEditComponent', () => {
  let component: EventOfferingAdminEditComponent;
  let fixture: ComponentFixture<EventOfferingAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventOfferingAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventOfferingAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
