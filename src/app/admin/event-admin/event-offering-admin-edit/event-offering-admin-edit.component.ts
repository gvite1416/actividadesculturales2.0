import { Component, OnInit, ViewChild } from '@angular/core';
import { EventAC } from '../../../models/event';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog } from '@angular/material';
import { Classroom } from '../../../models/classroom';
import { Role } from '../../../models/role';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../../services/globals';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { ClassroomService } from '../../../services/classroom.service';
import { RoleService } from '../../../services/role.service';
import { EventRole } from '../../../models/event-role';
import { CampusService } from '../../../services/campus.service';
import { Campus } from '../../../models/campus';
import { EventCampus } from '../../../models/event-campus';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';
import { ImageFile } from '../../../models/image-file';

@Component({
  selector: 'app-event-offering-admin-edit',
  templateUrl: './event-offering-admin-edit.component.html',
  styleUrls: ['./event-offering-admin-edit.component.scss']
})
export class EventOfferingAdminEditComponent implements OnInit {
  offering: EventAC;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  classrooms: Classroom[];
  @ViewChild('picker', {static: false}) picker;
  fileName: string;
  fileNameThumbnail: string;
  roleStudents: Role[];
  campus: Campus[];
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderThumbnail: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private eventService: EventService,
    private classroomService: ClassroomService,
    private formBuilder: FormBuilder,
    private roleService: RoleService,
    private campusService: CampusService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.eventService.getById(params['id']).subscribe(event => {
        this.offering = event;
        this.classroomService.getAll().subscribe(classrooms => {
          this.classrooms = classrooms;
          this.roleService.getStudents().subscribe(roles => {
            this.campusService.getAll().subscribe(campus => {
              this.campus = campus;
              this.fileName = this.offering.image;
              this.fileNameThumbnail = this.offering.thumbnail;
              this.roleStudents = roles;

              // students Form
              const studentsArray = this.editFormGroup.get('students') as FormArray;
              const valueStudents = [];
              this.roleStudents.forEach(x => {
                valueStudents.push({
                  student: false,
                  id: x.id,
                  price: '',
                  limit: ''
                });
                studentsArray.push(this.formBuilder.group({
                  student: new FormControl(false),
                  id: new FormControl(x.id),
                  price: new FormControl(''),
                  limit: new FormControl('')
                }));
              });
              this.offering.students.forEach((student) => {
                valueStudents.forEach((s) => {
                  if (s.id === student.id) {
                    s.id = student.id;
                    s.limit = student.pivot.limit;
                    s.price = student.pivot.price;
                    s.student = true;
                  }
                });
              });

              // campus array

              const campusArray = this.editFormGroup.get('campus') as FormArray;
              const valueCampus = [];
              this.campus.forEach(x => {
                valueCampus.push({
                  camp: false,
                  id: x.id
                });
                campusArray.push(this.formBuilder.group({
                  camp: new FormControl(false),
                  id: new FormControl(''),
                }));
              });
              this.offering.campus.forEach((camp) => {
                valueCampus.forEach((s) => {
                  if (s.id === camp.id) {
                    s.id = camp.id;
                    s.camp = true;
                  }
                });
              });
              this.editFormGroup.setValue({
                title: this.offering.title,
                inscription_start: this.offering.inscription_start,
                inscription_finish: this.offering.inscription_finish,
                inscription_start_hour: this.offering.inscription_start.format('HH:mm'),
                inscription_finish_hour: this.offering.inscription_finish.format('HH:mm'),
                all_campus: this.offering.all_campus,
                image: '',
                thumbnail: '',
                classroom: this.offering.classroom_id,
                description: this.offering.description,
                students: valueStudents,
                campus: valueCampus
              });
            });
          });
        });
      });
    });
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderThumbnail.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(new ImageFile(response));
      this.uploader.clearQueue();
    };
    this.uploaderThumbnail.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImageThumbnail(new ImageFile(response));
      this.uploaderThumbnail.clearQueue();
    };
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required], checkEventValidator.bind(this)),
      inscription_start: new FormControl('', [Validators.required]),
      inscription_finish: new FormControl('', [Validators.required]),
      inscription_start_hour: new FormControl('', [Validators.required]),
      inscription_finish_hour: new FormControl('', [Validators.required]),
      classroom: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      image: new FormControl('', []),
      all_campus: new FormControl(false, []),
      thumbnail: new FormControl('', []),
      students: this.formBuilder.array([]),
      campus: this.formBuilder.array([])
    });
    function checkEventValidator(control: AbstractControl) {
      return this.eventService.checkName(control.value, this.offering.id);
    }
  }

  edit() {
    let formatDateHelper = new FormatDateHelper();
    this.editFormGroup.value.inscription_start.hour(formatDateHelper.getHourNum(this.editFormGroup.value.inscription_start_hour, 'HHmm'));
    this.editFormGroup.value.inscription_start.minutes(formatDateHelper.getMinNum(this.editFormGroup.value.inscription_start_hour, 'HHmm'));
    this.editFormGroup.value.inscription_finish.hour(formatDateHelper.getHourNum(this.editFormGroup.value.inscription_finish_hour, 'HHmm'));
    this.editFormGroup.value.inscription_finish.minutes(formatDateHelper.getMinNum(this.editFormGroup.value.inscription_finish_hour, 'HHmm'));

    this.offering.title = this.editFormGroup.value.title;
    this.offering.description = this.editFormGroup.value.description;
    this.offering.inscription_start = this.editFormGroup.value.inscription_start.format();
    this.offering.inscription_finish = this.editFormGroup.value.inscription_finish.format();
    this.offering.all_campus = this.editFormGroup.value.all_campus;
    this.offering.image = this.fileName;
    this.offering.thumbnail = this.fileNameThumbnail;
    this.offering.classroom_id = this.editFormGroup.value.classroom;
    this.offering.event_category_id = 2;
    this.offering.quota = -1;
    const students = this.editFormGroup.get('students').value;
    this.offering.students = [];
    students.forEach((student) => {
      if (student.student) {
        this.offering.students.push(new EventRole({
          id: student.id,
          pivot: {
            role_id: student.role_id,
            limit: student.limit,
            price: student.price
          }
        }));
      }
    });
    const campus = this.editFormGroup.get('campus').value;
    this.offering.campus = [];
    campus.forEach((camp) => {
      if (camp.camp) {
        this.offering.campus.push(new EventCampus({
          id: camp.id
        }));
      }
    });
    this.eventService.update(this.offering).subscribe((response) => {
      if (response.success) {
        this.snackBar.open('Evento de ofrenda editado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/eventos']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }
  openModalImage(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 1200, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileName = result || null;
    });
  }
  openModalImageThumbnail(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 600, heightRatio: 300}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameThumbnail = result || null;
    });
  }
}
