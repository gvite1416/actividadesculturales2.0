import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventAdminComponent } from './event-admin.component';
import { EventOfferingAdminNewComponent } from './event-offering-admin-new/event-offering-admin-new.component';
import { EventOfferingAdminEditComponent } from './event-offering-admin-edit/event-offering-admin-edit.component';
import { EventConcertAdminNewComponent } from './event-concert-admin-new/event-concert-admin-new.component';
import { EventConcertAdminEditComponent } from './event-concert-admin-edit/event-concert-admin-edit.component';
import { EventPublicAdminNewComponent } from './event-public-admin-new/event-public-admin-new.component';
import { EventPublicAdminEditComponent } from './event-public-admin-edit/event-public-admin-edit.component';

const routes: Routes = [
  {
    path: '',
    component: EventAdminComponent
  },
  {
    path: 'ofrendas/nuevo',
    component: EventOfferingAdminNewComponent
  },
  {
    path: 'ofrendas/editar/:id',
    component: EventOfferingAdminEditComponent
  },
  {
    path: 'conciertos/nuevo',
    component: EventConcertAdminNewComponent
  },
  {
    path: 'conciertos/editar/:id',
    component: EventConcertAdminEditComponent
  },
  {
    path: 'publicos/nuevo',
    component: EventPublicAdminNewComponent
  },
  {
    path: 'publicos/editar/:id',
    component: EventPublicAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventAdminRoutingModule { }
