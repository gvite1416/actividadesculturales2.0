import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventAdminRoutingModule } from './event-admin-routing.module';
import { EventAdminComponent } from './event-admin.component';
import { MaterialModule } from '../../material/material.module';
import { PipesModule } from '../../pipes/pipes.module';
import { EventOfferingAdminNewComponent } from './event-offering-admin-new/event-offering-admin-new.component';
import { EventOfferingAdminEditComponent } from './event-offering-admin-edit/event-offering-admin-edit.component';
import { EventConcertAdminEditComponent } from './event-concert-admin-edit/event-concert-admin-edit.component';
import { EventConcertAdminNewComponent } from './event-concert-admin-new/event-concert-admin-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { EventService } from '../../services/event.service';
import { FileUploadModule } from 'ng2-file-upload';
import { EventPublicAdminNewComponent } from './event-public-admin-new/event-public-admin-new.component';
import { EventPublicAdminEditComponent } from './event-public-admin-edit/event-public-admin-edit.component';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    EventAdminRoutingModule,
    MaterialModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    FileUploadModule,
    NgxMaskModule
  ],
  declarations: [
    EventAdminComponent,
    EventOfferingAdminNewComponent,
    EventOfferingAdminEditComponent,
    EventConcertAdminEditComponent,
    EventConcertAdminNewComponent,
    EventPublicAdminNewComponent,
    EventPublicAdminEditComponent
  ],
  providers: [EventService]
})
export class EventAdminModule { }
