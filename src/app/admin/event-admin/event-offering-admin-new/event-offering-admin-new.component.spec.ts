import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventOfferingAdminNewComponent } from './event-offering-admin-new.component';

describe('EventOfferingAdminNewComponent', () => {
  let component: EventOfferingAdminNewComponent;
  let fixture: ComponentFixture<EventOfferingAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventOfferingAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventOfferingAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
