import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder, FormArray } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog } from '@angular/material';
import { EventAC } from '../../../models/event';
import { Router } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { Classroom } from '../../../models/classroom';
import { ClassroomService } from '../../../services/classroom.service';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../../services/globals';
import { RoleService } from '../../../services/role.service';
import { Role } from '../../../models/role';
import { EventRole } from '../../../models/event-role';
import { Campus } from '../../../models/campus';
import { CampusService } from '../../../services/campus.service';
import { EventCampus } from '../../../models/event-campus';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';
import { ImageFile } from '../../../models/image-file';

@Component({
  selector: 'app-event-offering-admin-new',
  templateUrl: './event-offering-admin-new.component.html',
  styleUrls: ['./event-offering-admin-new.component.scss'],
})
export class EventOfferingAdminNewComponent implements OnInit {
  offering: EventAC;
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  classrooms: Classroom[];
  fileName: string;
  fileNameThumbnail: string;
  roleStudents: Role[];
  campus: Campus[];
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderThumbnail: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private eventService: EventService,
    private classroomService: ClassroomService,
    private formBuilder: FormBuilder,
    private roleService: RoleService,
    private campusService: CampusService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.classroomService.getAll().subscribe( classrooms => this.classrooms = classrooms);
    this.createForm();
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderThumbnail.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(new ImageFile(response));
      this.uploader.clearQueue();
    };
    this.uploaderThumbnail.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImageThumbnail(new ImageFile(response));
      this.uploaderThumbnail.clearQueue();
    };
    this.roleService.getStudents().subscribe( roles => {
      this.roleStudents = roles;
      const studentsArray = this.createFormGroup.get('students') as FormArray;
      this.roleStudents.forEach(x => {
        studentsArray.push(this.formBuilder.group({
          student: new FormControl(false),
          id: new FormControl(x.id),
          price: new FormControl(0),
          limit: new FormControl('')
        }));
      });
    });
    this.campusService.getAll().subscribe(campus => {
      this.campus = campus;
      const campusArray = this.createFormGroup.get('campus') as FormArray;
      this.campus.forEach(x => {
        campusArray.push(this.formBuilder.group({
          camp: new FormControl(false),
          id: new FormControl(x.id)
        }));
      });
    });
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required], checkEventValidator.bind(this)),
      start: new FormControl('' , [Validators.required]),
      finish: new FormControl('' , [Validators.required]),
      finish_hour: new FormControl('', [Validators.required]),
      start_hour: new FormControl('', [Validators.required]),
      classroom: new FormControl('' , [Validators.required]),
      description: new FormControl('' , [Validators.required]),
      image: new FormControl('' , [Validators.required]),
      thumbnail: new FormControl('' , [Validators.required]),
      all_campus: new FormControl(true, []),
      students: this.formBuilder.array([]),
      campus: this.formBuilder.array([])
    });
    function checkEventValidator(control: AbstractControl) {
      return this.eventService.checkName(control.value);
    }
  }

  add() {
    let formatDateHelper = new FormatDateHelper();
    this.createFormGroup.value.start.hour(formatDateHelper.getHourNum(this.createFormGroup.value.start_hour, 'HHmm'));
    this.createFormGroup.value.start.minutes(formatDateHelper.getMinNum(this.createFormGroup.value.start_hour, 'HHmm'));
    this.createFormGroup.value.finish.hour(formatDateHelper.getHourNum(this.createFormGroup.value.finish_hour, 'HHmm'));
    this.createFormGroup.value.finish.minutes(formatDateHelper.getMinNum(this.createFormGroup.value.finish_hour, 'HHmm'));
    
    this.offering = new EventAC();
    this.offering.title = this.createFormGroup.value.title;
    this.offering.description = this.createFormGroup.value.description;
    this.offering.inscription_start = this.createFormGroup.value.start.format();
    this.offering.inscription_finish = this.createFormGroup.value.finish.format();
    this.offering.all_campus = this.createFormGroup.value.all_campus;
    this.offering.image = this.fileName;
    this.offering.thumbnail = this.fileNameThumbnail;
    this.offering.quota = -1;
    this.offering.classroom_id = this.createFormGroup.value.classroom;
    this.offering.event_category_id = 2;
    const students = this.createFormGroup.get('students').value;
    this.offering.students = [];
    students.forEach((student) => {
      if (student.student) {
        this.offering.students.push( new EventRole({
          id: student.id,
          pivot: {
            limit: student.limit,
            price: student.price
          }
        }));
      }
    });

    const campus = this.createFormGroup.get('campus').value;
    this.offering.campus = [];
    campus.forEach((camp) => {
      if (camp.camp) {
        this.offering.campus.push( new EventCampus({
          id: camp.id
        }));
      }
    });
    this.eventService.create(this.offering).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Evento de ofrenda creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/eventos']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

  openModalImage(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 1200, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileName = result || null;
    });
  }
  openModalImageThumbnail(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 600, heightRatio: 300}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameThumbnail = result || null;
    });
  }

}
