import { Component, OnInit, ViewChild } from '@angular/core';
import { EventAC } from '../../../models/event';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog } from '@angular/material';
import { Classroom } from '../../../models/classroom';
import { Role } from '../../../models/role';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../../services/globals';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { ClassroomService } from '../../../services/classroom.service';
import { RoleService } from '../../../services/role.service';
import { EventRole } from '../../../models/event-role';
import { Showing } from '../../../models/showing';
import { CampusService } from '../../../services/campus.service';
import { Campus } from '../../../models/campus';
import { EventCampus } from '../../../models/event-campus';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';
import { ImageFile } from '../../../models/image-file';
@Component({
  selector: 'app-event-concert-admin-edit',
  templateUrl: './event-concert-admin-edit.component.html',
  styleUrls: ['./event-concert-admin-edit.component.scss']
})
export class EventConcertAdminEditComponent implements OnInit {
  concert: EventAC;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  classrooms: Classroom[];
  fileName: string;
  fileNameThumbnail: string;
  fileNameTicket: string;
  roleStudents: Role[];
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderThumbnail: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderTicket: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  campus: Campus[];
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private eventService: EventService,
    private classroomService: ClassroomService,
    private formBuilder: FormBuilder,
    private roleService: RoleService,
    private route: ActivatedRoute,
    private campusService: CampusService,
    public dialog: MatDialog
  ) {
    this.concert = new EventAC();
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.eventService.getById(params['id']).subscribe(event => {
        this.concert = event;
        this.classroomService.getAll().subscribe(classrooms => {
          this.classrooms = classrooms;
          this.roleService.getStudents().subscribe(roles => {
            this.campusService.getAll().subscribe(campus => {
              this.campus = campus;
              this.fileName = this.concert.image;
              this.fileNameThumbnail = this.concert.thumbnail;
              this.roleStudents = roles;

              // students array
              const studentsArray = this.editFormGroup.get('students') as FormArray;
              const valueStudents = [];
              this.roleStudents.forEach(x => {
                valueStudents.push({
                  student: false,
                  id: x.id,
                  price: '',
                  limit: ''
                });
                studentsArray.push(this.formBuilder.group({
                  student: new FormControl(false),
                  id: new FormControl(''),
                  price: new FormControl(''),
                  limit: new FormControl('')
                }));
              });
              this.concert.students.forEach((student) => {
                valueStudents.forEach((s) => {
                  if (s.id === student.id) {
                    s.id = student.id;
                    s.limit = student.pivot.limit;
                    s.price = student.pivot.price;
                    s.student = true;
                  }
                });
              });

              // campus array

              const campusArray = this.editFormGroup.get('campus') as FormArray;
              const valueCampus = [];
              this.campus.forEach(x => {
                valueCampus.push({
                  camp: false,
                  id: x.id
                });
                campusArray.push(this.formBuilder.group({
                  camp: new FormControl(false),
                  id: new FormControl(''),
                }));
              });
              this.concert.campus.forEach((camp) => {
                valueCampus.forEach((s) => {
                  if (s.id === camp.id) {
                    s.id = camp.id;
                    s.camp = true;
                  }
                });
              });

              const valueShowings = [];
              this.concert.showings.forEach(showing => {
                this.addShowing();
                valueShowings.push({
                  id: showing.id,
                  start: showing.start,
                  finish: showing.finish,
                  start_hour: showing.start.format('HH:mm'),
                  finish_hour: showing.finish.format('HH:mm')
                });
              });
              this.editFormGroup.setValue({
                title: this.concert.title,
                inscription_start: this.concert.inscription_start,
                inscription_finish: this.concert.inscription_finish,
                inscription_start_hour: this.concert.inscription_start.format('HH:mm'),
                inscription_finish_hour: this.concert.inscription_finish.format('HH:mm'),
                quota: this.concert.quota,
                image: '',
                thumbnail: '',
                ticketimage: '',
                classroom: this.concert.classroom_id,
                description: this.concert.description,
                all_campus: this.concert.all_campus,
                students: valueStudents,
                campus: valueCampus,
                showings: valueShowings
              });
            });
          });
        });
      });
    });
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderThumbnail.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderTicket.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(new ImageFile(response));
      this.uploader.clearQueue();
    };
    this.uploaderThumbnail.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImageThumbnail(new ImageFile(response));
      this.uploaderThumbnail.clearQueue();
    };
    this.uploaderTicket.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImageTicket(new ImageFile(response));
      this.uploaderTicket.clearQueue();
    };
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required], checkEventValidator.bind(this)),
      inscription_finish: new FormControl('', [Validators.required]),
      inscription_start: new FormControl('', [Validators.required]),
      inscription_finish_hour: new FormControl('', [Validators.required]),
      inscription_start_hour: new FormControl('', [Validators.required]),
      quota: new FormControl('', [Validators.required]),
      classroom: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      image: new FormControl('', []),
      thumbnail: new FormControl('', []),
      ticketimage: new FormControl('', []),
      all_campus: new FormControl(false, []),
      students: this.formBuilder.array([]),
      campus: this.formBuilder.array([]),
      showings: this.formBuilder.array([])
    });
    function checkEventValidator(control: AbstractControl) {
      return this.eventService.checkName(control.value, this.concert.id);
    }
  }
  private createShowing(): FormGroup {
    return this.formBuilder.group({
      id: new FormControl('', []),
      start: new FormControl('', [Validators.required]),
      finish: new FormControl('', [Validators.required]),
      start_hour: new FormControl('', [Validators.required]),
      finish_hour: new FormControl('', [Validators.required])
    });
  }
  addShowing() {
    const showingsArray = this.editFormGroup.get('showings') as FormArray;
    showingsArray.push(this.createShowing());
  }
  deleteShowing(index) {
    const showingsArray = this.editFormGroup.get('showings') as FormArray;
    showingsArray.removeAt(index);
  }

  edit() {

    let formatDateHelper = new FormatDateHelper();
    this.editFormGroup.value.inscription_start.hour(formatDateHelper.getHourNum(this.editFormGroup.value.inscription_start_hour, 'HHmm'));
    this.editFormGroup.value.inscription_start.minutes(formatDateHelper.getMinNum(this.editFormGroup.value.inscription_start_hour, 'HHmm'));
    this.editFormGroup.value.inscription_finish.hour(formatDateHelper.getHourNum(this.editFormGroup.value.inscription_finish_hour, 'HHmm'));
    this.editFormGroup.value.inscription_finish.minutes(formatDateHelper.getMinNum(this.editFormGroup.value.inscription_finish_hour, 'HHmm'));

    this.concert.title = this.editFormGroup.value.title;
    this.concert.description = this.editFormGroup.value.description;
    this.concert.inscription_start = this.editFormGroup.value.inscription_start.format();
    this.concert.inscription_finish = this.editFormGroup.value.inscription_finish.format();
    this.concert.all_campus = this.editFormGroup.value.all_campus;
    this.concert.quota = this.editFormGroup.value.quota;
    this.concert.image = this.fileName;
    this.concert.thumbnail = this.fileNameThumbnail;
    this.concert.ticketimage = this.fileNameTicket;
    this.concert.classroom_id = this.editFormGroup.value.classroom;
    this.concert.event_category_id = 1;
    this.concert.showings = this.editFormGroup.get('showings').value.map(showing => {
      showing.start.hour(formatDateHelper.getHourNum(showing.start_hour, 'HHmm'));
      showing.start.minutes(formatDateHelper.getMinNum(showing.start_hour, 'HHmm'));
      showing.finish.hour(formatDateHelper.getHourNum(showing.finish_hour, 'HHmm'));
      showing.finish.minutes(formatDateHelper.getMinNum(showing.finish_hour, 'HHmm'));
      return new Showing(showing);
    });
    const students = this.editFormGroup.get('students').value;
    this.concert.students = [];
    students.forEach((student) => {
      if (student.student) {
        this.concert.students.push(new EventRole({
          id: student.id,
          pivot: {
            limit: student.limit ? student.limit : -1,
            price: student.price ? student.price : 0
          }
        }));
      }
    });
    const campus = this.editFormGroup.get('campus').value;
    this.concert.campus = [];
    campus.forEach((camp) => {
      if (camp.camp) {
        this.concert.campus.push(new EventCampus({
          id: camp.id
        }));
      }
    });
    this.eventService.update(this.concert).subscribe((response) => {
        this.snackBar.open('Evento editado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/eventos']);
    }, error => {
      for (const index in error.error) {
        if (index) {
          this.snackBar.open(error.error[index], '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
      }
    });
  }
  openModalImage(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 1200, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileName = result || null;
    });
  }
  openModalImageThumbnail(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 600, heightRatio: 300}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameThumbnail = result || null;
    });
  }
  openModalImageTicket(image: ImageFile) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '800px',
      data: {image, widthRatio: 550, heightRatio: 1000}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameTicket = result || null;
    });
  }
}
