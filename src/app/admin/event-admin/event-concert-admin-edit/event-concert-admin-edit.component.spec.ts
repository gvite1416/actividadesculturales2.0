import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventConcertAdminEditComponent } from './event-concert-admin-edit.component';

describe('EventConcertAdminEditComponent', () => {
  let component: EventConcertAdminEditComponent;
  let fixture: ComponentFixture<EventConcertAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventConcertAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventConcertAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
