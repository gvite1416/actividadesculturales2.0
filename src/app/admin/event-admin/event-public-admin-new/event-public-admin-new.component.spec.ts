import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPublicAdminNewComponent } from './event-public-admin-new.component';

describe('EventPublicAdminNewComponent', () => {
  let component: EventPublicAdminNewComponent;
  let fixture: ComponentFixture<EventPublicAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventPublicAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPublicAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
