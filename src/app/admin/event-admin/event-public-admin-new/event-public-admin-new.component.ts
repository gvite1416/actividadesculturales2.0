import { Component, OnInit, ViewChild } from '@angular/core';
import { EventAC } from '../../../models/event';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl, FormArray } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Classroom } from '../../../models/classroom';
import { Role } from '../../../models/role';
import { Campus } from '../../../models/campus';
import { Router } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { ClassroomService } from '../../../services/classroom.service';
import { RoleService } from '../../../services/role.service';
import { CampusService } from '../../../services/campus.service';
import { EventRole } from '../../../models/event-role';
import { EventCampus } from '../../../models/event-campus';
import { Showing } from '../../../models/showing';
import * as moment from 'moment';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';

@Component({
  selector: 'app-event-public-admin-new',
  templateUrl: './event-public-admin-new.component.html',
  styleUrls: ['./event-public-admin-new.component.scss']
})
export class EventPublicAdminNewComponent implements OnInit {
  concert: EventAC;
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  classrooms: Classroom[];
  roleStudents: Role[];
  campus: Campus[];
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private eventService: EventService,
    private classroomService: ClassroomService,
    private formBuilder: FormBuilder,
    private roleService: RoleService,
    private campusService: CampusService
  ) { }

  ngOnInit() {
    this.classroomService.getAll().subscribe( classrooms => this.classrooms = classrooms);
    this.createForm();
    this.roleService.getStudents().subscribe( roles => {
      this.roleStudents = roles;
    });
    this.campusService.getAll().subscribe(campus => {
      this.campus = campus;
    });
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required], checkEventValidator.bind(this)),
      classroom: new FormControl('' , [Validators.required]),
      description: new FormControl('' , [Validators.required]),
      showings: this.formBuilder.array([this.createShowing()])
    });
    function checkEventValidator(control: AbstractControl) {
      return this.eventService.checkName(control.value);
    }
  }
  private createShowing(): FormGroup {
    return this.formBuilder.group({
      start: new FormControl('' , [Validators.required]),
      finish: new FormControl('' , [Validators.required]),
      start_hour: new FormControl('', [Validators.required]),
      finish_hour: new FormControl('', [Validators.required])
    });
  }
  addShowing() {
    const showingsArray = this.createFormGroup.get('showings') as FormArray;
    showingsArray.push(this.createShowing());
  }
  deleteShowing(index) {
    const showingsArray = this.createFormGroup.get('showings') as FormArray;
    showingsArray.removeAt(index);
  }

  add() {
    let formatDateHelper = new FormatDateHelper();
    this.concert = new EventAC();
    this.concert.title = this.createFormGroup.value.title;
    this.concert.description = this.createFormGroup.value.description;
    this.concert.showings = this.createFormGroup.get('showings').value.map(showing => {
      showing.start.hour(formatDateHelper.getHourNum(showing.start_hour, 'HHmm'));
      showing.start.minutes(formatDateHelper.getMinNum(showing.start_hour, 'HHmm'));
      showing.finish.hour(formatDateHelper.getHourNum(showing.finish_hour, 'HHmm'));
      showing.finish.minutes(formatDateHelper.getMinNum(showing.finish_hour, 'HHmm'));
      return new Showing(showing);
    });
    this.concert.inscription_start = moment();
    this.concert.inscription_finish = moment();
    this.concert.all_campus = true;
    this.concert.image = 'default';
    this.concert.thumbnail = 'default';
    this.concert.classroom_id = this.createFormGroup.value.classroom;
    this.concert.quota = 0;
    this.concert.event_category_id = 3;
    
    this.concert.students = [];
    this.roleStudents.forEach((student) => {
      this.concert.students.push( new EventRole({
        id: student.id,
        pivot: {
          limit:  -1,
          price:  0
        }
      }));
    });
    this.concert.campus = [];
    this.campus.forEach((camp) => {
      this.concert.campus.push( new EventCampus({
        id: camp.id
      }));
    });
    this.eventService.create(this.concert).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Evento público creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/eventos']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
