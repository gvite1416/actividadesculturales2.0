import { Component, OnInit } from '@angular/core';
import { EventService } from '../../services/event.service';
import { EventAC } from '../../models/event';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { MatDialog, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import saveAs from 'file-saver';
import { ReportService } from '../../services/report.service';
import { ModalLoadComponent } from '../../material/modal-load/modal-load.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';
@Component({
  selector: 'app-event-admin',
  templateUrl: './event-admin.component.html',
  styleUrls: ['./event-admin.component.scss']
})
export class EventAdminComponent implements OnInit {
  offerings: EventAC[];
  concerts: EventAC[];
  publicEvents: EventAC[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  disableBtn: boolean;
  constructor(
    private eventService: EventService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private reportService: ReportService
  ) {
    this.offerings = [];
    this.concerts = [];
  }

  ngOnInit() {
    this.eventService.getAll().subscribe( events => {
      this.concerts = events.filter( event => (event.event_category_id === 1) ? event : false);
      this.offerings = events.filter( event => (event.event_category_id === 2) ? event : false);
      this.publicEvents = events.filter( event => (event.event_category_id === 3) ? event : false);
    });
  }
  downloadReport(event: EventAC, typeFile: string) {
    this.disableBtn = true;
    let contentType = '';
    let dialogRef = this.dialog.open(ModalLoadComponent, {panelClass: 'load-modal', disableClose: true});
    this.reportService.events({
      typeFile: typeFile,
      id: event.id
    }).subscribe(response => {
      this.downLoadFile(response, contentType, 'reporteEvento.' + typeFile);
      this.disableBtn = false;
      dialogRef.close();
    });
  }

  delete(event) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: event , type: 'event'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.eventService.getAll().subscribe( (events) => {
          this.concerts = events.filter( eventF => (eventF.event_category_id === 1) ? eventF : false);
          this.offerings = events.filter( eventF => (eventF.event_category_id === 2) ? eventF : false);
          this.publicEvents = events.filter( eventF => (eventF.event_category_id === 3) ? eventF : false);
        });
      }
    });
  }
  restore(event) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: event , type: 'event'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.eventService.getAll().subscribe( (events) => {
          this.concerts = events.filter( eventF => (eventF.event_category_id === 1) ? eventF : false);
          this.offerings = events.filter( eventF => (eventF.event_category_id === 2) ? eventF : false);
          this.publicEvents = events.filter( eventF => (eventF.event_category_id === 3) ? eventF : false);
        });
      }
    });
  }
  private downLoadFile(data: any, type: string, name: string) {
    const blob = new Blob([data], { type: type});
    saveAs(blob, name);
  }

}
