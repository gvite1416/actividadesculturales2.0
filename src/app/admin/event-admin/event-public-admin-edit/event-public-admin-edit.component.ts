import { Component, OnInit, ViewChild } from '@angular/core';
import { EventAC } from '../../../models/event';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl, FormArray } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Classroom } from '../../../models/classroom';
import { Role } from '../../../models/role';
import { Campus } from '../../../models/campus';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { ClassroomService } from '../../../services/classroom.service';
import { RoleService } from '../../../services/role.service';
import { CampusService } from '../../../services/campus.service';
import { EventCampus } from '../../../models/event-campus';
import { EventRole } from '../../../models/event-role';
import { Showing } from '../../../models/showing';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';

@Component({
  selector: 'app-event-public-admin-edit',
  templateUrl: './event-public-admin-edit.component.html',
  styleUrls: ['./event-public-admin-edit.component.scss']
})
export class EventPublicAdminEditComponent implements OnInit {
  event: EventAC;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  classrooms: Classroom[];
  @ViewChild('picker', {static: false}) picker;
  roleStudents: Role[];
  campus: Campus[];
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private eventService: EventService,
    private classroomService: ClassroomService,
    private formBuilder: FormBuilder,
    private roleService: RoleService,
    private route: ActivatedRoute,
    private campusService: CampusService
  ) {
    this.event = new EventAC();
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.eventService.getById(params['id']).subscribe(event => {
        this.event = event;
        this.classroomService.getAll().subscribe(classrooms => {
          this.classrooms = classrooms;
              // campus array
              const valueShowings = [];
              this.event.showings.forEach(showing => {
                this.addShowing();
                valueShowings.push({
                  id: showing.id,
                  start: showing.start,
                  finish: showing.finish,
                  start_hour: showing.start.format('HH:mm'),
                  finish_hour: showing.finish.format('HH:mm')
                });
              });
              this.editFormGroup.setValue({
                title: this.event.title,
                classroom: this.event.classroom_id,
                description: this.event.description,
                showings: valueShowings
              });
        });
      });
    });
  }
  private createForm() {
    this.editFormGroup = new FormGroup({
      title: new FormControl('', [Validators.required], checkEventValidator.bind(this)),
      classroom: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      showings: this.formBuilder.array([])
    });
    function checkEventValidator(control: AbstractControl) {
      return this.eventService.checkName(control.value, this.event.id);
    }
  }
  private createShowing(): FormGroup {
    return this.formBuilder.group({
      id: new FormControl('', []),
      start: new FormControl('', [Validators.required]),
      finish: new FormControl('', [Validators.required]),
      start_hour: new FormControl('', [Validators.required]),
      finish_hour: new FormControl('', [Validators.required])
    });
  }
  addShowing() {
    const showingsArray = this.editFormGroup.get('showings') as FormArray;
    showingsArray.push(this.createShowing());
  }
  deleteShowing(index) {
    const showingsArray = this.editFormGroup.get('showings') as FormArray;
    showingsArray.removeAt(index);
  }

  edit() {
    let formatDateHelper = new FormatDateHelper();
    this.event.title = this.editFormGroup.value.title;
    this.event.description = this.editFormGroup.value.description;
    this.event.classroom_id = this.editFormGroup.value.classroom;
    this.event.showings = this.editFormGroup.get('showings').value.map(showing => {
      showing.start.hour(formatDateHelper.getHourNum(showing.start_hour, 'HHmm'));
      showing.start.minutes(formatDateHelper.getMinNum(showing.start_hour, 'HHmm'));
      showing.finish.hour(formatDateHelper.getHourNum(showing.finish_hour, 'HHmm'));
      showing.finish.minutes(formatDateHelper.getMinNum(showing.finish_hour, 'HHmm'));
      return new Showing(showing)
    });
    this.eventService.update(this.event).subscribe((response) => {
      if (response.success) {
        this.snackBar.open('Evento editado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/eventos']);
      } else {
        for (const index in response.error) {
          if (index) {
            this.snackBar.open(response.error[index], '', {
              duration: 3000,
              verticalPosition: this.positionVertical
            });
          }
        }
      }
    });
  }

}
