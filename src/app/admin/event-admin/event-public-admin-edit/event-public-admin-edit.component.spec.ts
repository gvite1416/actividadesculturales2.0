import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPublicAdminEditComponent } from './event-public-admin-edit.component';

describe('EventPublicAdminEditComponent', () => {
  let component: EventPublicAdminEditComponent;
  let fixture: ComponentFixture<EventPublicAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventPublicAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPublicAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
