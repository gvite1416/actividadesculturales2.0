import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherAdminEditComponent } from './teacher-admin-edit.component';

describe('TeacherAdminEditComponent', () => {
  let component: TeacherAdminEditComponent;
  let fixture: ComponentFixture<TeacherAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
