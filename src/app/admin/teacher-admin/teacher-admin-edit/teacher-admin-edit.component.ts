import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Teacher } from '../../../models/teacher';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { TeacherService } from '../../../services/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-teacher-admin-edit',
  templateUrl: './teacher-admin-edit.component.html',
  styleUrls: ['./teacher-admin-edit.component.scss']
})
export class TeacherAdminEditComponent implements OnInit {
  editFormGroup: FormGroup;
  teacher: Teacher;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  genders: any;
  constructor(
    private teacherService: TeacherService,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.teacher = new Teacher();
    this.genders = [];
    this.genders.push({id: 1, name: 'Masculino'});
    this.genders.push({id: 2, name: 'Femenino'});
    this.genders.push({id: 3, name: 'Otro'});
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.teacherService.getById(params['id']).subscribe( (teacher: Teacher) => {
        this.teacher = teacher;
        this.editFormGroup.setValue({
          name: this.teacher.name,
          lastname: this.teacher.lastname,
          surname: this.teacher.surname,
          email: this.teacher.email,
          phone: this.teacher.phone,
          celphone: this.teacher.celphone,
          gender: (this.teacher.gender === 'Masculino' || this.teacher.gender === 'Femenino') ? this.teacher.gender : 'Otro',
            genderOther: (!(this.teacher.gender === 'Masculino' || this.teacher.gender === 'Femenino')) ? this.teacher.gender : ''
        });
      });
    });
    this.createForm();
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      lastname: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      surname: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.required, Validators.email], checkEmailValidator.bind(this)),
      phone: new FormControl('', []),
      celphone: new FormControl('', []),
      gender: new FormControl('', [Validators.required]),
      genderOther: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
    });
    function checkEmailValidator(control: AbstractControl) {
      return this.userService.checkEmail(control.value, this.teacher.id);
    }
  }

  edit() {
    this.teacher.name = this.editFormGroup.value.name;
    this.teacher.lastname = this.editFormGroup.value.lastname;
    this.teacher.surname = this.editFormGroup.value.surname;
    this.teacher.email = this.editFormGroup.value.email;
    this.teacher.phone = this.editFormGroup.value.phone;
    this.teacher.celphone = this.editFormGroup.value.celphone;
    this.teacher.gender = (this.editFormGroup.value.gender === 'Masculino' ||
      this.editFormGroup.value.gender === 'Femenino') ? this.editFormGroup.value.gender : this.editFormGroup.value.genderOther;
    this.teacherService.update(this.teacher).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Profesor editado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/profesores/']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
