import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherAdminComponent } from './teacher-admin.component';
import { TeacherAdminRoutingModule } from './teacher-admin-routing.module';
import { MaterialModule } from '../../material/material.module';
import { TeacherAdminNewComponent } from './teacher-admin-new/teacher-admin-new.component';
import { TeacherAdminEditComponent } from './teacher-admin-edit/teacher-admin-edit.component';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';

@NgModule({
  imports: [
    CommonModule,
    TeacherAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [TeacherAdminComponent, TeacherAdminNewComponent, TeacherAdminEditComponent]
})
export class TeacherAdminModule { }
