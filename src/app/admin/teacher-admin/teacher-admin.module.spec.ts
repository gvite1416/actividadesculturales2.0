import { TeacherAdminModule } from './teacher-admin.module';

describe('TeacherAdminModule', () => {
  let teacherAdminModule: TeacherAdminModule;

  beforeEach(() => {
    teacherAdminModule = new TeacherAdminModule();
  });

  it('should create an instance', () => {
    expect(teacherAdminModule).toBeTruthy();
  });
});
