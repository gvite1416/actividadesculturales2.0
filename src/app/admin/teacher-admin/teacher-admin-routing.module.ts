import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeacherAdminComponent } from './teacher-admin.component';
import { TeacherAdminEditComponent } from './teacher-admin-edit/teacher-admin-edit.component';
import { TeacherAdminNewComponent } from './teacher-admin-new/teacher-admin-new.component';


const routes: Routes = [
  {
    path: '',
    component: TeacherAdminComponent
  },
  {
    path: 'editar/:id',
    component: TeacherAdminEditComponent
  },
  {
    path: 'nuevo',
    component: TeacherAdminNewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherAdminRoutingModule { }
