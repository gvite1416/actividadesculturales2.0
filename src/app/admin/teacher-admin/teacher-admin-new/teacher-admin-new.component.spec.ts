import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherAdminNewComponent } from './teacher-admin-new.component';

describe('TeacherAdminNewComponent', () => {
  let component: TeacherAdminNewComponent;
  let fixture: ComponentFixture<TeacherAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
