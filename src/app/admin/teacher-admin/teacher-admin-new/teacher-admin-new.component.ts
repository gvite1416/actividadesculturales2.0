import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Teacher } from '../../../models/teacher';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { TeacherService } from '../../../services/teacher.service';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { GenpassPipe } from '../../../pipes/genpass.pipe';

@Component({
  selector: 'app-teacher-admin-new',
  templateUrl: './teacher-admin-new.component.html',
  styleUrls: ['./teacher-admin-new.component.scss'],
  providers: [GenpassPipe]
})
export class TeacherAdminNewComponent implements OnInit {
  createFormGroup: FormGroup;
  teacher: Teacher;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  genders: any;
  constructor(
    private teacherService: TeacherService,
    public snackBar: MatSnackBar,
    private router: Router,
    private userService: UserService,
    private genpassPipe: GenpassPipe
  ) {
    this.genders = [];
    this.genders.push({id: 1, name: 'Masculino'});
    this.genders.push({id: 2, name: 'Femenino'});
    this.genders.push({id: 3, name: 'Otro'});
  }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      lastname: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      surname: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.required, Validators.email], checkEmailValidator.bind(this)),
      phone: new FormControl('', []),
      celphone: new FormControl('', []),
      gender: new FormControl('', [Validators.required]),
      genderOther: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
    });
    function checkEmailValidator(control: AbstractControl) {
      return this.userService.checkEmail(control.value);
    }
  }

  add() {
    this.teacher = new Teacher(this.createFormGroup.value);
    this.teacher.password = this.genpassPipe.transform(this.createFormGroup.value.name);
    this.teacher.password_confirmation = this.genpassPipe.transform(this.createFormGroup.value.name);
    this.teacher.gender = (this.createFormGroup.value.gender === 'Masculino' ||
      this.createFormGroup.value.gender === 'Femenino') ? this.createFormGroup.value.gender : this.createFormGroup.value.genderOther;
    this.teacherService.create(this.teacher).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Profesor creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/profesores/']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
