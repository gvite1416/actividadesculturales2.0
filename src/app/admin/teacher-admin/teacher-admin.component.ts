import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService } from '../../services/teacher.service';
import { MatDialog, MatPaginator, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { Teacher } from '../../models/teacher';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-teacher-admin',
  templateUrl: './teacher-admin.component.html',
  styleUrls: ['./teacher-admin.component.scss'],
  providers: [TeacherService]
})
export class TeacherAdminComponent implements OnInit {
  teachers: Teacher[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(private teacherService: TeacherService, public dialog: MatDialog, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.chargeData();
  }
  delete(teacher) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: teacher , type: 'teacher'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  restore(teacher) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: teacher , type: 'teacher'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  onChangeEvent($event){
    this.teacherService.getAll($event.pageSize, $event.pageIndex + 1, 'name', true).subscribe((teachers) => {
      this.teachers = teachers;
      this.totalRegisters = this.teacherService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.teacherService.getAll(this.pageSize, 1, 'name', true).subscribe((teachers) => {
      this.teachers = teachers;
      this.totalRegisters = this.teacherService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }
}
