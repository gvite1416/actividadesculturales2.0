import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { Router } from '@angular/router';
import { PeriodService } from '../../../services/period.service';
import { Period } from '../../../models/period';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';
@Component({
  selector: 'app-period-admin-new',
  templateUrl: './period-admin-new.component.html',
  styleUrls: ['./period-admin-new.component.scss']
})
export class PeriodAdminNewComponent implements OnInit {

  createFormGroup: FormGroup;
  period: Period;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  @ViewChild('picker', {static: false}) picker;
  constructor(private periodService: PeriodService,
    public snackBar: MatSnackBar,
    private router: Router
    ) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required], checkPeriodValidator.bind(this)),
      start: new FormControl('' , [Validators.required]),
      finish: new FormControl('' , [Validators.required]),
      startInscription: new FormControl('' , [Validators.required]),
      startInscriptionHour: new FormControl('' , [Validators.required]),
      finishInscription: new FormControl('' , [Validators.required]),
      finishInscriptionHour: new FormControl('' , [Validators.required]),
    });
    function checkPeriodValidator(control: AbstractControl) {
      return this.periodService.checkName(control.value);
    }
  }

  add() {
    let formatDateHelper = new FormatDateHelper();
    this.createFormGroup.value.startInscription.hour(formatDateHelper.getHourNum(this.createFormGroup.value.startInscriptionHour, 'HHmm'));
    this.createFormGroup.value.startInscription.minutes(formatDateHelper.getMinNum(this.createFormGroup.value.startInscriptionHour, 'HHmm'));
    this.createFormGroup.value.finishInscription.hour(formatDateHelper.getHourNum(this.createFormGroup.value.finishInscriptionHour, 'HHmm'));
    this.createFormGroup.value.finishInscription.minutes(formatDateHelper.getMinNum(this.createFormGroup.value.finishInscriptionHour, 'HHmm'));
    this.period = new Period({
      name: this.createFormGroup.value.name,
      start: this.createFormGroup.value.start.format(),
      finish: this.createFormGroup.value.finish.format(),
      inscription_start: this.createFormGroup.value.startInscription.format(),
      inscription_finish: this.createFormGroup.value.finishInscription.format()
    });
    this.periodService.create(this.period).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Periodo creado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/periodos']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
