import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodAdminNewComponent } from './period-admin-new.component';

describe('PeriodAdminNewComponent', () => {
  let component: PeriodAdminNewComponent;
  let fixture: ComponentFixture<PeriodAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
