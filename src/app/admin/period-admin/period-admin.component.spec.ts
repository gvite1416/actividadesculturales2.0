import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodAdminComponent } from './period-admin.component';

describe('PeriodAdminComponent', () => {
  let component: PeriodAdminComponent;
  let fixture: ComponentFixture<PeriodAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
