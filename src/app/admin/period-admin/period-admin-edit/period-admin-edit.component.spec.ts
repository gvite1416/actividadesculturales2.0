import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodAdminEditComponent } from './period-admin-edit.component';

describe('PeriodAdminEditComponent', () => {
  let component: PeriodAdminEditComponent;
  let fixture: ComponentFixture<PeriodAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
