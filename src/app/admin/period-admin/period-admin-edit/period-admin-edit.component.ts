import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { PeriodService } from '../../../services/period.service';
import { Period } from '../../../models/period';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';

@Component({
  selector: 'app-period-admin-edit',
  templateUrl: './period-admin-edit.component.html',
  styleUrls: ['./period-admin-edit.component.scss']
})
export class PeriodAdminEditComponent implements OnInit {
  period: Period;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private route: ActivatedRoute,
    public snackBar: MatSnackBar,
    private router: Router,
    private periodService: PeriodService
  ) {
    this.period = new Period();
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.periodService.getById(params['id']).subscribe( (period: Period) => {
        this.period = period;
        this.editFormGroup.setValue({
          name: this.period.name,
          start: this.period.start,
          finish: this.period.finish,
          startInscription: this.period.inscription_start,
          finishInscription: this.period.inscription_finish,
          startInscriptionHour: this.period.inscription_start.format("HH:mm"),
          finishInscriptionHour: this.period.inscription_finish.format("HH:mm"),
        });
      });
    });
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required], checkPeriodValidator.bind(this)),
      start: new FormControl('' , [Validators.required]),
      finish: new FormControl('' , [Validators.required]),
      startInscription: new FormControl('' , [Validators.required]),
      startInscriptionHour: new FormControl('' , [Validators.required]),
      finishInscription: new FormControl('' , [Validators.required]),
      finishInscriptionHour: new FormControl('' , [Validators.required]),
    });
    function checkPeriodValidator(control: AbstractControl) {
      return this.periodService.checkName(control.value, this.period.id);
    }
  }
  edit() {
    let formatDateHelper = new FormatDateHelper();
    this.editFormGroup.value.startInscription.hour(formatDateHelper.getHourNum(this.editFormGroup.value.startInscriptionHour, 'HHmm'));
    this.editFormGroup.value.startInscription.minutes(formatDateHelper.getMinNum(this.editFormGroup.value.startInscriptionHour, 'HHmm'));
    this.editFormGroup.value.finishInscription.hour(formatDateHelper.getHourNum(this.editFormGroup.value.finishInscriptionHour, 'HHmm'));
    this.editFormGroup.value.finishInscription.minutes(formatDateHelper.getMinNum(this.editFormGroup.value.finishInscriptionHour, 'HHmm'));
    this.period.name = this.editFormGroup.value.name;
    this.period.start = this.editFormGroup.value.start.format();
    this.period.finish = this.editFormGroup.value.finish.format();
    this.period.inscription_start = this.editFormGroup.value.startInscription.format();
    this.period.inscription_finish = this.editFormGroup.value.finishInscription.format();
    this.periodService.update(this.period).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Periodo editado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/periodos/']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
