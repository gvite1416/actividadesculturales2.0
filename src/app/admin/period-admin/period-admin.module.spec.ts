import { PeriodAdminModule } from './period-admin.module';

describe('PeriodAdminModule', () => {
  let periodAdminModule: PeriodAdminModule;

  beforeEach(() => {
    periodAdminModule = new PeriodAdminModule();
  });

  it('should create an instance', () => {
    expect(periodAdminModule).toBeTruthy();
  });
});
