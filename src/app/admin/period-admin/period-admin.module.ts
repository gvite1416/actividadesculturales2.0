import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeriodAdminRoutingModule } from './period-admin-routing.module';
import { PeriodAdminComponent } from './period-admin.component';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { PeriodAdminNewComponent } from './period-admin-new/period-admin-new.component';
import { PipesModule } from '../../pipes/pipes.module';
import { PeriodAdminEditComponent } from './period-admin-edit/period-admin-edit.component';
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  imports: [
    CommonModule,
    PeriodAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    PipesModule,
    NgxMaskModule
  ],
  declarations: [PeriodAdminComponent, PeriodAdminNewComponent, PeriodAdminEditComponent]
})
export class PeriodAdminModule { }
