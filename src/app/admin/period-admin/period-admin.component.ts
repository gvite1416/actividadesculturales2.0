import { Component, OnInit, ViewChild } from '@angular/core';
import { Period } from '../../models/period';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog, MatPaginator } from '@angular/material';
import { PeriodService } from '../../services/period.service';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-period-admin',
  templateUrl: './period-admin.component.html',
  styleUrls: ['./period-admin.component.scss']
})
export class PeriodAdminComponent implements OnInit {
  periods: Period[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(private periodService: PeriodService, public dialog: MatDialog, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.chargeData();
  }
  onChangeEvent($event){
    this.periodService.getAll($event.pageSize, $event.pageIndex + 1, 'created_at', false).subscribe((periods) => {
      this.periods = periods;
      this.totalRegisters = this.periodService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.periodService.getAll(this.pageSize, 1, 'created_at', false).subscribe((periods) => {
      this.periods = periods;
      this.totalRegisters = this.periodService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }

  delete(period) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: period , type: 'period'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }

  restore(period) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: period , type: 'period'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }

}
