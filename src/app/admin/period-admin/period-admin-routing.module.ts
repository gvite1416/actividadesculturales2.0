import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeriodAdminComponent } from './period-admin.component';
import { PeriodAdminNewComponent } from './period-admin-new/period-admin-new.component';
import { PeriodAdminEditComponent } from './period-admin-edit/period-admin-edit.component';

const routes: Routes = [
  {
    path: '',
    component: PeriodAdminComponent
  },
  {
    path: 'nuevo',
    component: PeriodAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: PeriodAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeriodAdminRoutingModule { }
