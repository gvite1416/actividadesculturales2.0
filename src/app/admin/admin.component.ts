import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  user: User;
  constructor(
    private userService: UserService
  ) {
    this.user = new User();
    /*this.userService.userActiveCharge.subscribe((user: User) => {
      this.user = user;
    });*/
    this.userService.getActive().subscribe((user: User) => {
      this.user = user;
    });
  }

  ngOnInit() {
  }

}
