import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferingAdminNewComponent } from './offering-admin-new.component';

describe('OfferingAdminNewComponent', () => {
  let component: OfferingAdminNewComponent;
  let fixture: ComponentFixture<OfferingAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferingAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferingAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
