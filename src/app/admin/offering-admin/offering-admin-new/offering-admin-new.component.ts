import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { EventAC } from '../../../models/event';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog } from '@angular/material';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../../services/globals';
import { Offering } from '../../../models/offering';
import { OfferingService } from '../../../services/offering.service';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';

@Component({
  selector: 'app-offering-admin-new',
  templateUrl: './offering-admin-new.component.html',
  styleUrls: ['./offering-admin-new.component.scss']
})
export class OfferingAdminNewComponent implements OnInit {
  event: EventAC;
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  offering: Offering;
  fileName: string;
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventService: EventService,
    public snackBar: MatSnackBar,
    private offeringService: OfferingService,
    public dialog: MatDialog
  ) {
    this.event = new EventAC();
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.eventService.getById(params['event_id']).subscribe( event => {
        this.event = event;
      });
    });
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(response.replace('tmp/' , ''));
      this.uploader.clearQueue();
    };
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      number: new FormControl('', [
        Validators.required,
        Validators.min(1)]),
      description: new FormControl('', []),
      image: new FormControl('', [Validators.required])
    });
  }
  add() {
    this.offering = new Offering(this.createFormGroup.value);
    this.offering.image = this.fileName;
    this.offering.event_id = this.event.id;
    this.offeringService.create(this.offering).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Ofrenda creada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/ofrendas/' + this.event.id]);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }
  openModalImage(image: string) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '650px',
      data: {image, widthRatio: 1200, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileName = result || null;
    });
  }

}
