import { OfferingAdminModule } from './offering-admin.module';

describe('OfferingAdminModule', () => {
  let offeringAdminModule: OfferingAdminModule;

  beforeEach(() => {
    offeringAdminModule = new OfferingAdminModule();
  });

  it('should create an instance', () => {
    expect(offeringAdminModule).toBeTruthy();
  });
});
