import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferingAdminEditComponent } from './offering-admin-edit.component';

describe('OfferingAdminEditComponent', () => {
  let component: OfferingAdminEditComponent;
  let fixture: ComponentFixture<OfferingAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferingAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferingAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
