import { Component, OnInit } from '@angular/core';
import { Offering } from '../../../models/offering';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog } from '@angular/material';
import { OfferingService } from '../../../services/offering.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../../services/globals';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';

@Component({
  selector: 'app-offering-admin-edit',
  templateUrl: './offering-admin-edit.component.html',
  styleUrls: ['./offering-admin-edit.component.scss']
})
export class OfferingAdminEditComponent implements OnInit {
  offering: Offering;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  fileName: string;
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(
    private offeringService: OfferingService,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.offering = new Offering();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.offeringService.getById(params['id']).subscribe( offering => {
        this.offering = offering;
        this.fileName = this.offering.image;
        this.editFormGroup.setValue({
          title: this.offering.title,
          number: this.offering.number,
          description: this.offering.description,
          image: ''
        });
      });
    });
    this.createForm();
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(response.replace('tmp/' , ''));
      this.uploader.clearQueue();
    };
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      number: new FormControl('', [
        Validators.required,
        Validators.min(1)]),
      description: new FormControl('', []),
      image: new FormControl('', [])
    });
  }

  edit() {
    this.offering.title = this.editFormGroup.value.title;
    this.offering.number = this.editFormGroup.value.number;
    this.offering.description = this.editFormGroup.value.description;
    this.offering.image = this.fileName;
    this.offeringService.update(this.offering).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Ofrenda actualizada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/ofrendas/' + this.offering.event_id]);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

  openModalImage(image: string) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '650px',
      data: {image, widthRatio: 1200, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileName = result || null;
    });
  }

}
