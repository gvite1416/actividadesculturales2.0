import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfferingAdminComponent } from './offering-admin.component';
import { OfferingAdminNewComponent } from './offering-admin-new/offering-admin-new.component';
import { OfferingAdminEditComponent } from './offering-admin-edit/offering-admin-edit.component';

const routes: Routes = [
  {
    path: ':event_id',
    component: OfferingAdminComponent
  },
  {
    path: 'nuevo/:event_id',
    component: OfferingAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: OfferingAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferingAdminRoutingModule { }
