import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferingAdminComponent } from './offering-admin.component';

describe('OfferingListAdminComponent', () => {
  let component: OfferingAdminComponent;
  let fixture: ComponentFixture<OfferingAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferingAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferingAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
