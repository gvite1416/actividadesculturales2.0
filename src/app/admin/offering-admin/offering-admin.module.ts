import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferingAdminRoutingModule } from './offering-admin-routing.module';
import { OfferingAdminComponent } from './offering-admin.component';
import { MaterialModule } from '../../material/material.module';
import { OfferingAdminNewComponent } from './offering-admin-new/offering-admin-new.component';
import { OfferingAdminEditComponent } from './offering-admin-edit/offering-admin-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { FileUploadModule } from 'ng2-file-upload';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    OfferingAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    FileUploadModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    OfferingAdminComponent,
    OfferingAdminNewComponent,
    OfferingAdminEditComponent
  ]
})
export class OfferingAdminModule { }
