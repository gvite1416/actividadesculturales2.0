import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OfferingService } from '../../services/offering.service';
import { Offering } from '../../models/offering';
import { EventAC } from '../../models/event';
import { EventService } from '../../services/event.service';
import { GLOBALS } from '../../services/globals';
import { MatDialog, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-offering-admin',
  templateUrl: './offering-admin.component.html',
  styleUrls: ['./offering-admin.component.scss']
})
export class OfferingAdminComponent implements OnInit {
  offerings: Offering[];
  event: EventAC;
  storageUrl = GLOBALS.storageUrl;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private route: ActivatedRoute,
    private offeringService: OfferingService,
    private eventService: EventService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.event = new EventAC();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.eventService.getById(params['event_id']).subscribe( event => {
        this.event = event;
        this.offeringService.getAllByEvent(this.event.id).subscribe( offerings => {
          this.offerings = offerings;
        });
      });
    });
  }
  delete(offering) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: offering , type: 'offering'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.offeringService.getAll().subscribe( (offerings) => {
          this.offerings = offerings;
        });
      }
    });
  }

}
