import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InscriptionAdminValidateComponent } from './inscription-admin-validate/inscription-admin-validate.component';
import { InscriptionAdminComponent } from './inscription-admin.component';


const routes: Routes = [
  {
    path: '',
    component: InscriptionAdminComponent
  },
  {
    path: ':id',
    component: InscriptionAdminValidateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InscriptionAdminRoutingModule { }
