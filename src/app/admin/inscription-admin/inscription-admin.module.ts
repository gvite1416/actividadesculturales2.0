import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InscriptionAdminRoutingModule } from './inscription-admin-routing.module';
import { InscriptionAdminComponent } from './inscription-admin.component';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { InscriptionAdminValidateComponent } from './inscription-admin-validate/inscription-admin-validate.component';


@NgModule({
  declarations: [InscriptionAdminComponent, InscriptionAdminValidateComponent],
  imports: [
    CommonModule,
    InscriptionAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule
  ]
})
export class InscriptionAdminModule { }
