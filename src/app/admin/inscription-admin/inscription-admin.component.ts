import { Component, OnInit } from '@angular/core';
import { Inscription } from '../../models/inscription';
import { InscriptionService } from '../../services/inscription.service';

@Component({
  selector: 'app-inscription-admin',
  templateUrl: './inscription-admin.component.html',
  styleUrls: ['./inscription-admin.component.scss']
})
export class InscriptionAdminComponent implements OnInit {
  inscriptions: Inscription[];
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  pageIndex: number;
  constructor(private inscriptionService: InscriptionService) {
    this.pageSize = 10;
    this.totalRegisters = 0;
    this.pageIndex = 0;
  }

  ngOnInit() {
    this.inscriptionService.getAllActive().subscribe(inscriptions => {
      this.inscriptions = inscriptions;
      this.totalRegisters = this.inscriptionService.xTotalRegister;
    });
  }
  onChangeEvent($event){
    this.inscriptionService.getAllActive($event.pageSize, $event.pageIndex + 1).subscribe((inscriptions) => {
      this.inscriptions = inscriptions;
      this.totalRegisters = this.inscriptionService.xTotalRegister;
    });
  }
  delete(inscription){
    this.inscriptionService.delete(inscription).subscribe(() => {
      this.inscriptionService.getAllActive().subscribe(inscriptions => {
        this.inscriptions = inscriptions;
        this.totalRegisters = this.inscriptionService.xTotalRegister;
      });
    });
  }

}
