import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptionAdminValidateComponent } from './inscription-admin-validate.component';

describe('InscriptionAdminValidateComponent', () => {
  let component: InscriptionAdminValidateComponent;
  let fixture: ComponentFixture<InscriptionAdminValidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscriptionAdminValidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptionAdminValidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
