import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Inscription } from '../../../models/inscription';
import { Period } from '../../../models/period';
import { InscriptionService } from '../../../services/inscription.service';
import { PeriodService } from '../../../services/period.service';

@Component({
  selector: 'app-inscription-admin-validate',
  templateUrl: './inscription-admin-validate.component.html',
  styleUrls: ['./inscription-admin-validate.component.scss']
})
export class InscriptionAdminValidateComponent implements OnInit {
  token: string = localStorage.getItem('token');
  inscription: Inscription;
  period: Period;
  requirementsWorkshop: any;
  requirementsPeriod: any[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private inscriptionService: InscriptionService,
    private periodService: PeriodService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.inscriptionService.getById(params['id']).subscribe( (inscription: Inscription) => {
        this.inscription = inscription;
        this.periodService.getByUserPeriodRequirementsInscription(this.inscription.id).subscribe(requirements => {
          this.period = requirements.period;
          this.requirementsWorkshop = requirements.requirementsWorkshop;
          this.requirementsPeriod = requirements.requirementsPeriod;
        });
      });
    });
  }

  validate(){
    this.inscriptionService.validate(this.inscription).subscribe( () => {
      this.snackBar.open('Inscripción validada', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
      this.router.navigate(['/admin/inscripciones/']);
    });
  }

  askAgain(){
    this.inscriptionService.askAgainFiles(this.inscription).subscribe( () => {
      this.snackBar.open('Cambió estado de la inscripción', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
    });
  }

}
