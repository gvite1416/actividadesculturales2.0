import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent
  },
  {
    path: 'talleres',
    loadChildren: 'app/admin/workshop-admin/workshop-admin.module#WorkshopAdminModule'
  },
  {
    path: 'periodos',
    loadChildren: 'app/admin/period-admin/period-admin.module#PeriodAdminModule'
  },
  {
    path: 'profesores',
    loadChildren: 'app/admin/teacher-admin/teacher-admin.module#TeacherAdminModule'
  },
  {
    path: 'facultades',
    loadChildren: 'app/admin/campus-admin/campus-admin.module#CampusAdminModule'
  },
  {
    path: 'carreras',
    loadChildren: 'app/admin/career-admin/career-admin.module#CareerAdminModule'
  },
  {
    path: 'ocupaciones',
    loadChildren: 'app/admin/occupation-admin/occupation-admin.module#OccupationAdminModule'
  },
  {
    path: 'salones',
    loadChildren: 'app/admin/classroom-admin/classroom-admin.module#ClassroomAdminModule'
  },
  {
    path: 'talleres-periodo',
    loadChildren: 'app/admin/workshop-period-admin/workshop-period-admin.module#WorkshopPeriodAdminModule'
  },
  {
    path: 'requisitos',
    loadChildren: 'app/admin/requirement-admin/requirement-admin.module#RequirementAdminModule'
  },
  {
    path: 'requisitos-periodo',
    loadChildren: 'app/admin/period-requirements-admin/period-requirements-admin.module#PeriodRequirementsAdminModule'
  },
  {
    path: 'sliders',
    loadChildren: 'app/admin/slider-admin/slider-admin.module#SliderAdminModule'
  },
  {
    path: 'eventos',
    loadChildren: 'app/admin/event-admin/event-admin.module#EventAdminModule'
  },
  {
    path: 'ofrendas',
    loadChildren: 'app/admin/offering-admin/offering-admin.module#OfferingAdminModule'
  },
  {
    path: 'usuarios',
    loadChildren: 'app/admin/users-admin/users-admin.module#UsersAdminModule'
  },
  {
    path: 'talento-aragones',
    loadChildren: 'app/admin/talent-admin/talent-admin.module#TalentAdminModule'
  },
  {
    path: 'reportes',
    loadChildren: 'app/admin/report-admin/report-admin.module#ReportAdminModule'
  },
  {
    path: 'lineas-de-captura',
    loadChildren: 'app/admin/captureline-admin/captureline-admin.module#CapturelineAdminModule'
  },
  {
    path: 'inscripciones',
    loadChildren: 'app/admin/inscription-admin/inscription-admin.module#InscriptionAdminModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
