import { CareerAdminModule } from './career-admin.module';

describe('CareerAdminModule', () => {
  let careerAdminModule: CareerAdminModule;

  beforeEach(() => {
    careerAdminModule = new CareerAdminModule();
  });

  it('should create an instance', () => {
    expect(careerAdminModule).toBeTruthy();
  });
});
