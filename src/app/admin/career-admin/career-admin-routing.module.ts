import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CareerAdminComponent } from './career-admin.component';
import { CareerAdminNewComponent } from './career-admin-new/career-admin-new.component';
import { CareerAdminEditComponent } from './career-admin-edit/career-admin-edit.component';

const routes: Routes = [
  {
    path: '',
    component: CareerAdminComponent
  },
  {
    path: 'nuevo',
    component: CareerAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: CareerAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareerAdminRoutingModule { }
