import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerAdminNewComponent } from './career-admin-new.component';

describe('CareerAdminNewComponent', () => {
  let component: CareerAdminNewComponent;
  let fixture: ComponentFixture<CareerAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
