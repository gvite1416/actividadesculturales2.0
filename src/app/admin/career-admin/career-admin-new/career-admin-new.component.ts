import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Career } from '../../../models/career';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { CareerService } from '../../../services/career.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-career-admin-new',
  templateUrl: './career-admin-new.component.html',
  styleUrls: ['./career-admin-new.component.scss']
})
export class CareerAdminNewComponent implements OnInit {
  createFormGroup: FormGroup;
  career: Career;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private careerService: CareerService,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)],
        checkCareerValidator.bind(this)),
      code: new FormControl('', [])
    });
    function checkCareerValidator(control: AbstractControl) {
      return this.careerService.checkName(control.value);
    }
  }
  add() {
    this.career = new Career(this.createFormGroup.value);
    this.careerService.create(this.career).subscribe( (response) => {
        this.snackBar.open('Carrera creada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/carreras/']);
    }, response => {
      if (response.status == 400){
        for (const index in response.error.message) {
          if (index) {
            response.error.message[index].forEach(error => {
              this.snackBar.open(error, '', {
                duration: 3000,
              });
            });
          }
        }
      }
    });
  }

}
