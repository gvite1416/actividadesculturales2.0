import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBarVerticalPosition, MatDialog, MatSnackBar, MatPaginator } from '@angular/material';
import { CareerService } from '../../services/career.service';
import { Career } from '../../models/career';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-career-admin',
  templateUrl: './career-admin.component.html',
  styleUrls: ['./career-admin.component.scss']
})
export class CareerAdminComponent implements OnInit {
  careers: Career[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(
    private careerService: CareerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }
  ngOnInit() {
    this.chargeData();
  }
  onChangeEvent($event){
    this.careerService.getAll($event.pageSize, $event.pageIndex + 1, 'name', true).subscribe((careers) => {
      this.careers = careers;
      this.totalRegisters = this.careerService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.careerService.getAll(this.pageSize, 1, 'name', true).subscribe((careers) => {
      this.careers = careers;
      this.totalRegisters = this.careerService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }

  delete(career) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: career , type: 'career'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  restore(career) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: career , type: 'career'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.chargeData();
      }
    });
  }


}
