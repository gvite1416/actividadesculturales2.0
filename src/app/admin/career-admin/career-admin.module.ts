import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CareerAdminRoutingModule } from './career-admin-routing.module';
import { CareerAdminComponent } from './career-admin.component';
import { CareerAdminNewComponent } from './career-admin-new/career-admin-new.component';
import { CareerAdminEditComponent } from './career-admin-edit/career-admin-edit.component';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { NgxMaskModule } from 'ngx-mask';
import { CampusService } from '../../services/campus.service';

@NgModule({
  imports: [
    CommonModule,
    CareerAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [CareerAdminComponent, CareerAdminNewComponent, CareerAdminEditComponent],
  providers: [CampusService]
})
export class CareerAdminModule { }
