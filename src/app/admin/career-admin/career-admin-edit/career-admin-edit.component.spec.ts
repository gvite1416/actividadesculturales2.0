import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerAdminEditComponent } from './career-admin-edit.component';

describe('CareerAdminEditComponent', () => {
  let component: CareerAdminEditComponent;
  let fixture: ComponentFixture<CareerAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
