import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Career } from '../../../models/career';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { CareerService } from '../../../services/career.service';

@Component({
  selector: 'app-career-admin-edit',
  templateUrl: './career-admin-edit.component.html',
  styleUrls: ['./career-admin-edit.component.scss']
})
export class CareerAdminEditComponent implements OnInit {
  editFormGroup: FormGroup;
  career: Career;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private careerService: CareerService,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.career = new Career();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.careerService.getById(params['id']).subscribe( (career: Career) => {
        this.career = career;
        this.editFormGroup.setValue({
          name: this.career.name,
          code: this.career.code,
          college_degree_option: this.career.college_degree_option,
        });
      });
    });
    this.createForm();
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)],
        checkCareerValidator.bind(this)),
      code: new FormControl('', []),
      college_degree_option: new FormControl(false,[])
    });
    function checkCareerValidator(control: AbstractControl) {
      return this.careerService.checkName(control.value, this.career.id);
    }
  }
  edit() {
    this.career.name = this.editFormGroup.value.name;
    this.career.code = this.editFormGroup.value.code;
    this.career.college_degree_option = this.editFormGroup.value.college_degree_option;
    this.careerService.update(this.career).subscribe( (response) => {
      this.snackBar.open('Carrera editada', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
      this.router.navigate(['/admin/carreras/']);
    }, response => {
      if (response.status == 400){
        for (const index in response.error.message) {
          if (index) {
            response.error.message[index].forEach(error => {
              this.snackBar.open(error, '', {
                duration: 3000,
              });
            });
          }
        }
      }
    });
  }

}
