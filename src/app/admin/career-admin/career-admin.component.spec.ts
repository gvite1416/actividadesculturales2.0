import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerAdminComponent } from './career-admin.component';

describe('CareerAdminComponent', () => {
  let component: CareerAdminComponent;
  let fixture: ComponentFixture<CareerAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
