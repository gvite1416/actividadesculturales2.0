import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalentAdminShowComponent } from './talent-admin-show.component';

describe('TalentAdminShowComponent', () => {
  let component: TalentAdminShowComponent;
  let fixture: ComponentFixture<TalentAdminShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TalentAdminShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalentAdminShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
