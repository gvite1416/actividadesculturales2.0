import { Component, OnInit } from '@angular/core';
import { TalentService } from '../../../services/talent.service';
import { ActivatedRoute } from '@angular/router';
import { Talent } from '../../../models/talent';
import { Career } from '../../../models/career';

@Component({
  selector: 'app-talent-admin-show',
  templateUrl: './talent-admin-show.component.html',
  styleUrls: ['./talent-admin-show.component.scss']
})
export class TalentAdminShowComponent implements OnInit {
  talent: Talent;
  constructor(
    private talentService: TalentService,
    private route: ActivatedRoute
  ) {
    this.talent = new Talent();
    this.talent.career = new Career();
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.talentService.getById(params['id']).subscribe((talent) => {
        this.talent = talent;
      });
    });
  }

}
