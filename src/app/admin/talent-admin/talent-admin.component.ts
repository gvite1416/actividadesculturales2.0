import { Component, OnInit } from '@angular/core';
import { TalentService } from '../../services/talent.service';
import { Talent } from '../../models/talent';
import { GLOBALS } from '../../services/globals';

@Component({
  selector: 'app-talent-admin',
  templateUrl: './talent-admin.component.html',
  styleUrls: ['./talent-admin.component.scss']
})
export class TalentAdminComponent implements OnInit {

  talents: Talent[];
  urlCSV: string;
  constructor(
    private talentService: TalentService
  ) {
    this.talents = [];
    this.urlCSV = GLOBALS.apiUrl + 'talent/export-csv?token=' + localStorage.getItem('token');
  }

  ngOnInit() {
    this.talentService.getAll().subscribe((talents) => {
      this.talents = talents;
    });
  }

}
