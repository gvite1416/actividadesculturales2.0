import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TalentAdminComponent } from './talent-admin.component';
import { TalentAdminShowComponent } from './talent-admin-show/talent-admin-show.component';


const routes: Routes = [
  {
    path: '',
    component: TalentAdminComponent
  },
  {
    path: ':id',
    component: TalentAdminShowComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TalentAdminRoutingModule { }
