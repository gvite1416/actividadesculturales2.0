import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TalentAdminRoutingModule } from './talent-admin-routing.module';
import { TalentAdminComponent } from './talent-admin.component';
import { MaterialModule } from '../../material/material.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TalentAdminShowComponent } from './talent-admin-show/talent-admin-show.component';

@NgModule({
  imports: [
    CommonModule,
    TalentAdminRoutingModule,
    MaterialModule,
    PipesModule
  ],
  declarations: [TalentAdminComponent, TalentAdminShowComponent]
})
export class TalentAdminModule { }
