import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalentAdminComponent } from './talent-admin.component';

describe('TalentComponent', () => {
  let component: TalentAdminComponent;
  let fixture: ComponentFixture<TalentAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TalentAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalentAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
