import { Component, OnInit } from '@angular/core';
import { CapturelineService } from '../../services/captureline.service';
import { Captureline } from '../../models/captureline';
import { GLOBALS } from '../../services/globals';
import { MatDialog, MatPaginator, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { ViewChild } from '@angular/core';
import { Period } from '../../models/period';
import { PeriodService } from '../../services/period.service';

@Component({
  selector: 'app-captureline-admin',
  templateUrl: './captureline-admin.component.html',
  styleUrls: ['./captureline-admin.component.scss']
})
export class CapturelineAdminComponent implements OnInit {
  capturelines: Captureline[];
  token: string = localStorage.getItem('token');
  storageUrl: string = GLOBALS.storageUrl;
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  cpValues: number[];
  cpValue: number;
  cpUse: string;
  cpPeriodId: number;
  periods: Period[];
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(
    private capturelineService: CapturelineService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public periodService: PeriodService
  ) {
    this.pageSize = 10;
    this.totalRegisters = 0;
  }

  ngOnInit() {
    this.cpUse = 'all';
    this.cpValue = -1;
    this.cpPeriodId = -1;
    this.periodService.getAll().subscribe(periods => {
      this.periods = periods;
      this.chargeData();
    });
  }
  onChangeEvent($event){
    this.capturelineService.getAll($event.pageSize, $event.pageIndex + 1, this.cpValue, this.cpUse, this.cpPeriodId).subscribe((capturelines) => {
      this.capturelines = capturelines;
      this.totalRegisters = this.capturelineService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    
    this.capturelineService.getAll(this.pageSize, 1, this.cpValue, this.cpUse, this.cpPeriodId).subscribe((capturelines) => {
      this.capturelines = capturelines;
      this.totalRegisters = this.capturelineService.xTotalRegister;
      this.cpValues = this.capturelineService.xCapturelinesValues;
      this.paginator.pageIndex = 0;
    });
  }
  delete(captureline: Captureline, force: boolean) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: captureline , type: 'captureline', force: force}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.chargeData();
      }
    });
  }

}
