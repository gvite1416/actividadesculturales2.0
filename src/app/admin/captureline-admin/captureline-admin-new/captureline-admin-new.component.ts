import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../../services/globals';
import * as moment from 'moment';
import { Period } from '../../../models/period';
import { PeriodService } from '../../../services/period.service';
@Component({
  selector: 'app-captureline-admin-new',
  templateUrl: './captureline-admin-new.component.html',
  styleUrls: ['./captureline-admin-new.component.scss']
})
export class CapturelineAdminNewComponent implements OnInit {
  value: number;
  date_valid: moment.Moment;
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'captureline',
    itemAlias: 'captureline',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  fileName: string[];
  filesAdd: any[];
  periods: Period[];
  period_id: number;
  constructor(private periodService: PeriodService) {
    this.fileName = [];
    this.filesAdd = [];
    this.value = 0;
    this.period_id = -1;
  }

  ngOnInit() {
    this.uploader.onAfterAddingAll = (files) => {
      this.filesAdd = files;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.fileName.push(response);
      // this.uploader.clearQueue();
    };
    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      form.append('value', this.value);
      this.date_valid.seconds(59);
      this.date_valid.minute(59);
      this.date_valid.hour(23);
      form.append('date_valid', this.date_valid.format());
      form.append('period_id', this.period_id);
    };
    this.periodService.getAll().subscribe(periods => {
      this.periods = periods;
    })

  }
  clean() {
    this.uploader.clearQueue();
    this.filesAdd = [];
  }

}
