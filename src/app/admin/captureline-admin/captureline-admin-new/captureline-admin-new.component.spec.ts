import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapturelineAdminNewComponent } from './captureline-admin-new.component';

describe('CapturelineAdminNewComponent', () => {
  let component: CapturelineAdminNewComponent;
  let fixture: ComponentFixture<CapturelineAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapturelineAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapturelineAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
