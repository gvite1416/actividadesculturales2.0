import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapturelineAdminEditComponent } from './captureline-admin-edit.component';

describe('CapturelineAdminEditComponent', () => {
  let component: CapturelineAdminEditComponent;
  let fixture: ComponentFixture<CapturelineAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapturelineAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapturelineAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
