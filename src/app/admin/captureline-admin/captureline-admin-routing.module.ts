import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CapturelineAdminComponent } from './captureline-admin.component';
import { CapturelineAdminNewComponent } from './captureline-admin-new/captureline-admin-new.component';
import { CapturelineAdminEditComponent } from './captureline-admin-edit/captureline-admin-edit.component';


const routes: Routes = [
  {
    path: '',
    component: CapturelineAdminComponent
  },
  {
    path: 'nuevo',
    component: CapturelineAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: CapturelineAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CapturelineAdminRoutingModule { }
