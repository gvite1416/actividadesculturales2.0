import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapturelineAdminComponent } from './captureline-admin.component';

describe('CapturelineAdminComponent', () => {
  let component: CapturelineAdminComponent;
  let fixture: ComponentFixture<CapturelineAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapturelineAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapturelineAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
