import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CapturelineAdminRoutingModule } from './captureline-admin-routing.module';
import { CapturelineAdminComponent } from './captureline-admin.component';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CapturelineAdminNewComponent } from './captureline-admin-new/captureline-admin-new.component';
import { CapturelineAdminEditComponent } from './captureline-admin-edit/captureline-admin-edit.component';
import { CapturelineService } from '../../services/captureline.service';
import { FileUploadModule } from 'ng2-file-upload';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { PipesModule } from '../../pipes/pipes.module';


@NgModule({
  declarations: [CapturelineAdminComponent, CapturelineAdminNewComponent, CapturelineAdminEditComponent],
  imports: [
    CommonModule,
    CapturelineAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    DialogsModule,
    PipesModule
  ],
  providers: [CapturelineService]
})
export class CapturelineAdminModule { }
