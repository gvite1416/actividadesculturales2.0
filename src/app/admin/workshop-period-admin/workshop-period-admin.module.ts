import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkshopPeriodAdminRoutingModule } from './workshop-period-admin-routing.module';
import { WorkshopPeriodAdminComponent } from './workshop-period-admin.component';
import { PeriodService } from '../../services/period.service';
import { MaterialModule } from '../../material/material.module';
import { PipesModule } from '../../pipes/pipes.module';
import { WorkshopPeriodAdminNewComponent } from './workshop-period-admin-new/workshop-period-admin-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { WorkshopPeriodAdminEditComponent } from './workshop-period-admin-edit/workshop-period-admin-edit.component';
import { WorkshopPeriodAdminInscriptionsComponent } from './workshop-period-admin-inscriptions/workshop-period-admin-inscriptions.component';
import { WorkshopPeriodAdminInscribeComponent } from './workshop-period-admin-inscribe/workshop-period-admin-inscribe.component';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    WorkshopPeriodAdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    MaterialModule,
    PipesModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [WorkshopPeriodAdminComponent, WorkshopPeriodAdminNewComponent, WorkshopPeriodAdminEditComponent, WorkshopPeriodAdminInscriptionsComponent, WorkshopPeriodAdminInscribeComponent],
  providers: [PeriodService]
})
export class WorkshopPeriodAdminModule { }
