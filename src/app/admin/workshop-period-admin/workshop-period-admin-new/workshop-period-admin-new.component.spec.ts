import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopPeriodAdminNewComponent } from './workshop-period-admin-new.component';

describe('WorkshopPeriodAdminNewComponent', () => {
  let component: WorkshopPeriodAdminNewComponent;
  let fixture: ComponentFixture<WorkshopPeriodAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopPeriodAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopPeriodAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
