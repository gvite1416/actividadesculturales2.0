import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WorkshopPeriod } from '../../../models/workshop-period';
import { ClassroomService } from '../../../services/classroom.service';
import { Classroom } from '../../../models/classroom';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder, FormArray } from '@angular/forms';
import { Workshop } from '../../../models/workshop';
import { WorkshopService } from '../../../services/workshop.service';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { Router } from '@angular/router';
import { TeacherService } from '../../../services/teacher.service';
import { Teacher } from '../../../models/teacher';
import { RoleService } from '../../../services/role.service';
import { Role } from '../../../models/role';
import { Schedule } from '../../../models/schedule';
import { WorkshopPeriodRole } from '../../../models/workshop-period-role';
import { WorkshopPeriodService } from '../../../services/workshop-period.service';
import { CampusService } from '../../../services/campus.service';
import { Campus } from '../../../models/campus';
import { WorkshopPeriodCampus } from '../../../models/workshop-period-campus';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';

@Component({
  selector: 'app-workshop-period-admin-new',
  templateUrl: './workshop-period-admin-new.component.html',
  styleUrls: ['./workshop-period-admin-new.component.scss']
})
export class WorkshopPeriodAdminNewComponent implements OnInit {
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  workshopPeriod: WorkshopPeriod;
  period_id: number;
  classrooms: Classroom[];
  workshops: Workshop[];
  teachers: Teacher[];
  roleStudents: Role[];
  days: string[];
  campus: Campus[];
  constructor(
    private workshopPeriodService: WorkshopPeriodService,
    private classroomService: ClassroomService,
    private workshopService: WorkshopService,
    private teacherService: TeacherService,
    private roleService: RoleService,
    private campusService: CampusService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.workshopPeriod = new WorkshopPeriod();
    this.days = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.workshopPeriod.period_id = params['period_id'];
    });
    this.classroomService.getAll().subscribe( classrooms => this.classrooms = classrooms);
    this.workshopService.getAll().subscribe( workshops => this.workshops = workshops);
    this.teacherService.getAll().subscribe( teachers => this.teachers = teachers);
    this.campusService.getAll().subscribe( campus => {
      this.campus = campus;
      const campusArray = this.createFormGroup.get('campus') as FormArray;
      this.campus.forEach(x => {
        campusArray.push(this.formBuilder.group({
          camp: new FormControl(false),
          id: new FormControl(x.id),
        }));
      });
    });
    this.roleService.getStudents().subscribe( roles => {
      this.roleStudents = roles;
      const studentsArray = this.createFormGroup.get('students') as FormArray;
      this.roleStudents.forEach(x => {
        studentsArray.push(this.formBuilder.group({
          student: new FormControl(false),
          id: new FormControl(x.id),
          price: new FormControl(''),
          limit: new FormControl('')
        }));
      });
    });
  }
  private createForm() {
    this.createFormGroup = new FormGroup({
      workshop: new FormControl('', [Validators.required]),
      teacher: new FormControl('', [Validators.required]),
      classroom: new FormControl('', [Validators.required]),
      students: this.formBuilder.array([]),
      schedules: this.formBuilder.array([]),
      campus: this.formBuilder.array([]),
      all_campus: new FormControl(true, []),
      quota: new FormControl('', [Validators.required]),
      group: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ-]/g)]),
      notes: new FormControl('', [])
    });
    const schedulesArray = this.createFormGroup.get('schedules') as FormArray;
    for (let i = 0 ; i < this.days.length ; i++) {
      schedulesArray.push(this.formBuilder.group({
        day: new FormControl(false),
        start: new FormControl(''),
        finish: new FormControl('')
      }));
    }
  }
  add() {
    let formatDateHelper = new FormatDateHelper();
    this.workshopPeriod.classroom_id = this.createFormGroup.value.classroom;
    this.workshopPeriod.teacher_id = this.createFormGroup.value.teacher;
    this.workshopPeriod.workshop_id = this.createFormGroup.value.workshop;
    this.workshopPeriod.group = this.createFormGroup.value.group;
    this.workshopPeriod.quota = this.createFormGroup.value.quota;
    this.workshopPeriod.all_campus = this.createFormGroup.value.all_campus;
    this.workshopPeriod.notes = this.createFormGroup.value.notes;
    this.workshopPeriod.schedules = [];
    const scheldures = this.createFormGroup.get('schedules').value;
    scheldures.forEach((scheldure, index) => {
      if (scheldure.day) {
        this.workshopPeriod.schedules.push( new Schedule({
          day: index + 1,
          start: (scheldure.start ) ? formatDateHelper.getHourFormat(scheldure.start, 'HH:mm:ss') : '00:00:00',
          finish: (scheldure.finish ) ? formatDateHelper.getHourFormat(scheldure.finish, 'HH:mm:ss') : '00:00:00'
        }));
      }
    });
    const students = this.createFormGroup.get('students').value;
    this.workshopPeriod.students = [];
    students.forEach((student) => {
      if (student.student) {
        this.workshopPeriod.students.push( new WorkshopPeriodRole({
          id: student.id,
          pivot: {
            limit: student.limit ? student.limit : -1,
            price: student.price ? student.price : 0
          }
        }));
      }
    });

    const campus = this.createFormGroup.get('campus').value;
    this.workshopPeriod.campus = [];
    campus.forEach((camp) => {
      if (camp.camp) {
        this.workshopPeriod.campus.push(new WorkshopPeriodCampus({
          id: camp.id
        }));
      }
    });
    this.workshopPeriodService.create(this.workshopPeriod).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Se ha dado de alta el taller en el periodo', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/talleres-periodo/' + this.workshopPeriod.period_id]);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
