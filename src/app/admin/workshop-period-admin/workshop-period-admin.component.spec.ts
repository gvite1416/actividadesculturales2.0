import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopPeriodAdminComponent } from './workshop-period-admin.component';

describe('WorkshopPeriodAdminComponent', () => {
  let component: WorkshopPeriodAdminComponent;
  let fixture: ComponentFixture<WorkshopPeriodAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopPeriodAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopPeriodAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
