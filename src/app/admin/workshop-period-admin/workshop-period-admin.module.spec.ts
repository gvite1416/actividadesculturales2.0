import { WorkshopPeriodAdminModule } from './workshop-period-admin.module';

describe('WorkshopPeriodAdminModule', () => {
  let workshopPeriodAdminModule: WorkshopPeriodAdminModule;

  beforeEach(() => {
    workshopPeriodAdminModule = new WorkshopPeriodAdminModule();
  });

  it('should create an instance', () => {
    expect(workshopPeriodAdminModule).toBeTruthy();
  });
});
