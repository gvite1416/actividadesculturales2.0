import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkshopPeriodAdminComponent } from './workshop-period-admin.component';
import { WorkshopPeriodAdminNewComponent } from './workshop-period-admin-new/workshop-period-admin-new.component';
import { WorkshopPeriodAdminEditComponent } from './workshop-period-admin-edit/workshop-period-admin-edit.component';
import { WorkshopPeriodAdminInscriptionsComponent } from './workshop-period-admin-inscriptions/workshop-period-admin-inscriptions.component';
import { WorkshopPeriodAdminInscribeComponent } from './workshop-period-admin-inscribe/workshop-period-admin-inscribe.component';

const routes: Routes = [
  {
    path: ':period_id',
    component: WorkshopPeriodAdminComponent
  },
  {
    path: ':period_id/nuevo',
    component: WorkshopPeriodAdminNewComponent
  },
  {
    path: ':period_id/editar/:id',
    component: WorkshopPeriodAdminEditComponent
  },
  {
    path: ':period_id/alumnos/:id',
    component: WorkshopPeriodAdminInscriptionsComponent
  },
  {
    path: ':period_id/alumnos/:id/inscribir',
    component: WorkshopPeriodAdminInscribeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkshopPeriodAdminRoutingModule { }
