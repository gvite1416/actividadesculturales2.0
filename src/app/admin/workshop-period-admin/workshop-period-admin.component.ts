import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WorkshopPeriod } from '../../models/workshop-period';
import { WorkshopPeriodService } from '../../services/workshop-period.service';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { MatDialog, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { GLOBALS } from '../../services/globals';

@Component({
  selector: 'app-workshop-period-admin',
  templateUrl: './workshop-period-admin.component.html',
  styleUrls: ['./workshop-period-admin.component.scss']
})
export class WorkshopPeriodAdminComponent implements OnInit {
  workshopPeriods: WorkshopPeriod[];
  period_id: number;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  urlApi: string;
  token: string;
  constructor(
    private workshopPeriodService: WorkshopPeriodService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.urlApi = GLOBALS.apiUrl;
    this.token = localStorage.getItem('token');
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.period_id = params['period_id'];
      this.workshopPeriodService.getAllByPeriodId(this.period_id).subscribe ( (workshopPeriods) => {
        this.workshopPeriods = workshopPeriods;
      });
    });
  }

  delete(workshopPeriod) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: workshopPeriod , type: 'workshopPeriod'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.workshopPeriodService.getAllByPeriodId(this.period_id).subscribe ( (workshopPeriods) => {
          this.workshopPeriods = workshopPeriods;
        });
      }
    });
  }

}
