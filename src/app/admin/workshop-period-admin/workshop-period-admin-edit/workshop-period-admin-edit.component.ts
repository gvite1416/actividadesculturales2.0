import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkshopService } from '../../../services/workshop.service';
import { WorkshopPeriodService } from '../../../services/workshop-period.service';
import { WorkshopPeriod } from '../../../models/workshop-period';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Classroom } from '../../../models/classroom';
import { Workshop } from '../../../models/workshop';
import { Teacher } from '../../../models/teacher';
import { Role } from '../../../models/role';
import { ClassroomService } from '../../../services/classroom.service';
import { TeacherService } from '../../../services/teacher.service';
import { RoleService } from '../../../services/role.service';
import { Schedule } from '../../../models/schedule';
import { WorkshopPeriodRole } from '../../../models/workshop-period-role';
import { CampusService } from '../../../services/campus.service';
import { Campus } from '../../../models/campus';
import { WorkshopPeriodCampus } from '../../../models/workshop-period-campus';
import { FormatDateHelper } from '../../../helpers/formatDate.helper';

@Component({
  selector: 'app-workshop-period-admin-edit',
  templateUrl: './workshop-period-admin-edit.component.html',
  styleUrls: ['./workshop-period-admin-edit.component.scss']
})
export class WorkshopPeriodAdminEditComponent implements OnInit {
  period_id: number;
  workshopPeriod: WorkshopPeriod;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  classrooms: Classroom[];
  workshops: Workshop[];
  teachers: Teacher[];
  roleStudents: Role[];
  days: string[];
  campus: Campus[];
  constructor(
    private workshopPeriodService: WorkshopPeriodService,
    private classroomService: ClassroomService,
    private workshopService: WorkshopService,
    private teacherService: TeacherService,
    private roleService: RoleService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private campusService: CampusService
  ) {
    this.days = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.period_id = params['period_id'];
      this.classroomService.getAll().subscribe( classrooms => {
        this.classrooms = classrooms;
        this.workshopService.getAll().subscribe( workshops => {
          this.workshops = workshops;
          this.teacherService.getAll().subscribe( teachers => {
            this.teachers = teachers;
            this.campusService.getAll().subscribe(campus => {
              this.roleService.getStudents().subscribe( roles => {
                this.workshopPeriodService.getById(params['id']).subscribe ( (workshopPeriod) => {
                  this.roleStudents = roles;
                  this.campus = campus;
                  this.workshopPeriod = workshopPeriod;
                  const studentsArray = this.editFormGroup.get('students') as FormArray;
                  const valueStudents = [];
                  this.roleStudents.forEach(x => {
                    valueStudents.push({
                      student: false,
                      id: x.id,
                      price: '',
                      limit: ''
                    });
                    studentsArray.push(this.formBuilder.group({
                      student: new FormControl(false),
                      id: new FormControl(x.id),
                      price: new FormControl(''),
                      limit: new FormControl('')
                    }));
                  });
                  this.workshopPeriod.students.forEach( (student) => {
                    valueStudents.forEach( (s) => {
                      if (s.id === student.id) {
                        s.id = student.id;
                        s.limit = student.pivot.limit;
                        s.price = student.pivot.price;
                        s.student = true;
                      }
                    });
                  });
                  const valueSchedules = [];
                  this.days.forEach( (d, index) => {
                    let find = false;
                    let value = {};
                    this.workshopPeriod.schedules.forEach( (schedule) => {
                      if ((index + 1) === schedule.day) {
                        find = true;
                        value = {
                          id: schedule.id,
                          day: true,
                          start: schedule.start.format('HH:mm'),
                          finish: schedule.finish.format('HH:mm')
                        };
                      }
                    });
                    if (find) {
                      valueSchedules.push(value);
                    } else {
                      valueSchedules.push({
                        id: '',
                        day: false,
                        start: '',
                        finish: ''
                      });
                    }
                  });

                  // campus array
                  const campusArray = this.editFormGroup.get('campus') as FormArray;
                  const valueCampus = [];
                  this.campus.forEach(x => {
                    valueCampus.push({
                      camp: false,
                      id: x.id
                    });
                    campusArray.push(this.formBuilder.group({
                      camp: new FormControl(false),
                      id: new FormControl(''),
                    }));
                  });
                  this.workshopPeriod.campus.forEach((camp) => {
                    valueCampus.forEach((s) => {
                      if (s.id === camp.id) {
                        s.id = camp.id;
                        s.camp = true;
                      }
                    });
                  });
                  this.editFormGroup.setValue({
                    workshop: this.workshopPeriod.workshop_id,
                    teacher: this.workshopPeriod.teacher_id,
                    classroom: this.workshopPeriod.classroom_id,
                    quota: this.workshopPeriod.quota,
                    group: this.workshopPeriod.group,
                    students: valueStudents,
                    all_campus: this.workshopPeriod.all_campus,
                    notes: this.workshopPeriod.notes,
                    schedules: valueSchedules,
                    campus: valueCampus
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      workshop: new FormControl('', [Validators.required]),
      teacher: new FormControl('', [Validators.required]),
      classroom: new FormControl('', [Validators.required]),
      students: this.formBuilder.array([]),
      schedules: this.formBuilder.array([]),
      campus: this.formBuilder.array([]),
      all_campus: new FormControl(false, []),
      notes: new FormControl('', []),
      quota: new FormControl('', [Validators.required]),
      group: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ-]/g)])
    });
    const schedulesArray = this.editFormGroup.get('schedules') as FormArray;
    for (let i = 0 ; i < this.days.length ; i++) {
      schedulesArray.push(this.formBuilder.group({
        id: new FormControl(''),
        day: new FormControl(false),
        start: new FormControl(''),
        finish: new FormControl('')
      }));
    }
  }
  edit() {
    let formatDateHelper = new FormatDateHelper();
    this.workshopPeriod.classroom_id = this.editFormGroup.value.classroom;
    this.workshopPeriod.teacher_id = this.editFormGroup.value.teacher;
    this.workshopPeriod.workshop_id = this.editFormGroup.value.workshop;
    this.workshopPeriod.group = this.editFormGroup.value.group;
    this.workshopPeriod.quota = this.editFormGroup.value.quota;
    this.workshopPeriod.all_campus = this.editFormGroup.value.all_campus;
    this.workshopPeriod.notes = this.editFormGroup.value.notes;
    this.workshopPeriod.schedules = [];
    const scheldures = this.editFormGroup.get('schedules').value;
    scheldures.forEach((scheldure, index) => {
      if (scheldure.day) {
        this.workshopPeriod.schedules.push( new Schedule({
          id: scheldure.id,
          day: index + 1,
          start: (scheldure.start ) ? formatDateHelper.getHourFormat(scheldure.start, 'HH:mm:ss') : '00:00:00',
          finish: (scheldure.finish ) ? formatDateHelper.getHourFormat(scheldure.finish, 'HH:mm:ss') : '00:00:00'
        }));
      }
    });
    const students = this.editFormGroup.get('students').value;
    this.workshopPeriod.students = [];
    students.forEach((student, index) => {
      if (student.student) {
        this.workshopPeriod.students.push( new WorkshopPeriodRole({
          id: student.id,
          pivot: {
            limit: student.limit,
            price: student.price
          }
        }));
      }
    });
    const campus = this.editFormGroup.get('campus').value;
    this.workshopPeriod.campus = [];
    campus.forEach((camp) => {
      if (camp.camp) {
        this.workshopPeriod.campus.push(new WorkshopPeriodCampus({
          id: camp.id
        }));
      }
    });
    this.workshopPeriodService.update(this.workshopPeriod).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Se ha guardado correctamente el taller periodo', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/talleres-periodo/' + this.workshopPeriod.period_id]);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
