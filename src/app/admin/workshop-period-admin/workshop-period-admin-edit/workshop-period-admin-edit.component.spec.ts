import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopPeriodAdminEditComponent } from './workshop-period-admin-edit.component';

describe('WorkshopPeriodAdminEditComponent', () => {
  let component: WorkshopPeriodAdminEditComponent;
  let fixture: ComponentFixture<WorkshopPeriodAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopPeriodAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopPeriodAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
