import { Component, OnInit } from '@angular/core';
import { WorkshopPeriodService } from '../../../services/workshop-period.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../models/user';
import { WorkshopPeriod } from '../../../models/workshop-period';
import { Workshop } from '../../../models/workshop';
import { Schedule } from '../../../models/schedule';
import { Teacher } from '../../../models/teacher';
import { MatDialog, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { ChangeWorkshopDialogComponent } from '../../../dialogs/change-workshop-dialog/change-workshop-dialog.component';
import { Inscription } from '../../../models/inscription';
import { InscriptionDialogComponent } from '../../../dialogs/inscription-dialog/inscription-dialog.component';
import { InscriptionService } from '../../../services/inscription.service';
import { DeleteDialogComponent } from '../../../dialogs/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-workshop-period-admin-inscriptions',
  templateUrl: './workshop-period-admin-inscriptions.component.html',
  styleUrls: ['./workshop-period-admin-inscriptions.component.scss']
})
export class WorkshopPeriodAdminInscriptionsComponent implements OnInit {
  registers: any[];
  workshopPeriods: WorkshopPeriod[];
  workshopPeriod: WorkshopPeriod;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  period_id: number;
  token: string = localStorage.getItem('token');
  id: number;
  blockButtons = false;
  constructor(
    private workshopPeriodService: WorkshopPeriodService,
    private inscriptionService: InscriptionService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.workshopPeriod = new WorkshopPeriod();
    this.workshopPeriod.workshop = new Workshop();
    this.workshopPeriod.schedules = [];
    this.workshopPeriod.teacher = new Teacher();
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.period_id = +params['period_id'];
      this.id = +params['id'];
      this.workshopPeriodService.getAllUsers(this.id).subscribe((response) => {
        this.workshopPeriod = response.workshopPeriod;
        this.registers = response.registers;
      });
    });
  }

  changeWorkshopPeriod(inscription: Inscription) {
    const dialogRef = this.dialog.open(ChangeWorkshopDialogComponent, {
      width: '800px',
      data: {period_id: this.period_id , inscription_id: inscription.id , workshopPeriods: this.workshopPeriods}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.workshopPeriods = result.workshopPeriods;
      if (result.success) {
        this.snackBar.open('Cambio exitoso', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.workshopPeriodService.getAllUsers(this.workshopPeriod.id).subscribe((response) => {
          this.workshopPeriod = response.workshopPeriod;
          this.registers = response.registers;
        });
      }
    });
  }
  sendCaptureline(inscription: Inscription) {
    this.blockButtons = true;
    this.inscriptionService.sendCaptureline(inscription).subscribe((resp) => {
      if(resp.success){
        this.workshopPeriodService.getAllUsers(this.id).subscribe((response) => {
          this.workshopPeriod = response.workshopPeriod;
          this.registers = response.registers;
          this.blockButtons = false;
        });
      } else {
        this.snackBar.open(resp.message, '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.blockButtons = false;
      }
    });
  }
  removeCaptureline(inscription: Inscription) {
    this.blockButtons = true;
    this.inscriptionService.removeCaptureline(inscription).subscribe((resp) => {
      if(resp.success){
        this.workshopPeriodService.getAllUsers(this.id).subscribe((response) => {
          this.workshopPeriod = response.workshopPeriod;
          this.registers = response.registers;
          this.blockButtons = false;
        });
      } else {
        this.snackBar.open(resp.message, '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.blockButtons = false;
      }
    });
  }
  delete(inscription: Inscription){
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      data: {element: inscription , type: 'inscription', message: "Esta acción quitará la inscripción del alumno y su línea de captura asignada ¿Estás seguro?"}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if(result){
          this.workshopPeriodService.getAllUsers(this.id).subscribe((response) => {
            this.workshopPeriod = response.workshopPeriod;
            this.registers = response.registers;
            this.blockButtons = false;
          });
        } else {
          this.snackBar.open("Ocurrió un error", '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
          this.blockButtons = false;
        }
      }
    });
  }
  viewInscription(inscriptionId: number) {
    const dialogRef = this.dialog.open(InscriptionDialogComponent, {
      width: '600px',
      data: {inscriptionId: inscriptionId}
    });
  }

}
