import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopPeriodAdminInscriptionsComponent } from './workshop-period-admin-inscriptions.component';

describe('WorkshopPeriodAdminInscriptionsComponent', () => {
  let component: WorkshopPeriodAdminInscriptionsComponent;
  let fixture: ComponentFixture<WorkshopPeriodAdminInscriptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopPeriodAdminInscriptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopPeriodAdminInscriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
