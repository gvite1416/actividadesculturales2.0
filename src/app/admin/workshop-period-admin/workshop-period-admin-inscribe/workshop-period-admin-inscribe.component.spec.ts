import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopPeriodAdminInscribeComponent } from './workshop-period-admin-inscribe.component';

describe('WorkshopPeriodAdminInscribeComponent', () => {
  let component: WorkshopPeriodAdminInscribeComponent;
  let fixture: ComponentFixture<WorkshopPeriodAdminInscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshopPeriodAdminInscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopPeriodAdminInscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
