import { Component, OnInit } from '@angular/core';
import { WorkshopPeriodService } from '../../../services/workshop-period.service';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkshopPeriod } from '../../../models/workshop-period';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar, MatDialog } from '@angular/material';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { InscriptionService } from '../../../services/inscription.service';
import { Workshop } from '../../../models/workshop';
import { Teacher } from '../../../models/teacher';
import { PriceInscriptionDialogComponent } from '../../../dialogs/price-inscription-dialog/price-inscription-dialog.component';

@Component({
  selector: 'app-workshop-period-admin-inscribe',
  templateUrl: './workshop-period-admin-inscribe.component.html',
  styleUrls: ['./workshop-period-admin-inscribe.component.scss']
})
export class WorkshopPeriodAdminInscribeComponent implements OnInit {
  workshopPeriod: WorkshopPeriod;
  searchFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  users: User[];
  disableAllBtn: boolean;
  constructor(
    private workshopPeriodService: WorkshopPeriodService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    public dialog: MatDialog,
    private inscriptionService: InscriptionService
  ) {
    this.disableAllBtn = false;
    this.workshopPeriod = new WorkshopPeriod();
    this.workshopPeriod.workshop = new Workshop();
    this.workshopPeriod.teacher = new Teacher();
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.workshopPeriodService.getById(params['id']).subscribe((response) => {
        this.workshopPeriod = response;
      });
    });
    this.createForm();
    this.route.queryParams.subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.userService.searchUsers({
        name: params['name'],
        email: params['email'],
        number_id: params['numberId']
      }).subscribe(users => {
        this.users = users;
      });
    });
  }

  private createForm() {
    this.searchFormGroup = new FormGroup({
      name: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.email]),
      numberId: new FormControl('', [])
    });
  }

  search() {
    const isData = this.searchFormGroup.value.name || this.searchFormGroup.value.email || this.searchFormGroup.value.numberId;
    if (isData !== '') {
      this.router.navigate(['/admin/talleres-periodo/' + this.workshopPeriod.period_id + '/alumnos/' + this.workshopPeriod.id + '/inscribir'], {
        queryParams: {
          name: this.searchFormGroup.value.name,
          email: this.searchFormGroup.value.email,
          numberId: this.searchFormGroup.value.numberId
      }});
    } else {
      this.snackBar.open('Llena al menos un campo para poder buscar a un usuario', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
    }
  }

  inscribe(user: User, price?: number) {
    this.disableAllBtn = true;
    this.inscriptionService.inscribe(this.workshopPeriod.id, user.id, user.sendCaptureline, price).subscribe((response) => {
      if ( response.success ) {
        this.snackBar.open('Se ha inscrito al taller correctamente', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/talleres-periodo/' + this.workshopPeriod.period_id + '/alumnos/' + this.workshopPeriod.id]);
      } else {
        this.snackBar.open('Ocurrió un error: ' + response.message, '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
      this.disableAllBtn = false;
    });
  }

  canInscribe(user: User) {
    let can = false;
    this.workshopPeriod.students.forEach(s => {
      if (s.id === user.student_type_id) {
        can = true;
      }
    });
    return can;
  }

  openPrice(user: User) {
    const dialogRef = this.dialog.open(PriceInscriptionDialogComponent, {
      width: '400px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === true) {
          this.inscribe(user, result.price);
        }
      }
    });
  }

}
