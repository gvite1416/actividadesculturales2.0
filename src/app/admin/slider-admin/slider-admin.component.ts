import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBarVerticalPosition, MatDialog, MatSnackBar, MatPaginator } from '@angular/material';
import { SliderService } from '../../services/slider.service';
import { Slider } from '../../models/slider';
import { GLOBALS } from '../../services/globals';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-slider-admin',
  templateUrl: './slider-admin.component.html',
  styleUrls: ['./slider-admin.component.scss']
})
export class SliderAdminComponent implements OnInit {
  sliders: Slider[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  storageUrl = GLOBALS.storageUrl;
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(private sliderService: SliderService, public dialog: MatDialog, public snackBar: MatSnackBar) { }
  ngOnInit() {
    this.chargeData();
  }
  onChangeEvent($event){
    this.sliderService.getAll($event.pageSize, $event.pageIndex + 1, 'created_at', false).subscribe((sliders) => {
      sliders.sort( (a, b) => a.order - b.order);
      this.sliders = sliders;
      this.totalRegisters = this.sliderService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.sliderService.getAll(this.pageSize, 1, 'created_at', false).subscribe((sliders) => {
      sliders.sort( (a, b) => a.order - b.order);
      this.sliders = sliders;
      this.totalRegisters = this.sliderService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }
  delete(slider) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: slider , type: 'slider'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  restore(slider) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: slider , type: 'slider'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }

}
