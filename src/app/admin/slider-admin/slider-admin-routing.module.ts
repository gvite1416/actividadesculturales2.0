import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SliderAdminComponent } from './slider-admin.component';
import { SliderAdminNewComponent } from './slider-admin-new/slider-admin-new.component';
import { SliderAdminEditComponent } from './slider-admin-edit/slider-admin-edit.component';

const routes: Routes = [
  {
    path: '',
    component: SliderAdminComponent
  },
  {
    path: 'nuevo',
    component: SliderAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: SliderAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SliderAdminRoutingModule { }
