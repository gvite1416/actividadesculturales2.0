import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBar, MatSnackBarVerticalPosition, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { GLOBALS } from '../../../services/globals';
import {  FileUploader } from 'ng2-file-upload';
import { Slider } from '../../../models/slider';
import { SliderService } from '../../../services/slider.service';
import { ImageDialogComponent } from '../../../dialogs/image-dialog/image-dialog.component';
@Component({
  selector: 'app-slider-admin-edit',
  templateUrl: './slider-admin-edit.component.html',
  styleUrls: ['./slider-admin-edit.component.scss']
})
export class SliderAdminEditComponent implements OnInit {
  slider: Slider;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  fileNameDesktop: string;
  fileNameMobile: string;
  uploaderDesktop: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  uploaderMobile: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/image',
    itemAlias: 'image',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  constructor(
    private sliderService: SliderService,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.sliderService.getById(params['id']).subscribe( (slider: Slider) => {
        this.slider = slider;
        this.fileNameDesktop = this.slider.img_desktop;
        this.fileNameMobile = this.slider.img_mobile;
        this.editFormGroup.setValue({
          title: this.slider.title,
          link: this.slider.link,
          order: this.slider.order,
          img_desktop: '',
          img_mobile: '',
        });
      });
    });
    this.createForm();
    this.uploaderDesktop.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderMobile.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploaderDesktop.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImage(response.replace('tmp/' , ''));
      this.uploaderDesktop.clearQueue();
    };
    this.uploaderMobile.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.openModalImageMobile(response.replace('tmp/' , ''));
      this.uploaderMobile.clearQueue();
    };
  }
  private createForm() {
    this.editFormGroup = new FormGroup({
      title: new FormControl('', []),
      link: new FormControl('', []),
      order: new FormControl('', [Validators.required]),
      img_desktop: new FormControl('', []),
      img_mobile: new FormControl('', [])
    });
  }

  edit() {
    this.slider.title = this.editFormGroup.value.title;
    this.slider.link = this.editFormGroup.value.link;
    this.slider.order = this.editFormGroup.value.order;
    this.slider.img_desktop = this.fileNameDesktop;
    this.slider.img_mobile = this.fileNameMobile;
    this.sliderService.update(this.slider).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Slider actualizado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/admin/sliders']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

  openModalImage(image: string) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '650px',
      data: {image, widthRatio: 1200, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameDesktop = result || null;
    });
  }
  openModalImageMobile(image: string) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: '1200px',
      height: '650px',
      data: {image, widthRatio: 800, heightRatio: 600}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      this.fileNameMobile = result || null;
    });
  }


}
