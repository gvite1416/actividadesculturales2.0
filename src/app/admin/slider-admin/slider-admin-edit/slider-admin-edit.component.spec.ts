import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderAdminEditComponent } from './slider-admin-edit.component';

describe('SliderAdminEditComponent', () => {
  let component: SliderAdminEditComponent;
  let fixture: ComponentFixture<SliderAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
