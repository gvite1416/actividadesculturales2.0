import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderAdminNewComponent } from './slider-admin-new.component';

describe('SliderAdminNewComponent', () => {
  let component: SliderAdminNewComponent;
  let fixture: ComponentFixture<SliderAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
