import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderAdminRoutingModule } from './slider-admin-routing.module';
import { SliderAdminComponent } from './slider-admin.component';
import { SliderService } from '../../services/slider.service';
import { MaterialModule } from '../../material/material.module';
import { SliderAdminNewComponent } from './slider-admin-new/slider-admin-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { SliderAdminEditComponent } from './slider-admin-edit/slider-admin-edit.component';

@NgModule({
  imports: [
    CommonModule,
    SliderAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    DialogsModule
  ],
  providers: [SliderService],
  declarations: [SliderAdminComponent, SliderAdminNewComponent, SliderAdminEditComponent]
})
export class SliderAdminModule { }
