import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomAdminEditComponent } from './classroom-admin-edit.component';

describe('ClassroomAdminEditComponent', () => {
  let component: ClassroomAdminEditComponent;
  let fixture: ComponentFixture<ClassroomAdminEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomAdminEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomAdminEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
