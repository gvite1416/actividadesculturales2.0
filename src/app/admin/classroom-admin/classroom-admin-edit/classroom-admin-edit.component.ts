import { Component, OnInit } from '@angular/core';
import { Classroom } from '../../../models/classroom';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { ClassroomService } from '../../../services/classroom.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classroom-admin-edit',
  templateUrl: './classroom-admin-edit.component.html',
  styleUrls: ['./classroom-admin-edit.component.scss']
})
export class ClassroomAdminEditComponent implements OnInit {
  classroom: Classroom;
  editFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private classroomService: ClassroomService,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.classroom = new Classroom();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.classroomService.getById(params['id']).subscribe( (classroom: Classroom) => {
        this.classroom = classroom;
        this.editFormGroup.setValue({
          name: this.classroom.name,
          quota: this.classroom.quota
        });
      });
    });
    this.createForm();
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)],
        checkClassroomValidator.bind(this)),
      quota: new FormControl('', [Validators.required, Validators.min(1)])
    });
    function checkClassroomValidator(control: AbstractControl) {
      return this.classroomService.checkName(control.value, this.classroom.id);
    }
  }
  edit() {
    this.classroom.name = this.editFormGroup.value.name;
    this.classroom.quota = this.editFormGroup.value.quota;
    this.classroomService.update(this.classroom).subscribe( (response) => {
      this.snackBar.open('Salón editado', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
      this.router.navigate(['/admin/salones/']);
    }, response => {
      if (response.status == 400){
        for (const index in response.error.message) {
          if (index) {
            response.error.message[index].forEach(error => {
              this.snackBar.open(error, '', {
                duration: 3000,
              });
            });
          }
        }
      }
    });
  }

}
