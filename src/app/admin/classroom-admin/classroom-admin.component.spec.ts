import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomAdminComponent } from './classroom-admin.component';

describe('ClassroomAdminComponent', () => {
  let component: ClassroomAdminComponent;
  let fixture: ComponentFixture<ClassroomAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
