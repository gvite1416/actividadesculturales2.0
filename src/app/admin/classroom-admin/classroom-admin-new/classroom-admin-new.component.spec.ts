import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassroomAdminNewComponent } from './classroom-admin-new.component';

describe('ClassroomAdminNewComponent', () => {
  let component: ClassroomAdminNewComponent;
  let fixture: ComponentFixture<ClassroomAdminNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassroomAdminNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassroomAdminNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
