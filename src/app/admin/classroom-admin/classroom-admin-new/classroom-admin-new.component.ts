import { Component, OnInit } from '@angular/core';
import { Classroom } from '../../../models/classroom';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { ClassroomService } from '../../../services/classroom.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classroom-admin-new',
  templateUrl: './classroom-admin-new.component.html',
  styleUrls: ['./classroom-admin-new.component.scss']
})
export class ClassroomAdminNewComponent implements OnInit {
  classroom: Classroom;
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private classroomService: ClassroomService,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)],
        checkCareerValidator.bind(this)),
      quota: new FormControl('', [Validators.required, Validators.min(1)])
    });
    function checkCareerValidator(control: AbstractControl) {
      return this.classroomService.checkName(control.value);
    }
  }
  add() {
    this.classroom = new Classroom(this.createFormGroup.value);
    this.classroomService.create(this.classroom).subscribe( (response) => {
      this.snackBar.open('Salón creado', '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
      this.router.navigate(['/admin/salones/']);
    }, response => {
      if (response.status == 400){
        for (const index in response.error.message) {
          if (index) {
            response.error.message[index].forEach(error => {
              this.snackBar.open(error, '', {
                duration: 3000,
              });
            });
          }
        }
      }
    });
  }

}
