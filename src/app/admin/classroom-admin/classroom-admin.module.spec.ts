import { ClassroomAdminModule } from './classroom-admin.module';

describe('ClassroomAdminModule', () => {
  let classroomAdminModule: ClassroomAdminModule;

  beforeEach(() => {
    classroomAdminModule = new ClassroomAdminModule();
  });

  it('should create an instance', () => {
    expect(classroomAdminModule).toBeTruthy();
  });
});
