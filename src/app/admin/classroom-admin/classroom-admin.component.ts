import { Component, OnInit, ViewChild } from '@angular/core';
import { ClassroomService } from '../../services/classroom.service';
import { Classroom } from '../../models/classroom';
import { MatSnackBarVerticalPosition, MatDialog, MatSnackBar, MatPaginator } from '@angular/material';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { RestoreDialogComponent } from '../../dialogs/restore-dialog/restore-dialog.component';

@Component({
  selector: 'app-classroom-admin',
  templateUrl: './classroom-admin.component.html',
  styleUrls: ['./classroom-admin.component.scss']
})
export class ClassroomAdminComponent implements OnInit {
  classrooms: Classroom[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  totalPages: number;
  totalRegisters: number;
  pageSize: number;
  @ViewChild('paginator', { read: MatPaginator, static: false }) paginator: MatPaginator;
  constructor(
    private classroomService: ClassroomService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }
  ngOnInit() {
    this.chargeData();
  }
  onChangeEvent($event){
    this.classroomService.getAll($event.pageSize, $event.pageIndex + 1, 'name', true).subscribe((classrooms) => {
      this.classrooms = classrooms;
      this.totalRegisters = this.classroomService.xTotalRegister;
    });
  }
  onFilterChange(){
    this.chargeData();
  }
  onValueChange(){
    this.chargeData();
  }
  onPeriodChange(){
    this.chargeData();
  }
  chargeData(){
    this.pageSize = 10;
    this.classroomService.getAll(this.pageSize, 1, 'name', true).subscribe((classrooms) => {
      this.classrooms = classrooms;
      this.totalRegisters = this.classroomService.xTotalRegister;
      this.paginator.pageIndex = 0;
    });
  }

  delete(classroom) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '250px',
      data: {element: classroom , type: 'classroom'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result.success === false) {
          this.snackBar.open('Ocurrió un error', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        }
        this.chargeData();
      }
    });
  }
  restore(classroom) {
    const dialogRef = this.dialog.open(RestoreDialogComponent, {
      width: '250px',
      data: {element: classroom , type: 'classroom'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.chargeData();
      }
    });
  }

}
