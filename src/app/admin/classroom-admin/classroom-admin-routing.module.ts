import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassroomAdminComponent } from './classroom-admin.component';
import { ClassroomAdminNewComponent } from './classroom-admin-new/classroom-admin-new.component';
import { ClassroomAdminEditComponent } from './classroom-admin-edit/classroom-admin-edit.component';

const routes: Routes = [
  {
    path: '',
    component: ClassroomAdminComponent
  },
  {
    path: 'nuevo',
    component: ClassroomAdminNewComponent
  },
  {
    path: 'editar/:id',
    component: ClassroomAdminEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassroomAdminRoutingModule { }
