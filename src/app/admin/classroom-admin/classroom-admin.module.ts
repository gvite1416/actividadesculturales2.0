import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClassroomAdminRoutingModule } from './classroom-admin-routing.module';
import { ClassroomAdminComponent } from './classroom-admin.component';
import { ClassroomAdminNewComponent } from './classroom-admin-new/classroom-admin-new.component';
import { ClassroomAdminEditComponent } from './classroom-admin-edit/classroom-admin-edit.component';
import { ClassroomService } from '../../services/classroom.service';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogsModule } from '../../dialogs/dialogs.module';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    ClassroomAdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [ ClassroomAdminComponent, ClassroomAdminNewComponent, ClassroomAdminEditComponent ],
  providers: [ ClassroomService ]
})
export class ClassroomAdminModule { }
