export const GLOBALS = {
    apiUrl: '//localhost/fesa/apiactividadesculturales/api/',
    storageUrl: '//localhost/fesa/apiactividadesculturales/storage/',
    storageFiles: '//localhost/fesa/apiactividadesculturales/files/',
    /*apiUrl: 'http://actividades-culturales-fesa.eastus.cloudapp.azure.com/be-ac/api/',
    storageUrl: 'http://actividades-culturales-fesa.eastus.cloudapp.azure.com/be-ac/storage/',
    storageFiles: 'http://actividades-culturales-fesa.eastus.cloudapp.azure.com/be-ac/files/',*/
    MY_MOMENT_FORMATS: {
        parse: {
            dateInput: "l"
        },
        display: {
            dateA11yLabel: "LL",
            dateInput: "L",
            monthYearA11yLabel: "MMMM YYYY",
            monthYearLabel: "MMM YYYY"
        }
    },
    daysWeek: ['Domingo', 'Lunes', 'Martes' , 'Miércoles' , 'Jueves' , 'Viernes', 'Sábado']
};