import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs/index';
import { Role } from '../models/role';
@Injectable({
  providedIn: 'root'
})
export class RoleService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Role[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'role?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Role(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getStudents(): Observable <Role[]> {
    return this._http.get(GLOBALS.apiUrl + 'student/types').pipe(
      map((response: Role[]) => {
        return response.map( item => new Role(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getByGroup(slug: string): Observable <Role[]> {
    return this._http.get(GLOBALS.apiUrl + 'role/by-group/' + slug).pipe(
      map((response: Role[]) => {
        return response.map( item => new Role(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getByIdStudent(id: number): Observable <Role> {
    return this._http.get(GLOBALS.apiUrl + 'student/by-id/' + id).pipe(
      map((response: any) => {
        return new Role(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
}
