import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { Offering } from '../models/offering';

@Injectable({
  providedIn: 'root'
})
export class OfferingService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Offering[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'title';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'offering?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Offering(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getAllByEvent(event_id: number): Observable <Offering[]> {
    return this._http.get(GLOBALS.apiUrl + 'offering/by-event/' + event_id).pipe(
      map((response: Offering[]) => {
        return response.map( item => new Offering(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Offering> {
    return this._http.get(GLOBALS.apiUrl + 'offering/' + id).pipe(
      map((response: any) => {
        return new Offering(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(offering: Offering): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'offering', offering).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(offering: Offering): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'offering/' + offering.id, offering).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(offering: Offering): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'offering/' + offering.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  restore(offering: Offering): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'offering/' + offering.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
}
