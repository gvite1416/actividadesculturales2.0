import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Captureline } from '../models/captureline';
import { GLOBALS } from './globals';
import { Observable, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';

@Injectable({
  providedIn: 'root'
})
export class CapturelineService {
  xTotalRegister: number;
  xTotalPages: number;
  xCapturelinesValues: number[];
  constructor(private _http: HttpClient) {}

  getAll(limit?:number, page?:number, cpValue?: number, cpUse?: string, cpPeriodId?: number): Observable <Captureline[]> {
    limit = limit ? limit : 10;
    page = page ? page : 1;
    return this._http.get(GLOBALS.apiUrl + 'captureline?limit=' + limit + '&page=' + page + '&value=' + cpValue + '&use='+ cpUse + '&period_id=' + (cpPeriodId != -1 ? cpPeriodId : ''), {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        this.xCapturelinesValues = JSON.parse(response.headers.get("X-Capturelines-Values"));
        return response.body.map( item => new Captureline(item, true));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  delete(captureline: Captureline, force: boolean): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'captureline/' + captureline.id + '?force=' + force).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

}
