import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { Requirement } from '../models/requirement';


@Injectable({
  providedIn: 'root'
})
export class RequirementService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Requirement[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'requirement?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Requirement(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Requirement> {
    return this._http.get(GLOBALS.apiUrl + 'requirement/' + id).pipe(
      map((response: any) => {
        return new Requirement(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(requirement: Requirement): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'requirement', requirement).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(requirement: Requirement): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'requirement/' + requirement.id, requirement).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(requirement: Requirement): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'requirement/' + requirement.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  restore(requirement: Requirement): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'requirement/' + requirement.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
}
