import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { GLOBALS } from './globals';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private _http: HttpClient) { }

  one(data: any): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'report/one', data, {responseType: 'arraybuffer'});
  }
  two(data: any): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'report/two', data, {responseType: 'arraybuffer'});
  }
  three(data: any): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'report/three', data, {responseType: 'arraybuffer'});
  }
  events(data: any): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'report/events', data, {responseType: 'arraybuffer'});
  }
}
