import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, of } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs/index';
import { Period } from '../models/period';

@Injectable({
  providedIn: 'root'
})
export class PeriodService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient, private router: Router) { }
  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Period[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'period?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Period(item, true));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Period> {
    return this._http.get(GLOBALS.apiUrl + 'period/' + id).pipe(
      map((response: object) => {
        return new Period(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(period: Period): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'period', period).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(period: Period): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'period/' + period.id, period).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(period: Period): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'period/' + period.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  restore(period: Period): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'period/' + period.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  checkName(name: string, id?: number): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'period/check-name' + (id ? '/' + id : ''), {name}).pipe(
      map((response: any) => {
        return null;
      }),
      catchError( (response) => {
        const _error = {};
        response.error.message.name.forEach( error => {
            _error[error] = true;
        });
        return of(_error || 'Error en el servicio');
      }));
  }

  getCurrent(): Observable <Period> {
    return this._http.get(GLOBALS.apiUrl + 'period/current').pipe(
      map((response: object) => {
        return new Period(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }

  getUserPeriodRequirements(): Observable <any> {
    return this._http.get(GLOBALS.apiUrl + 'period/user-files-requirements').pipe(
      map((response: any) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }

  getByUserPeriodRequirements(user_id): Observable <any> {
    return this._http.get(GLOBALS.apiUrl + 'period/user-files-requirements/' + user_id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getByUserPeriodRequirementsInscription(inscription_id): Observable <any> {
    return this._http.get(GLOBALS.apiUrl + 'period/user-files-requirements-inscription/' + inscription_id).pipe(
      map((response: any) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }

}
