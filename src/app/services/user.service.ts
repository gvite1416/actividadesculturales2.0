import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, throwError, Observer, BehaviorSubject } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { UserAdmin } from '../models/user-admin';
import * as moment from 'moment';

@Injectable()
export class UserService {
  user: User;
  userActiveCharge: Observable<any>;
  userActiveChangeObserver: Observer<any>;
  private loginSource = new BehaviorSubject('DEFAULT');
  eventLogin = this.loginSource.asObservable();
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) {
    this.userActiveCharge = new Observable((observer) => {
      this.userActiveChangeObserver = observer;
    });
  }

  registerUser(user: User): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'register', user).pipe(
      map((response) => {
        return response;
      }),
      catchError((response) => {
        return throwError(response);
      }));
  }

  checkEmail(email: string, id?: number): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'register/check-email' + (id ? '/' + id : ''), { email }).pipe(
      map((response: any) => {
        if (response.success === false) {
          const _error = {};
          response.error.email.forEach(error => {
            _error[error] = true;
          });
          return _error;
        } else {
          return null;
        }
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  checkNumberId(number_id: string, id?: number): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'register/check-number-id' + (id ? '/' + id : ''), { number_id }).pipe(
      map((response: any) => {
        if (response.success === false) {
          const _error = {};
          response.error.number_id.forEach(error => {
            _error[error] = true;
          });
          return _error;
        } else {
          return null;
        }
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  searchUsers(params, limit?:number, page?:number): Observable<any> {
    return this._http.get(GLOBALS.apiUrl + 'users/search', {observe: 'response', params}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map(item => new User(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  verifyUser(code: string) {
    return this._http.get(GLOBALS.apiUrl + 'verify/' + code).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }

  login(user: string, password: string): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'login', { user, password }).pipe(
      map((response: any) => {
        let expires = moment();
        expires.add(response.expires_in, "minutes");
        localStorage.setItem('token', response.access_token);
        localStorage.setItem('token_expires', expires.format('dddd MMMM Do YYYY HH:mm:ss Z'));
        this.loginSource.next('LOGIN');
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  logout(): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'logout', { token: localStorage.getItem('token') }).pipe(
      map((response: any) => {
        if (response.success === false) {
          return false;
        } else {
          localStorage.removeItem('token');
          return true;
        }
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  delete(user: User): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'users/' + user.id).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  restore(user: User): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'users/' + user.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  getById(id: number): Observable<User> {
    return this._http.get(GLOBALS.apiUrl + 'users/' + id).pipe(
      map((response) => {
        return new User(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(user: UserAdmin): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'users', user).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  update(user: User): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'users/' + user.id, user).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getActive(): Observable<any> {
    /*let expires = moment(localStorage.getItem('token_expires'), 'dddd MMMM Do YYYY HH:mm:ss Z');
    let now = moment().utc();
    if (expires.diff(now) <= 0) {
      
    }*/
    return this._http.get(GLOBALS.apiUrl + 'users/active').pipe(
      map((response) => {
        this.user = new User(response);
        if (this.userActiveChangeObserver !== undefined) {
          this.userActiveChangeObserver.next(this.user);
        }
        return this.user;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  updateProfile(user: User): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'users/profile', user).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  updatePass(pass: any): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'users/profile-pass', pass).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  updatePassUser(user_id: number, password: string, password_confirmation: string): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'users/change-password/' + user_id, {
      password,
      password_confirmation
    }).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  changePass(pass: any): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'reset', pass).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error);
      }));
  }
  recoverPass(user: string): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'recover', { user }).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  savePhoto(photo: string) {
    return this._http.put(GLOBALS.apiUrl + 'users/profile-photo', { photo }).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  generateQr() {
    return this._http.put(GLOBALS.apiUrl + 'users/profile-qr', {}).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  verify(user_id): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'users/verify/' + user_id, {}).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  exportExcel(filename: string, limit: number, page: number): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'users/export', {name: filename, limit: limit, page: page}).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
}
