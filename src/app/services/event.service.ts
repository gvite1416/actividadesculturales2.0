import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, of, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { EventAC } from '../models/event';
import { Showing } from '../models/showing';
@Injectable({
  providedIn: 'root'
})
export class EventService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <EventAC[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'title';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'event?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new EventAC(item, true));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <EventAC> {
    return this._http.get(GLOBALS.apiUrl + 'event/' + id).pipe(
      map((response: any) => {
        return new EventAC(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getBySlug(slug: string): Observable <EventAC> {
    return this._http.get(GLOBALS.apiUrl + 'event/by-slug/' + slug).pipe(
      map((response: any) => {
        return new EventAC(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getOfferingBySlug(slug: string): Observable <EventAC> {
    return this._http.get(GLOBALS.apiUrl + 'event/offering-by-slug/' + slug).pipe(
      map((response: any) => {
        return new EventAC(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getByCategory(event_category_id: number): Observable <EventAC[]> {
    return this._http.get(GLOBALS.apiUrl + 'event-category/' + event_category_id + '/events').pipe(
      map((response: EventAC[]) => {
        return response.map( item => new EventAC(item, true));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(event: EventAC): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'event', event).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(event: EventAC): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'event/' + event.id, event).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(event: EventAC): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'event/' + event.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  restore(event: EventAC): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'event/' + event.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  checkName(title: string, id?: number): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'event/check-name' + (id ? '/' + id : ''), {title}).pipe(
      map((response: any) => {
        return null;
      }),
      catchError( (response) => {
        const _error = {};
        response.error.message.name.forEach( error => {
            _error[error] = true;
        });
        return of(_error || 'Error en el servicio');
      }));
  }
  inscribe(showing: Showing): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'event/inscribe', {event_showing_id: showing.id}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
}
