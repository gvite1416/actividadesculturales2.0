import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, of } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { Workshop } from '../models/workshop';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class WorkshopService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Workshop[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'workshop?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Workshop(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getBySlug(slug: string): Observable <Workshop> {
    return this._http.get(GLOBALS.apiUrl + 'workshop/by-slug/' + slug).pipe(
      map((response: object) => {
        return new Workshop(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getBySlugCurrentPeriod(slug: string): Observable <Workshop> {
    return this._http.get(GLOBALS.apiUrl + 'workshop/by-slug-current-period/' + slug).pipe(
      map((response: object) => {
        return new Workshop(response , true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(workshop: Workshop): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'workshop', workshop).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(workshop: Workshop): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'workshop/' + workshop.id, workshop).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(workshop: Workshop): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'workshop/' + workshop.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  restore(workshop: Workshop): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'workshop/' + workshop.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  checkName(name: string, id?: number): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'workshop/check-name' + (id ? '/' + id : ''), {name}).pipe(
      map((response: any) => {
        return null;
      }),
      catchError( (response) => {
        const _error = {};
        response.error.message.name.forEach( error => {
            _error[error] = true;
        });
        return of(_error || 'Error en el servicio');
      }));
  }
  getAllByCurrentPeriod(limit?: number): Observable <Workshop[]> {
    return this._http.get(GLOBALS.apiUrl + 'workshop/current-period?limit=' + (limit ? limit : '')).pipe(
      map((response: Workshop[]) => {
        return response.map( item => new Workshop(item, true));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  inscribe(wp_id: number, collegeDegreeOption: boolean): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'workshop/inscribe', {workshop_period_id: wp_id, college_degree_option: collegeDegreeOption}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }

}
