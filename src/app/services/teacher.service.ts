import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Teacher } from '../models/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Teacher[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'teacher?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Teacher(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Teacher> {
    return this._http.get(GLOBALS.apiUrl + 'teacher/' + id).pipe(
      map((response: object) => {
        return new Teacher(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(teacher: Teacher): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'teacher', teacher).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(teacher: Teacher): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'teacher/' + teacher.id, teacher).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(teacher: Teacher): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'teacher/' + teacher.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  restore(teacher: Teacher): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'teacher/' + teacher.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
}
