import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, of, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { Career } from '../models/career';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CareerService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Career[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'career?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Career(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Career> {
    return this._http.get(GLOBALS.apiUrl + 'career/' + id).pipe(
      map((response: any) => {
        return new Career(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(camp: Career): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'career', camp).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(camp: Career): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'career/' + camp.id, camp).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(camp: Career): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'career/' + camp.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  
  restore(career: Career): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'career/' + career.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  checkName(name: string, id?: number): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'career/check-name' + (id ? '/' + id : ''), {name}).pipe(
      map((response: any) => {
        return null;
      }),
      catchError( (response) => {
        const _error = {};
        response.error.message.name.forEach( error => {
            _error[error] = true;
        });
        return of(_error || 'Error en el servicio');
      }));
  }
}
