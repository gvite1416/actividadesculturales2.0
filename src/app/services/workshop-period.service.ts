import { Injectable } from '@angular/core';
import { WorkshopPeriod } from '../models/workshop-period';
import { Observable, throwError } from 'rxjs/index';
import { GLOBALS } from './globals';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Inscription } from '../models/inscription';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class WorkshopPeriodService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <WorkshopPeriod[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'workshop-period?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new WorkshopPeriod(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getAllByPeriodId(id: number): Observable<WorkshopPeriod[]> {
    return this._http.get(GLOBALS.apiUrl + 'workshop-period/by-period/' + id).pipe(
      map((response: WorkshopPeriod[]) => {
        return response.map( item => new WorkshopPeriod(item, true));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(workshopPeriod: WorkshopPeriod): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'workshop-period', workshopPeriod).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  update(workshopPeriod: WorkshopPeriod): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'workshop-period/' + workshopPeriod.id, workshopPeriod).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  getById(id: number): Observable <WorkshopPeriod> {
    return this._http.get(GLOBALS.apiUrl + 'workshop-period/' + id).pipe(
      map((response: object) => {
        return new WorkshopPeriod(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getByUser(id: number, user_id?: number): Observable <any> {
    return this._http.get(GLOBALS.apiUrl + 'workshop-period/' + id + '/by-user' + ((user_id) ? '/' + user_id : '')).pipe(
      map((response: any) => {
        return {
          inscription: new Inscription(response, true),
          workshop_period: new WorkshopPeriod(response.workshop_period, true),
          user: new User(response.user)
        };
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getAllByUser(user_id?: number): Observable <any[]> {
    return this._http.get(GLOBALS.apiUrl + 'workshop-period/all-by-user' + ((user_id) ? '/' + user_id : '')).pipe(
      map((response: any) => {
        return response.map(res => {
          return {
            inscription: new Inscription(res, true),
            workshop_period: new WorkshopPeriod(res.workshop_period, true)
          };
        });
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getAllUsers(id: number): Observable<any> {
    return this._http.get(GLOBALS.apiUrl + 'workshop-period/' + id + '/users').pipe(
      map((response: any) => {
        const res = {
          workshopPeriod: new WorkshopPeriod(response),
          registers: response.students_inscribed.map(reg => {
            return {
              user: new User(reg),
              inscription: new Inscription(reg.pivot, true)
            };
          })
        };
        return res;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  delete(workshopPeriod: WorkshopPeriod): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'workshop-period/' + workshopPeriod.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  restore(workshopPeriod: WorkshopPeriod): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'workshop-period/' + workshopPeriod.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
}
