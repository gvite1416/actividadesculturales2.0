import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { GLOBALS } from './globals';
import { Inscription } from '../models/inscription';
import { WorkshopPeriod } from '../models/workshop-period';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  checkTicketFolio(ticket_folio: string, id: number): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'inscription/check-ticket' + (id ? '/' + id : ''), { ticket_folio }).pipe(
      map((response: any) => {
        if (response.success === false) {
          const _error = {};
          response.error.ticket_folio.forEach(error => {
            _error[error] = true;
          });
          return _error;
        } else {
          return null;
        }
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  validate(inscription: Inscription): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'inscription/' + inscription.id + '/validate', inscription).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  askAgainFiles(inscription: Inscription): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'inscription/ask-again-files/' + inscription.id, inscription).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  inscribe(wp_id: number, user_id: number , sendCaptureline: boolean, price?: number): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'inscription/inscribe/' + wp_id, {user_id, price, sendCaptureline}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  change(inscription_id: number, workshop_period_id: number):  Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'inscription/change/' + inscription_id, {workshop_period_id}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Inscription> {
    return this._http.get(GLOBALS.apiUrl + 'inscription/' + id).pipe(
      map((response: object) => {
        return new Inscription(response, true);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getByUser(id: number, user_id?: number): Observable <any> {
    return this._http.get(GLOBALS.apiUrl + 'inscription/by-user/' + id + ((user_id) ? '/' + user_id : '')).pipe(
      map((response: any) => {
        return {
          inscription: new Inscription(response, true),
          workshop_period: new WorkshopPeriod(response.workshop_period, true),
          user: new User(response.user)
        };
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  filesUploaded(inscription_id: number): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'inscription/files-uploaded/' + inscription_id, {}).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getAllActive(limit?:number, page?:number): Observable <Inscription[]> {
    limit = limit ? limit : 10;
    page = page ? page : 1;
    return this._http.get(GLOBALS.apiUrl + 'inscription/files-actives?limit=' + limit + '&page=' + page, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Inscription(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  delete(inscription: Inscription): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'inscription/' + inscription.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  sendCaptureline(inscription: Inscription): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'inscription/send-catureline/' + inscription.id, {}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  removeCaptureline(inscription: Inscription): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'inscription/remove-catureline/' + inscription.id, {}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
}
