import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs/index';
import { Slider } from '../models/slider';

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Slider[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'title';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'slider?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Slider(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Slider> {
    return this._http.get(GLOBALS.apiUrl + 'slider/' + id).pipe(
      map((response: object) => {
        return new Slider(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(slider: Slider): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'slider', slider).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(slider: Slider): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'slider/' + slider.id, slider).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(slider: Slider): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'slider/' + slider.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  restore(slider: Slider): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'slider/' + slider.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
}
