import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { Observable, of, throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { Occupation } from '../models/occupation';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class OccupationService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(private _http: HttpClient) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Occupation[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'occupation?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Occupation(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getById(id: number): Observable <Occupation> {
    return this._http.get(GLOBALS.apiUrl + 'occupation/' + id).pipe(
      map((response: any) => {
        return new Occupation(response);
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  create(occupation: Occupation): Observable<any> {
    return this._http.post(GLOBALS.apiUrl + 'occupation', occupation).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  update(occupation: Occupation): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'occupation/' + occupation.id, occupation).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  delete(occupation: Occupation): Observable<any> {
    return this._http.delete(GLOBALS.apiUrl + 'occupation/' + occupation.id).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  restore(occupation: Occupation): Observable<any> {
    return this._http.put(GLOBALS.apiUrl + 'occupation/' + occupation.id + '/restore',{}).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }

  checkName(name: string, id?: number): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'occupation/check-name' + (id ? '/' + id : ''), {name}).pipe(
      map((response: any) => {
        return null;
      }),
      catchError( (response) => {
        const _error = {};
        response.error.message.name.forEach( error => {
            _error[error] = true;
        });
        return of(_error || 'Error en el servicio');
      }));
  }
}
