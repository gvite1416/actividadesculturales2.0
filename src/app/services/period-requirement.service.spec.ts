import { TestBed, inject } from '@angular/core/testing';

import { PeriodRequirementService } from './period-requirement.service';

describe('PeriodRequirementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PeriodRequirementService]
    });
  });

  it('should be created', inject([PeriodRequirementService], (service: PeriodRequirementService) => {
    expect(service).toBeTruthy();
  }));
});
