import { Injectable } from '@angular/core';
import { GLOBALS } from './globals';
import { throwError } from 'rxjs/index';
import { map, catchError } from 'rxjs/operators/index';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UploadsService {

  constructor(private _http: HttpClient) { }

  saveBase64(image: string, oldImage?: string) {
    return this._http.post(GLOBALS.apiUrl + 'upload/image-base64', { image, old: oldImage || null }).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  deleteTmp(file: string){
    return this._http.delete(GLOBALS.apiUrl + 'upload/tmp/' + file).pipe(
      map((response) => {
        return response;
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
}
