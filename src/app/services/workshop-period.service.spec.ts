import { TestBed, inject } from '@angular/core/testing';

import { WorkshopPeriodService } from './workshop-period.service';

describe('WorkshopPeriodService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkshopPeriodService]
    });
  });

  it('should be created', inject([WorkshopPeriodService], (service: WorkshopPeriodService) => {
    expect(service).toBeTruthy();
  }));
});
