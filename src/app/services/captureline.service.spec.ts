import { TestBed } from '@angular/core/testing';

import { CapturelineService } from './captureline.service';

describe('CapturelineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CapturelineService = TestBed.get(CapturelineService);
    expect(service).toBeTruthy();
  });
});
