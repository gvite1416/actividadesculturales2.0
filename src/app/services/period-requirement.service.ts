import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs/index';
import { Requirement } from '../models/requirement';
import { HttpClient } from '@angular/common/http';
import { GLOBALS } from './globals';
import { map, catchError } from 'rxjs/operators/index';

@Injectable({
  providedIn: 'root'
})
export class PeriodRequirementService {
  xTotalRegister: number;
  xTotalPages: number;
  constructor(
    private _http: HttpClient
  ) { }

  getAll(limit?:number, page?:number, orderBy?: string, orderAsc?: boolean): Observable <Requirement[]> {
    limit = limit ? limit : -1;
    page = page ? page : 1;
    orderBy = orderBy ? orderBy : 'name';
    orderAsc = orderAsc === true ? true : false;
    return this._http.get(GLOBALS.apiUrl + 'requirement?limit=' + limit + '&page=' + page + '&orderBy=' + orderBy + '&orderAsc=' + orderAsc, {observe: 'response'}).pipe(
      map((response: any) => {
        this.xTotalRegister = response.headers.get("X-Total-Registers");
        this.xTotalPages = response.headers.get("X-Total-Pages");
        return response.body.map( item => new Requirement(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  getAllByPeriod(period_id: number): Observable <Requirement[]> {
    return this._http.get(GLOBALS.apiUrl + 'requirement/by-period/' + period_id).pipe(
      map((response: any) => {
        return response.map( item => new Requirement(item));
      }),
      catchError((error) => {
        return throwError(error || 'Error en el servicio');
      }));
  }
  enable(period_id, requirement_id): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'period/requirement', {
      period_id,
      requirement_id,
      action: true
    }).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
  disable(period_id, requirement_id): Observable <any> {
    return this._http.post(GLOBALS.apiUrl + 'period/requirement', {
      period_id,
      requirement_id,
      action: false
    }).pipe(
      map((response) => {
        return response;
      }),
      catchError( (error) => {
        return throwError(error || 'Error en el servicio');
      })
    );
  }
}
