import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MomentPipe } from './moment.pipe';
import { SchedulePipe } from './schedule.pipe';
import { GenpassPipe } from './genpass.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MomentPipe, SchedulePipe, GenpassPipe],
  exports: [MomentPipe, SchedulePipe, GenpassPipe]
})
export class PipesModule { }
