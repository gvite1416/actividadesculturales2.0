import { Pipe, PipeTransform } from '@angular/core';
import { Schedule } from '../models/schedule';
import { GLOBALS } from '../services/globals';

@Pipe({
  name: 'scheduleFormat'
})
export class SchedulePipe implements PipeTransform {

  transform(value: Schedule, args?: any): any {
    return GLOBALS.daysWeek[value.day] + ' ' + value.start.format('HH:mm') + ' Hrs. - ' + value.finish.format('HH:mm') + ' Hrs.';
  }

}
