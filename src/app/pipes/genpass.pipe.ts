import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'genpass'
})
export class GenpassPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    return value.length >= 3 ? this.capitalizeFirstLetter(value).substring(0, 3) + '12345' : null;
  }
  private capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1).toLocaleLowerCase();
  }

}
