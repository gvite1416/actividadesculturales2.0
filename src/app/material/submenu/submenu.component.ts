import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MenuItem } from '../../models/menu-item';

@Component({
  selector: 'app-submenu',
  templateUrl: './submenu.component.html',
  styleUrls: ['./submenu.component.scss']
})
export class SubmenuComponent implements OnInit {
  @Input() items: MenuItem[];
  @ViewChild('childMenu', { static: false }) public childMenu;
  constructor() { }

  ngOnInit() {
  }

}
