import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'acfa-icon',
  templateUrl: './acfa-icon.component.html',
  styleUrls: ['./acfa-icon.component.scss', '../colors-icon.scss']
})
export class AcfaIconComponent implements OnInit {
  protected _elementClass: string[] = [];
  @Input() name: string;
  @Input() color: string;
  @Input() size: string;
  @Input() animate: string;
  constructor(private elRef: ElementRef) { }

  ngOnInit() {
    if (this.elRef.nativeElement.className !== '') {
      this._elementClass = this.elRef.nativeElement.className.split(' ');
    }
    this._elementClass.push('icon-acfa');
    this._elementClass.push('icon-acfa-' + this.name);
    if (this.color) {
      this._elementClass.push('icon-md-' + this.color);
    }
    if (this.animate) {
      this._elementClass.push('icon-animate');
    }
    this.elRef.nativeElement.className = this._elementClass.join(' ');
    if (this.size) {
      this.elRef.nativeElement.style.fontSize = this.size;
    }
  }

}
