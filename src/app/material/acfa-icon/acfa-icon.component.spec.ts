import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcfaIconComponent } from './acfa-icon.component';

describe('AcfaIconComponent', () => {
  let component: AcfaIconComponent;
  let fixture: ComponentFixture<AcfaIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcfaIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcfaIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
