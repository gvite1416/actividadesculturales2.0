import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDialogModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatDividerModule,
  MatRadioModule,
  MatToolbarModule,
  MatButtonModule,
  MatMenuModule,
  MatSidenavModule,
  MatListModule,
  MatTabsModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatTableModule,
  MatProgressBarModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatStepperModule,
  MatProgressSpinnerModule,
  MatDatepickerModule,
  MatSliderModule,
  MatPaginatorModule,
  MatPaginatorIntl
} from '@angular/material';
// import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, OwlDateTimeIntl } from 'ng-pick-datetime';
// import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS} from '@angular/material-moment-adapter';
import { AcfaIconComponent } from './acfa-icon/acfa-icon.component';
import { FaIconComponent } from './fa-icon/fa-icon.component';
import { ModalLoadComponent } from './modal-load/modal-load.component';
import { LoaderComponent } from './loader/loader.component';
import { GLOBALS } from '../services/globals';
import { SubmenuComponent } from './submenu/submenu.component';
import { RouterModule } from '@angular/router';
import { SwiperComponent } from './swiper/swiper.component';
import { SvgImgComponent } from './svg-img/svg-img.component';
import { CropperComponent } from './cropper/cropper.component';
import { CustomMatPaginatorIntl } from '../paginators/CustomMatPaginatorIntl';
@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatRadioModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    MatTabsModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatTableModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatSliderModule,
    MatPaginatorModule,
    // OwlMomentDateTimeModule,
    // OwlDateTimeModule,
    RouterModule,
  ],
  exports: [
    MatDialogModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatRadioModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    MatTabsModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    MatPaginatorModule,
    // OwlMomentDateTimeModule,
    // OwlDateTimeModule,
    MatDatepickerModule,
    AcfaIconComponent,
    FaIconComponent,
    SvgImgComponent,
    ModalLoadComponent,
    LoaderComponent,
    SubmenuComponent,
    SwiperComponent,
    CropperComponent,
    MatTableModule
  ],
  declarations: [
    AcfaIconComponent,
    FaIconComponent,
    ModalLoadComponent,
    LoaderComponent,
    SubmenuComponent,
    SwiperComponent,
    SvgImgComponent,
    CropperComponent
  ],
  entryComponents: [ModalLoadComponent, LoaderComponent],
  providers: [
    /*{provide: OWL_DATE_TIME_FORMATS, useValue: GLOBALS.MY_MOMENT_FORMATS},
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'es'},
    {provide: OwlDateTimeIntl, useClass: DefaultIntl}*/
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: GLOBALS.MY_MOMENT_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'es'},
    {provide: MatPaginatorIntl, useClass: CustomMatPaginatorIntl}
  ]
})
export class MaterialModule { }
