import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  protected _elementClass: string[] = [];
  @Input() color: string;
  constructor(private elRef: ElementRef) { }

  ngOnInit() {
    if (this.elRef.nativeElement.className !== '') {
      this._elementClass = this.elRef.nativeElement.className.split(' ');
    }
    this._elementClass.push('load-img');
    if (this.color) {
      this._elementClass.push('load-img-' + this.color);
    } else {
      this._elementClass.push('load-img-primary');
    }
    this.elRef.nativeElement.className = this._elementClass.join(' ');
  }

}
