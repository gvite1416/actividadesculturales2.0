import { Component, OnInit,
  Input, Output, EventEmitter, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import * as Cropper from 'cropperjs/dist/cropper.min.js';

@Component({
  selector: 'app-cropper',
  templateUrl: './cropper.component.html',
  styleUrls: ['./cropper.component.scss']
})
export class CropperComponent implements OnInit, AfterViewInit {
  @ViewChild('cropper', { static: false }) cropperElement: ElementRef;
  @ViewChild('image', { static: false }) imageElement: ElementRef;
  @Input() src: number;
  @Input() aspectRatio: number;
  @Input() widthRatio: number;
  @Input() heightRatio: number;
  private cropper: any = false;
  @Output() getCropper = new EventEmitter();
  constructor(private element: ElementRef) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getCropper.emit(this);
  }

  create() {
    this.imageElement.nativeElement.crossOrigin = 'anonymous';
    this.cropper = new Cropper(this.imageElement.nativeElement, {
      aspectRatio: (this.widthRatio && this.heightRatio) ?  this.widthRatio / this.heightRatio : 16 / 9
    });
  }

  replace() {
    this.cropper.replace(this.src);
  }

  reset() {
    this.cropper.reset();
  }

  crop() {
    this.cropper.crop();
  }
  destroy() {
    this.cropper.destroy();
  }

  toDataURL() {
    return this.cropper.getCroppedCanvas({fillColor: '#fff', width: this.widthRatio, height: this.heightRatio}).toDataURL('image/jpeg');
  }

}
