import { Component, OnInit,
  Input, Output, EventEmitter, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import * as Swiper from 'swiper/js/swiper.min.js';

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss']
})

export class SwiperComponent implements OnInit, AfterViewInit {
  @Input() delay: number;
  @Input() speed: number;
  @Input() autoplay: boolean;
  @Input() loop: boolean;
  @Input() pagination: any;
  @Input() typeBullet: string;
  @Input() dynamicBullets: boolean;
  @Input() navigation: any;
  @Input() widthSwiper: number;
  @Input() slug: number;
  @Input() nameSwiper: string;
  @Input() autoHeight: boolean;
  @Input() createOnInit: boolean;
  @Input() selectorHeight: string;
  @Input() fillHeight: boolean;
  @Input() clickable: boolean;
  @Input() loopedSlides: number;
  @Input() initialSlide: number;
  @Input() opacityInitial: number;
  @Input() spaceBetween: number;
  @Input() slidesPerView: string;
  @Input() centeredSlides: boolean;
  @Output() getSwiper = new EventEmitter();
  @ViewChild('swiper', { static: false }) swiperElement: ElementRef;
  private swiper: any = false;

  constructor(private element: ElementRef) {}

  ngOnInit() {
    
  }
  ngAfterViewInit() {
    this.swiperElement.nativeElement.style.opacity = this.opacityInitial === undefined ? 1 : this.opacityInitial;
    if (this.loopedSlides === 0) {
      this.loopedSlides = 1;
    }
    if (this.pagination !== undefined) {
      this.pagination.el = '.swiper-pagination';
    }
    if (this.navigation !== undefined) {
      this.navigation.nextEl = '.swiper-button-next';
      this.navigation.prevEl = '.swiper-button-prev';
    }
    this.swiper = new Swiper (this.swiperElement.nativeElement, {
      slidesPerView: this.slidesPerView === undefined ? 1 : this.slidesPerView,
      name: this.nameSwiper,
      cache: false,
      spaceBetween: this.spaceBetween === undefined ? 0 : this.spaceBetween,
      init: this.createOnInit === true || this.createOnInit === undefined ? true : false,
      centeredSlides: this.centeredSlides === true ? true : false,
      loop: this.loop === true ? true : false,
      loopedSlides: this.loopedSlides === undefined ? null : this.loopedSlides,
      autoHeight: this.autoHeight === true ? true : false,
      dynamicBullets: this.dynamicBullets === true ? true : false,
      initialSlide: this.initialSlide === undefined ? 0 : this.initialSlide,
      autoplay: this.autoplay === true ? {
        delay: this.delay ? this.delay : 3500,
        disableOnInteraction: false
      } : '',
      pagination: this.pagination === undefined ? '' : this.pagination,
      keyboard: {
        enabled: true,
        onlyInViewport: false,
      },
      navigation: this.navigation === undefined ? '' : this.navigation,
      speed: this.speed || 500,
      draggable: true,
      width: this.widthSwiper || undefined,
      on: {},
    });
    this.getSwiper.emit(this);
  }

  initSwiper() {
    setTimeout(() => {
      this.swiper.init();
      if (this.fillHeight === true) {
        this.matchHeight(this.selectorHeight);
      }
      this.swiperElement.nativeElement.style.opacity = 1;
    });
  }
  getName() {
    return this.nameSwiper;
  }
  update() {
    this.swiper.update();
    if (this.fillHeight === true) {
      setTimeout(() => {
        this.matchHeight(this.selectorHeight);
      });
    }
  }
  slideTo(index) {
    if (this.swiper) {
      this.swiper.slideTo(index);
    }
  }
  matchHeight(selectorHeight: string ) {
    const elements: HTMLElement[] =  this.element.nativeElement.getElementsByClassName(selectorHeight);
    const itemHeights = Array.from(elements).map(x => x.getBoundingClientRect().height);
    const maxHeight = itemHeights.reduce((prev, curr) => {
      return curr > prev ? curr : prev;
    }, 0);
    Array.from(elements)
      .forEach((x: HTMLElement) => x.style.height = `${maxHeight}px`);
  }
}


