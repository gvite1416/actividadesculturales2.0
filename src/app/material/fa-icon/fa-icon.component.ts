import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'fa-icon',
  templateUrl: './fa-icon.component.html',
  styleUrls: ['./fa-icon.component.scss', '../colors-icon.scss']
})
export class FaIconComponent implements OnInit {
  protected _elementClass: string[] = [];
  @Input() name: string;
  @Input() color: string;
  @Input() size: string;
  @Input() animate: boolean;
  @Input() istyle: string;
  constructor(private elRef: ElementRef) { }

  ngOnInit() {
    if (this.elRef.nativeElement.className !== '') {
      this._elementClass = this.elRef.nativeElement.className.split(' ');
    }
    if (this.istyle === 'fab') {
      this._elementClass.push('fab');
    } else {
      this._elementClass.push('fas');
    }
    this._elementClass.push('fa-' + this.name);
    if (this.color) {
      this._elementClass.push('icon-md-' + this.color);
    }
    if (this.animate) {
      this._elementClass.push('icon-animate');
    }
    this.elRef.nativeElement.className = this._elementClass.join(' ');
    if (this.size) {
      this.elRef.nativeElement.style.fontSize = this.size;
    }
  }
}
