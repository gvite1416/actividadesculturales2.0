import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-svg-img',
  templateUrl: './svg-img.component.html',
  styleUrls: ['./svg-img.component.scss']
})
export class SvgImgComponent implements OnInit, AfterViewInit {
  protected _elementClass: string[] = [];
  @Input() name: string;
  @Input() alt: string;
  @Input() size: string;
  @Input() borderImg: boolean;
  @ViewChild('image', { static: false }) image;
  src: string;
  constructor(private elRef: ElementRef) { }

  ngOnInit() {
    if (this.elRef.nativeElement.className !== '') {
      this._elementClass = this.elRef.nativeElement.className.split(' ');
    }
    this._elementClass.push('app-svg-img');
    this.src = 'assets/svg/' + this.name + '.svg';
    if (this.borderImg) {
      this._elementClass.push('app-svg-img-border');
    }
    this.elRef.nativeElement.className = this._elementClass.join(' ');
  }
  ngAfterViewInit() {
    if (this.size) {
      this.image.nativeElement.style.width = this.size;
      this.image.nativeElement.style.height = this.size;
    }
  }

}
