import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-modal-load',
  templateUrl: './modal-load.component.html',
  styleUrls: ['./modal-load.component.scss']
})
export class ModalLoadComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalLoadComponent>
  ) { }

  ngOnInit() {
  }

}
