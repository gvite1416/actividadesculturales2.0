import { Injectable, NgModule } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators/index';
import { throwError } from 'rxjs/index';
import { Router } from '@angular/router';
@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  constructor(private router: Router) {

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (localStorage.getItem('token') !== null && localStorage.getItem('token') !== 'undefined') {
      req.headers.set('Content-Type',  'application/json');
      const dupReq = req.clone({ headers: req.headers.set('Authorization',  'Bearer ' + localStorage.getItem('token'))});

      return next.handle(dupReq).pipe(map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((err: any, caught) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401 || err.status === 403 || (err.status === 500 && err.error.message === 'Token has expired and can no longer be refreshed')) {
            localStorage.removeItem('token');
            this.router.navigate(['/']);
            window.location.reload();
          }
          return throwError(err);
        }
      }));
    } else {
      return next.handle(req).pipe(map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((err: any, caught) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(['/']);
          }
          return throwError(err);
        }
      }));
    }
  }
}

@NgModule({
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true
  }]
})
export class InterceptorModule { }
