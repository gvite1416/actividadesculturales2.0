import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  private token: string;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  passFormGroup: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    public snackBar: MatSnackBar,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['token'];
    });
    /*dialogRef.afterOpen().subscribe(() => {
      dialogRef.componentInstance.updateSwiper();
    });*/
    this.createFormPassword();
  }
  private createFormPassword() {
    this.passFormGroup = new FormGroup({
      password: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      confirmPassword: new FormControl('', [Validators.required]),
    }, passwordMatchValidator);
    function passwordMatchValidator(g: FormGroup) {
      if (g.controls.password.value === g.controls.confirmPassword.value && g.controls.password.value !== '') {
        g.controls.password.setErrors(null);
        g.controls.confirmPassword.setErrors(null);
        return null;
      } else {
        g.controls.password.setErrors({'passwordMatch': true});
        g.controls.confirmPassword.setErrors({'passwordMatch': true});
        return {'mismatch': true};
      }
    }
  }
  updatePass() {
    this.userService.changePass({
      token: this.token,
      password: this.passFormGroup.value.password,
      email: this.passFormGroup.value.email,
      password_confirmation: this.passFormGroup.value.confirmPassword
    }).subscribe( (response) => {
        this.snackBar.open('Contraseña actualizada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['login']);
    }, response => {
      this.snackBar.open(response.error.message, '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
    });
  }

}
