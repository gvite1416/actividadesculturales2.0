import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

  loginForm: FormGroup;
  isLogin: boolean;
  errorMessage: string;
  loadLogin; boolean;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private userService: UserService,
    public snackBar: MatSnackBar
  ) {
    this.loadLogin = false;
  }

  ngOnInit() {
    this.createFormLogin();
  }

  private createFormLogin() {
    this.loginForm = new FormGroup({
      user: new FormControl('', [Validators.required])
    });
  }

  public onSubmit() {
    this.loadLogin = true;
    this.userService.recoverPass(this.loginForm.value.user).subscribe(response => {
        if (response.success) {
          this.snackBar.open(response.message, '', {
            duration: 5000,
            verticalPosition: this.positionVertical
          });
          this.loadLogin = false;
        } else {
          this.errorMessage = response.message;
          this.isLogin = false;
          this.loadLogin = false;
        }
      }, response => {
        this.errorMessage = response.error.message;
        this.isLogin = false;
        this.loadLogin = false;
    });
  }

}
