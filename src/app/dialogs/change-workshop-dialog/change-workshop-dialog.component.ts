import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WorkshopPeriodService } from '../../services/workshop-period.service';
import { WorkshopPeriod } from '../../models/workshop-period';
import { InscriptionService } from '../../services/inscription.service';
import { Workshop } from '../../models/workshop';

@Component({
  selector: 'app-change-workshop-dialog',
  templateUrl: './change-workshop-dialog.component.html',
  styleUrls: ['./change-workshop-dialog.component.scss']
})
export class ChangeWorkshopDialogComponent implements OnInit {
  workshopPeriods: WorkshopPeriod[];
  loadSpinner: boolean;
  workshopPeriod: WorkshopPeriod;
  inscription_id: number;
  constructor(
    private workshopPeriodService: WorkshopPeriodService,
    private inscriptionService: InscriptionService,
    public dialogRef: MatDialogRef<ChangeWorkshopDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loadSpinner = true;
    this.workshopPeriod = new WorkshopPeriod;
    this.workshopPeriod.workshop = new Workshop;
    this.inscription_id = data.inscription_id;
    this.workshopPeriods = [];
    console.log(data);
    if (data.workshopPeriods === undefined) {
      this.workshopPeriodService.getAllByPeriodId(data.period_id).subscribe ( (workshopPeriods) => {
        this.workshopPeriods = workshopPeriods;
        console.log(this.workshopPeriods);
        this.loadSpinner = false;
      });
    } else {
      this.workshopPeriods = data.workshopPeriods;
      this.loadSpinner = false;
    }
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close({workshopPeriods: this.workshopPeriods, status: 0, success: false});
  }

  select(workshopPeriod: WorkshopPeriod) {
    this.workshopPeriod = workshopPeriod;
  }

  change() {
    this.inscriptionService.change(this.inscription_id, this.workshopPeriod.id).subscribe((response) => {
      if (response.success) {
        this.dialogRef.close({workshopPeriods: this.workshopPeriods, status: 1, success: true});
      } else {
        this.dialogRef.close({workshopPeriods: this.workshopPeriods, status: 1, success: false});
      }
    });
  }

}
