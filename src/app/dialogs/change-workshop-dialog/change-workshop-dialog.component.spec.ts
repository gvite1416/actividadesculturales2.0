import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeWorkshopDialogComponent } from './change-workshop-dialog.component';

describe('ChangeWorkshopDialogComponent', () => {
  let component: ChangeWorkshopDialogComponent;
  let fixture: ComponentFixture<ChangeWorkshopDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeWorkshopDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeWorkshopDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
