import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-college-degree-dialog',
  templateUrl: './college-degree-dialog.component.html',
  styleUrls: ['./college-degree-dialog.component.scss']
})
export class CollegeDegreeDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CollegeDegreeDialogComponent>) { }

  ngOnInit() {
  }
  close(response: boolean) {
    this.dialogRef.close(response);
  }
}
