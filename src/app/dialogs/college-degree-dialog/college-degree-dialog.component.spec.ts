import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeDegreeDialogComponent } from './college-degree-dialog.component';

describe('CollegeDegreeDialogComponent', () => {
  let component: CollegeDegreeDialogComponent;
  let fixture: ComponentFixture<CollegeDegreeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollegeDegreeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeDegreeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
