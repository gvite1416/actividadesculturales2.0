import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { User } from '../../models/user';
import { MatDialogRef, MAT_DIALOG_DATA, MatStepper } from '@angular/material';
import { UserService } from '../../services/user.service';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../services/globals';
import { CropperComponent } from '../../material/cropper/cropper.component';
import { PhotoRequirement } from '../../models/photo-requirement';
import { ImageFile } from '../../models/image-file';
import { UploadsService } from '../../services/uploads.service';

@Component({
  selector: 'app-photo-dialog',
  templateUrl: './photo-dialog.component.html',
  styleUrls: ['./photo-dialog.component.scss']
})
export class PhotoDialogComponent implements OnInit {
  disableBtnYes: boolean;
  user: User;
  element: any;
  image: ImageFile;
  storageUrl: string = GLOBALS.storageUrl;
  showCropper: boolean;
  preview: string;
  step: number;
  created: boolean;
  requirements: PhotoRequirement[];
  @ViewChild('stepper', { static: false }) stepper: MatStepper;
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/images/profile',
    itemAlias: 'photo',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  cropper: CropperComponent;
  constructor(
    private userService: UserService,
    private uploadService: UploadsService,
    public dialogRef: MatDialogRef<PhotoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.element = data.element;
    this.disableBtnYes = false;
    this.showCropper = false;
    this.preview = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
    this.step = 0;
    this.created = false;
    this.image = new ImageFile();
    this.requirements = [
      new PhotoRequirement({id: 1, title: 'Fondo Blanco', checked: false}),
      new PhotoRequirement({id: 2, title: 'De frente', checked: false}),
      new PhotoRequirement({id: 3, title: 'Cara completa', checked: false}),
      new PhotoRequirement({id: 4, title: 'No mayor a 30 días', checked: false})
    ];
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => {
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.image = new ImageFile(response);
      this.step = 1;
      this.uploader.clearQueue();
      setTimeout(() => {
        this.stepper.selectedIndex = 1;
        if (!this.created) {
          this.cropper.create();
          this.created = true;
        } else {
          this.cropper.destroy();
          this.cropper.create();
        }
      }, 0);
    };
  }
  selectionChange(stepper: MatStepper) {
    this.step = stepper.selectedIndex;
  }
  prevStep() {
    this.step = this.stepper.selectedIndex;
    if (this.step > 0) {
      this.step--;
      this.stepper.selectedIndex = this.step;
    }
  }
  nextStep() {
    this.step = this.stepper.selectedIndex;
    if (this.step < 2) {
      this.step++;
      this.stepper.selectedIndex = this.step;
      if (this.step === 2) {
        this.preview = this.cropper.toDataURL();
      }
    }
  }
  showPreview() {
    this.preview = this.cropper.toDataURL();
    this.step = 2;
  }
  save() {
    this.uploadService.deleteTmp(this.image.name).subscribe(response => {
      this.dialogRef.close(this.preview);
    });
  }
  close() {
    this.dialogRef.close();
  }
  getCropper(cropper: CropperComponent) {
    this.cropper = cropper;
    // this.cropper.create();
  }
}
