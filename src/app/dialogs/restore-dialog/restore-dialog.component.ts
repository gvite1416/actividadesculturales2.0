import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WorkshopService } from '../../services/workshop.service';
import { PeriodService } from '../../services/period.service';
import { TeacherService } from '../../services/teacher.service';
import { CampusService } from '../../services/campus.service';
import { CareerService } from '../../services/career.service';
import { OccupationService } from '../../services/occupation.service';
import { ClassroomService } from '../../services/classroom.service';
import { WorkshopPeriodService } from '../../services/workshop-period.service';
import { RequirementService } from '../../services/requirement.service';
import { SliderService } from '../../services/slider.service';
import { OfferingService } from '../../services/offering.service';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-restore-dialog',
  templateUrl: './restore-dialog.component.html',
  styleUrls: ['./restore-dialog.component.scss']
})
export class RestoreDialogComponent implements OnInit {
  element: any;
  typeElement: string;
  disableBtnYes: boolean;
  constructor(
    private workshopService: WorkshopService,
    private periodService: PeriodService,
    private teacherService: TeacherService,
    private careerService: CareerService,
    private campusService: CampusService,
    private occupationService: OccupationService,
    private classroomService: ClassroomService,
    private workshopPeriodService: WorkshopPeriodService,
    private requirementService: RequirementService,
    private sliderService: SliderService,
    private offeringService: OfferingService,
    private eventService: EventService,
    private userService: UserService,
    public dialogRef: MatDialogRef<RestoreDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.element = data.element;
      this.element.name = data.element.name || 'el elemento';
      this.typeElement = data.type;
      this.disableBtnYes = false;
    }

  ngOnInit() {
  }

  restore() {
    this.disableBtnYes = true;
    switch (this.typeElement) {
      case 'workshop':
        this.workshopService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'period':
        this.periodService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'teacher':
        this.teacherService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'campus':
        this.campusService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'career':
        this.careerService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'occupation':
        this.occupationService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'classroom':
        this.classroomService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'workshopPeriod':
        this.workshopPeriodService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'requirement':
        this.requirementService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'slider':
        this.sliderService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'event':
        this.eventService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'offering':
        this.offeringService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
      case 'user':
        this.userService.restore(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        });
        break;
    }
  }

  close() {
    this.dialogRef.close();
  }

}
