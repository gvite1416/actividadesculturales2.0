import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatSnackBarVerticalPosition, MAT_DIALOG_DATA } from '@angular/material';
import { WorkshopService } from '../../services/workshop.service';
import { PeriodService } from '../../services/period.service';
import { TeacherService } from '../../services/teacher.service';
import { CampusService } from '../../services/campus.service';
import { CareerService } from '../../services/career.service';
import { OccupationService } from '../../services/occupation.service';
import { ClassroomService } from '../../services/classroom.service';
import { WorkshopPeriodService } from '../../services/workshop-period.service';
import { RequirementService } from '../../services/requirement.service';
import { SliderService } from '../../services/slider.service';
import { OfferingService } from '../../services/offering.service';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';
import { CapturelineService } from '../../services/captureline.service';
import { InscriptionService } from '../../services/inscription.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {
  element: any;
  typeElement: string;
  disableBtnYes: boolean;
  message: string;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  force: boolean;
  constructor(
    private workshopService: WorkshopService,
    private periodService: PeriodService,
    private teacherService: TeacherService,
    private careerService: CareerService,
    private campusService: CampusService,
    private occupationService: OccupationService,
    private classroomService: ClassroomService,
    private workshopPeriodService: WorkshopPeriodService,
    private requirementService: RequirementService,
    private sliderService: SliderService,
    private offeringService: OfferingService,
    private eventService: EventService,
    private userService: UserService,
    private capturelineService: CapturelineService,
    private inscriptionService: InscriptionService,
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    public snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.element = data.element;
      this.element.name = data.element.name || 'el elemento';
      this.typeElement = data.type;
      this.message = data.message || '¿Estas seguro de borrar ' + this.element.name + '?';
      this.force = data.force;
      this.disableBtnYes = false;
    }

  ngOnInit() {
  }

  delete() {
    this.disableBtnYes = true;
    switch (this.typeElement) {
      case 'workshop':
        this.workshopService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'period':
        this.periodService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'teacher':
        this.teacherService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'campus':
        this.campusService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'career':
        this.careerService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'occupation':
        this.occupationService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'classroom':
        this.classroomService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'workshopPeriod':
        this.workshopPeriodService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'requirement':
        this.requirementService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'slider':
        this.sliderService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'event':
        this.eventService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'offering':
        this.offeringService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'user':
        this.userService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'captureline':
        this.capturelineService.delete(this.element, this.force).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
      case 'inscription':
        this.inscriptionService.delete(this.element).subscribe( (response) => {
          this.disableBtnYes = false;
          this.dialogRef.close(response);
        }, result => {
          this.snackBar.open('Ocurrió un error, intenta más tarde', '', {
            duration: 3000,
            verticalPosition: this.positionVertical
          });
        });
        break;
    }
  }

  close() {
    this.dialogRef.close();
  }

}
