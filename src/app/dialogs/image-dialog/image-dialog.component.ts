import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatStepper } from '@angular/material';
import { CropperComponent } from '../../material/cropper/cropper.component';
import { GLOBALS } from '../../services/globals';
import { UploadsService } from '../../services/uploads.service';

@Component({
  selector: 'app-image-dialog',
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss']
})
export class ImageDialogComponent implements OnInit {
  cropper: CropperComponent;
  imageFullpath: string;
  preview: string;
  widthRatio: number;
  heightRatio: number;
  step: number;
  imageName: string;
  @ViewChild('stepperImage', { static: false }) stepper: MatStepper;
  constructor(
    private uploadService: UploadsService,
    public dialogRef: MatDialogRef<ImageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.imageName = data.image.name;
    this.imageFullpath = data.image.fullpath;
    this.widthRatio = data.widthRatio;
    this.heightRatio = data.heightRatio;
    this.preview = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';
    this.step = 0;
  }

  ngOnInit() {
  }
  nextStep() {
    this.step = 1;
    this.preview = this.cropper.toDataURL();
    this.stepper.selectedIndex = this.step;
  }
  prevStep() {
    this.step = 0;
    this.stepper.selectedIndex = this.step;
  }
  showPreview() {
    this.preview = this.cropper.toDataURL();
    this.stepper.selectedIndex = this.step;
  }
  save() {
    this.uploadService.saveBase64(this.preview, this.imageName).subscribe((response: any)=>{
      this.dialogRef.close(response.name);
    });
    
  }
  close() {
    this.dialogRef.close();
  }
  selectionChange(stepper: MatStepper) {
    this.step = stepper.selectedIndex;
  }
  getCropper(cropper: CropperComponent) {
    this.cropper = cropper;
    this.cropper.create();
  }

}
