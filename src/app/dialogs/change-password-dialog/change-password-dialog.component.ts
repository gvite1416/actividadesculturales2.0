import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.scss']
})
export class ChangePasswordDialogComponent implements OnInit {
  user: User;
  editFormLogin: FormGroup;
  loadSpinner: boolean;
  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.user = data.user;
    this.loadSpinner = false;
  }

  ngOnInit() {
    this.createFormData();
  }
  close() {
    this.dialogRef.close({user: this.user, status: 0, success: false});
  }
  private createFormData() {
    this.editFormLogin = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)])
    }, passwordMatchValidator);
    function passwordMatchValidator(g: FormGroup) {
      if (g.controls.password.value === g.controls.confirmPassword.value && g.controls.password.value !== '') {
        g.controls.password.setErrors(null);
        g.controls.confirmPassword.setErrors(null);
        return null;
      } else {
        g.controls.password.setErrors({'passwordMatch': true});
        g.controls.confirmPassword.setErrors({'passwordMatch': true});
        return {'mismatch': true};
      }
    }
  }

  change() {
    this.loadSpinner = true;
    this.userService.updatePassUser(
      this.user.id,
      this.editFormLogin.value.password,
      this.editFormLogin.value.confirmPassword).subscribe((response) => {
        this.loadSpinner = false;
        if (response.success) {
          this.dialogRef.close({user: this.user, status: 1, success: true});
        } else {
          this.dialogRef.close({user: this.user, status: 1, success: false});
        }
      });
  }

}
