import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inscription } from '../../models/inscription';
import { InscriptionService } from '../../services/inscription.service';
import { WorkshopPeriod } from '../../models/workshop-period';
import { Period } from '../../models/period';
import { Workshop } from '../../models/workshop';
import { User } from '../../models/user';

@Component({
  selector: 'app-inscription-dialog',
  templateUrl: './inscription-dialog.component.html',
  styleUrls: ['./inscription-dialog.component.scss']
})
export class InscriptionDialogComponent implements OnInit {
  inscription: Inscription;
  constructor(
    private inscriptionService: InscriptionService,
    public dialogRef: MatDialogRef<InscriptionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.inscription = new Inscription();
    this.inscription.workshopPeriod = new WorkshopPeriod();
    this.inscription.workshopPeriod.period = new Period();
    this.inscription.workshopPeriod.workshop = new Workshop();
    this.inscription.user = new User();
    this.inscriptionService.getById(data.inscriptionId).subscribe(inscription => {
        this.inscription = inscription;
    });
  }

  ngOnInit() {
  }
  close() {
    this.dialogRef.close();
  }

}
