import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-price-inscription-dialog',
  templateUrl: './price-inscription-dialog.component.html',
  styleUrls: ['./price-inscription-dialog.component.scss']
})
export class PriceInscriptionDialogComponent implements OnInit {
  priceFormLogin: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<PriceInscriptionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.createFormData();
  }
  private createFormData() {
    this.priceFormLogin = new FormGroup({
      price: new FormControl('', [Validators.required])});
  }
  close() {
    this.dialogRef.close({success: false, price: null});
  }
  setPrice() {
    this.dialogRef.close({success: true, price: this.priceFormLogin.value.price});
  }



}
