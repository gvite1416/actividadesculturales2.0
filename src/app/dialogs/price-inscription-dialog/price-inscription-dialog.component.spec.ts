import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceInscriptionDialogComponent } from './price-inscription-dialog.component';

describe('PriceInscriptionDialogComponent', () => {
  let component: PriceInscriptionDialogComponent;
  let fixture: ComponentFixture<PriceInscriptionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceInscriptionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceInscriptionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
