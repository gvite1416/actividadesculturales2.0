import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { CampusService } from '../services/campus.service';
import { CareerService } from '../services/career.service';
import { PhotoDialogComponent } from './photo-dialog/photo-dialog.component';
import { FileUploadModule } from 'ng2-file-upload';
import { MaterialModule } from '../material/material.module';
import { ChangeWorkshopDialogComponent } from './change-workshop-dialog/change-workshop-dialog.component';
import { ChangePasswordDialogComponent } from './change-password-dialog/change-password-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PriceInscriptionDialogComponent } from './price-inscription-dialog/price-inscription-dialog.component';
import { NgxMaskModule } from 'ngx-mask';
import { UploadRequirementDialogComponent } from './upload-requirement-dialog/upload-requirement-dialog.component';
import { InscriptionDialogComponent } from './inscription-dialog/inscription-dialog.component';
import { PipesModule } from '../pipes/pipes.module';
import { ImageDialogComponent } from './image-dialog/image-dialog.component';
import { RestoreDialogComponent } from './restore-dialog/restore-dialog.component';
import { CollegeDegreeDialogComponent } from './college-degree-dialog/college-degree-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    PipesModule
  ],
  exports: [
    DeleteDialogComponent,
    PhotoDialogComponent,
    ChangeWorkshopDialogComponent,
    ChangePasswordDialogComponent,
    PriceInscriptionDialogComponent,
    UploadRequirementDialogComponent,
    InscriptionDialogComponent,
    ImageDialogComponent,
    RestoreDialogComponent,
    CollegeDegreeDialogComponent
  ],
  declarations: [
    DeleteDialogComponent,
    PhotoDialogComponent,
    ChangeWorkshopDialogComponent,
    ChangePasswordDialogComponent,
    PriceInscriptionDialogComponent,
    UploadRequirementDialogComponent,
    InscriptionDialogComponent,
    ImageDialogComponent,
    RestoreDialogComponent,
    CollegeDegreeDialogComponent
  ],
  entryComponents: [
    DeleteDialogComponent,
    PhotoDialogComponent,
    ChangeWorkshopDialogComponent,
    ChangePasswordDialogComponent,
    PriceInscriptionDialogComponent,
    UploadRequirementDialogComponent,
    InscriptionDialogComponent,
    ImageDialogComponent,
    RestoreDialogComponent,
    CollegeDegreeDialogComponent
  ],
  providers: [CampusService, CareerService]
})
export class DialogsModule { }
