import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FileUploader } from 'ng2-file-upload';
import { GLOBALS } from '../../services/globals';
import { Requirement } from '../../models/requirement';

@Component({
  selector: 'app-upload-requirement-dialog',
  templateUrl: './upload-requirement-dialog.component.html',
  styleUrls: ['./upload-requirement-dialog.component.scss']
})
export class UploadRequirementDialogComponent implements OnInit {
  uploader: FileUploader = new FileUploader({
    url: GLOBALS.apiUrl + 'upload/requirement',
    itemAlias: 'filereq',
    authToken: `Bearer ${localStorage.getItem('token')}`
  });
  fileName: string;
  requirement: Requirement;
  loadSpinner: number;
  constructor(
    public dialogRef: MatDialogRef<UploadRequirementDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.requirement = data.requirement;
    this.loadSpinner = 0;
    this.uploader.options.url = GLOBALS.apiUrl + 'upload/requirement/' + this.requirement.id;
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => {
      this.loadSpinner = 1;
      file.upload();
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.fileName = response;
      this.uploader.clearQueue();
      this.loadSpinner = 2;
      setTimeout(() => {
        setTimeout(() => {
          this.dialogRef.close();
        }, 2000);
      }, 0);
    };
  }
  close() {
    this.dialogRef.close();
  }

}
