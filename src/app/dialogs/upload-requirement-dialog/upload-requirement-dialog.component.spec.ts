import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadRequirementDialogComponent } from './upload-requirement-dialog.component';

describe('UploadRequirementDialogComponent', () => {
  let component: UploadRequirementDialogComponent;
  let fixture: ComponentFixture<UploadRequirementDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadRequirementDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadRequirementDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
