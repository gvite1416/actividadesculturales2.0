import { IWorkshopPeriod } from '../interfaces/workshop-period';
import { Period } from './period';
import { Workshop } from './workshop';
import { Teacher } from './teacher';
import { Classroom } from './classroom';
import { Schedule } from './schedule';
import { WorkshopPeriodRole } from './workshop-period-role';
import { WorkshopPeriodCampus } from './workshop-period-campus';

export class WorkshopPeriod implements IWorkshopPeriod {
    id: number;
    period_id: number;
    workshop_id: number;
    teacher_id: number;
    classroom_id: number;
    quota: number;
    group: string;
    period: Period;
    workshop?: any;
    teacher: Teacher;
    classroom: Classroom;
    schedules: Schedule[];
    students: WorkshopPeriodRole[];
    campus: WorkshopPeriodCampus[];
    students_inscribed_count: number;
    students_inscribed_count_validated: number;
    inscribed: boolean;
    can: boolean;
    cant_message: string;
    all_campus: boolean;
    notes: string;
    deleted_at: string;
    constructor(item?, utc?) {
        if (item) {
            this.id = +item.id;
            this.period_id = +item.period_id;
            this.workshop_id = +item.workshop_id;
            this.teacher_id = +item.teacher_id;
            this.classroom_id = +item.classroom_id;
            this.quota = +item.quota;
            this.group = item.group;
            this.can = item.can;
            this.inscribed = item.inscribed;
            this.cant_message = item.cant_message;
            this.notes = item.notes;
            this.all_campus = item.all_campus === 1 ? true : false;
            this.students_inscribed_count = +item.students_inscribed_count;
            this.students_inscribed_count_validated = +item.students_inscribed_count_validated;
            this.deleted_at = item.deleted_at;
            if (item.period) {
                this.period = new Period(item.period);
            }
            if (item.workshop) {
                this.workshop = new Workshop(item.workshop);
            }
            if (item.teacher) {
                this.teacher = new Teacher(item.teacher);
            }
            if (item.classroom) {
                this.classroom = new Classroom(item.classroom);
            }
            if (item.schedules !== undefined) {
                this.schedules = item.schedules.map(schedule => new Schedule(schedule , utc));
            }
            if (item.students !== undefined) {
                this.students = item.students.map(workshopPeriodRole => new WorkshopPeriodRole(workshopPeriodRole));
            }
            if (item.campus !== undefined) {
                this.campus = item.campus.map(workshopPeriodCampus => new WorkshopPeriodCampus(workshopPeriodCampus));
            }
        }
    }
}
