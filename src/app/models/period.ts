import { IPeriod } from '../interfaces/period';
import * as moment from 'moment';
import { Requirement } from './requirement';
export class Period implements IPeriod {
    id: number;
    name: string;
    start: moment.Moment;
    finish: moment.Moment;
    inscription_start: moment.Moment;
    inscription_finish: moment.Moment;
    requirements: Requirement[];
    deleted_at: string;
    constructor(item?, utc?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            if ( utc === true) {
                this.start =  moment(item.start + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                this.finish =  moment(item.finish + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                this.inscription_start = moment(item.inscription_start + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                this.inscription_finish = moment(item.inscription_finish + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
            } else {
                this.start =  moment(item.start);
                this.finish =  moment(item.finish);
                this.inscription_start = moment(item.inscription_start);
                this.inscription_finish = moment(item.inscription_finish);
            }
            if (item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
            if (item.requirements) {
                this.requirements = item.requirements.map(req => new Requirement(req));
            }
        }
    }
}
