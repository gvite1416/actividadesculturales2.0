import { ISlider } from '../interfaces/slider';

export class Slider implements ISlider {
    id: number;
    title: string;
    img_desktop: string;
    img_mobile: string;
    link: string;
    order: number;
    img_desktop_url: string;
    img_mobile_url: string;
    deleted_at: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.title = item.title;
            this.img_desktop = item.img_desktop;
            this.img_mobile = item.img_mobile;
            this.img_desktop_url = item.img_desktop_url;
            this.img_mobile_url = item.img_mobile_url;
            this.link = item.link;
            this.order = item.order;
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
