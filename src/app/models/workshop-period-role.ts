import { IRole } from '../interfaces/role';

export class WorkshopPeriodRole implements IRole {
    id: number;
    name: string;
    slug: string;
    description: string;
    pivot: {
        id: number;
        workshop_period_id: number;
        role_id: number;
        limit: number;
        price: number;
    };
    constructor (item?) {
        if (item) {
            this.id = +item.id;
            this.name = item.name;
            this.slug = item.slug;
            this.description = item.description;
            this.pivot = {
                id: item.pivot.id,
                workshop_period_id: item.pivot.workshop_period_id,
                role_id: item.pivot.role_id,
                limit: item.pivot.limit,
                price: item.pivot.price
            };
        }
    }
}
