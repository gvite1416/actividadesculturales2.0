import { IDataExternal } from '../interfaces/data-external';
import { Occupation } from './occupation';

export class DataExternal implements IDataExternal {
    id:  number;
    address: string;
    occupation_id: number;
    occupation: Occupation;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.address = item.address;
            this.occupation_id = item.occupation_id;
            if (item.occupation) {
                this.occupation = new Occupation(item.occupation);
            }
        }
    }
}
