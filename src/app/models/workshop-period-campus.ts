import { ICampus } from '../interfaces/campus';

export class WorkshopPeriodCampus implements ICampus {
    id: number;
    name: string;
    code: string;
    pivot: {
        id: number;
        workshop_period_id: number;
        campus_id: number;
    };
    constructor (item?) {
        if (item) {
            this.id = +item.id;
            this.name = item.name;
            this.code = item.code;
            if (item.pivot) {
                this.pivot = {
                    id: item.pivot.id,
                    workshop_period_id: item.pivot.workshop_period_id,
                    campus_id: item.pivot.campus_id
                };
            }
        }
    }
}
