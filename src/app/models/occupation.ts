import { IOccupation } from '../interfaces/occupation';

export class Occupation implements IOccupation {
    id: number;
    name: string;
    deleted_at: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
