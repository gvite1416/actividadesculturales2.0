import { IEventCategory } from '../interfaces/event-category';

export class EventCategory implements IEventCategory {
    id: number;
    name: string;
    slug: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            this.slug = item.slug;
        }
    }
}
