import { IDataStudent } from '../interfaces/data-student';
import { Campus } from './campus';
import { Career } from './career';

export class DataStudent implements IDataStudent {
    id: number;
    year: number;
    career_id: number;
    campus_id: number;
    semester: number;
    campus: Campus;
    career: Career;
    system: string;
    constructor (item?) {
        if (item) {
            this.id = item.id;
            this.year = item.year;
            this.career_id = item.career_id;
            this.campus_id = item.campus_id;
            this.system = item.system;
            this.semester = item.semester;
            this.campus = new Campus(item.campus);
            this.career = new Career(item.career);
        }
    }
}
