import { IUser } from '../interfaces/user';

export class Teacher implements IUser {
    id: number;
    name: string;
    lastname: string;
    surname: string;
    email: string;
    email_confirmation: string;
    phone: number;
    celphone: number;
    password: string;
    password_confirmation: string;
    photo: string;
    gender: string;
    qr: string;
    is_verified: number;
    is_verified_dept: number;
    deleted_at: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            this.lastname = item.lastname;
            this.surname = item.surname;
            this.email = item.email;
            this.phone = item.phone;
            this.celphone = item.celphone;
            this.photo = item.photo;
            this.gender = item.gender;
            this.qr = item.qr;
            this.is_verified = item.is_verified;
            this.is_verified_dept = item.is_verified_dept;
            if (item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
