import * as moment from 'moment';
import { ICaptureline } from '../interfaces/captureline';
import { Inscription } from './inscription';
export class Captureline implements ICaptureline {
    id: number;
    pathname: string;
    inscription_id: number;
    agreement: string;
    reference: string;
    date_valid: moment.Moment;
    inscription: Inscription;
    img_url: string;
    value: number;
    deleted_at: string;
    constructor(item?, utc?) {
        if (item) {
            this.id = item.id;
            this.pathname = item.pathname;
            this.inscription_id = item.inscription_id;
            this.agreement = item.agreement;
            this.reference = item.reference;
            this.img_url = item.img_url;
            this.value = item.value;
            this.value = item.value;
            if ( utc === true) {
                if (item.date_valid) {
                    this.date_valid = moment(item.date_valid + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                }
            } else {
                if (item.date_valid) {
                    this.date_valid =  moment(item.date_valid);
                }
            }
            if (item.inscription) {
                this.inscription = new Inscription(item.inscription);
            }
            if (item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}