import { IUser } from '../interfaces/user';
import { Role } from './role';
export class UserAdmin implements IUser {
    id: number;
    name: string;
    lastname: string;
    surname: string;
    email: string;
    email_confirmation: string;
    password: string;
    password_confirmation: string;
    phone: number;
    celphone: number;
    user_type: string;
    roles: Role[];
    photo: string;
    photo_url: string;
    gender: string;
    qr: string;
    is_verified: number;
    is_verified_dept: number;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.name = (item.name !== undefined) ? item.name : '';
            this.lastname = (item.lastname !== undefined) ? item.lastname : '';
            this.surname = (item.surname !== undefined) ? item.surname : '';
            this.email = (item.email !== undefined) ? item.email : '';
            this.phone = (item.phone !== undefined) ? item.phone : '';
            this.celphone = (item.celphone !== undefined) ? item.celphone : '';
            this.user_type = (item.user_type !== undefined) ? item.user_type : '';
            this.photo = item.photo;
            this.photo_url = item.photo_url;
            this.gender = item.gender;
            this.qr = item.qr;
            this.is_verified = item.is_verified;
            this.is_verified_dept = item.is_verified_dept;
            if (item.roles) {
                this.roles = item.roles.map( role => new Role(role));
            }
        }
    }
}
