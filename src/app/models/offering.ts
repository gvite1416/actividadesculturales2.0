import { IOffering } from '../interfaces/offering';

export class Offering implements IOffering {
    id: number;
    title: string;
    description: string;
    image: string;
    number: number;
    event_id: number;
    deleted_at: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.title = item.title;
            this.description = item.description;
            this.image = item.image;
            this.number = +item.number;
            this.event_id = +item.event_id;
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
