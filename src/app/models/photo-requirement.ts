import { IPhotoRequirement } from '../interfaces/photo-requirement';

export class PhotoRequirement implements IPhotoRequirement {
    id: number;
    title: string;
    checked: boolean;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.title = item.title;
            this.checked = item.checked;
        }
    }
}
