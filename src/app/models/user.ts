import { IUser } from '../interfaces/user';
import { Role } from './role';
import { DataExternal } from './data-external';
import { DataStudent } from './data-student';
import { DataEmployee } from './data-employee';
export class User implements IUser {
    public id: number;
    public name: string;
    public lastname: string;
    public surname: string;
    public email: string;
    public email_confirmation: string;
    public password: string;
    public password_confirmation: string;
    public phone: number;
    public celphone: number;
    public student_type: string;
    public student_type_id: number;
    public number_id: number;
    public year: number;
    public career_id: number;
    public campus_id: number;
    public system: string;
    public semester: number;
    public workshift: string;
    public area: string;
    public occupation_id: number;
    public address: string;
    public roles: Role[];
    public role: Role;
    public data_external: DataExternal;
    public data_student: DataStudent;
    public data_employee: DataEmployee;
    public photo: string;
    public photo_url: string;
    public gender: string;
    public qr: string;
    public is_verified: number;
    public is_verified_dept: number;
    public sendCaptureline: boolean;
    deleted_at: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.name = (item.name !== undefined) ? item.name : '';
            this.lastname = (item.lastname !== undefined) ? item.lastname : '';
            this.surname = (item.surname !== undefined) ? item.surname : '';
            this.email = (item.email !== undefined) ? item.email : '';
            this.photo = item.photo;
            this.photo_url = item.photo_url;
            this.phone = (item.phone !== undefined) ? item.phone : '';
            this.celphone = (item.celphone !== undefined) ? item.celphone : '';
            this.student_type = (item.student_type !== undefined) ? item.student_type : '';
            this.number_id = (item.number_id !== undefined) ? item.number_id : '';
            this.gender = item.gender;
            this.qr = item.qr;
            this.is_verified = item.is_verified;
            this.is_verified_dept = item.is_verified_dept;
            if (item.roles) {
                this.roles = item.roles.map( (role: Role) => {
                    this.student_type = role.slug;
                    this.student_type_id = role.id;
                    return new Role(role);
                });
            }
            if(item.role){
                this.student_type = item.role.slug;
                this.student_type_id = item.role.id;
                this.role = new Role(item.role);
            }
            if (item.data_external !== undefined && item.data_external.length > 0) {
                this.data_external = (item.data_external) ? new DataExternal(item.data_external[0]) : new DataExternal();
            }
            if (item.data_student !== undefined && item.data_student.length > 0) {
                this.data_student = (item.data_student) ? new DataStudent(item.data_student[0]) : new DataStudent();
            }
            if (item.data_employee !== undefined && item.data_employee.length > 0) {
                this.data_employee = (item.data_employee) ? new DataEmployee(item.data_employee[0]) : new DataEmployee();
            }
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
        this.sendCaptureline = false;
    }
}
