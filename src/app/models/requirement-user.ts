export class RequirementUser {
    name: string;
    user_id: number;
    role_id: number;
    has_file: boolean;
    deleted_at: string;
    upload_type: string;
    period_requirement_id: number;
    inscription_id: number;
    file: number;
    constructor (item?) {
        if (item) {
            this.name = item.name;
            this.role_id = item.role_id;
            this.user_id = item.user_id;
            this.has_file = item.has_file;
            this.upload_type = item.upload_type;
            this.period_requirement_id = item.period_requirement_id;
            this.inscription_id = item.inscription_id;
            this.file = item.file;
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
