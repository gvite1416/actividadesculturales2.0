import * as moment from 'moment';
import { IShowing } from '../interfaces/showing';
export class Showing implements IShowing {
    id: number;
    start: moment.Moment;
    finish: moment.Moment;
    can: boolean;
    students_inscribed_count: number;
    inscribed: boolean;
    insc_id: number;
    cant_message: string;
    role_user: string;
    constructor(item? , utc?) {
        if (item) {
            this.id = item.id;
        }
        if ( utc === true) {
            this.start =  moment(item.start + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
            this.finish =  moment(item.finish + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
        } else {
            this.start =  moment(item.start);
            this.finish =  moment(item.finish);
        }
        this.inscribed = item.inscribed;
        this.insc_id = item.insc_id;
        this.cant_message = item.cant_message;
        this.can = item.can;
        this.role_user = item.role_user;
        this.students_inscribed_count = +item.students_inscribed_count;
    }
}
