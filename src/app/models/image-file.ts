export class ImageFile {
    name: string;
    fullpath: string;
    constructor(response?: string){
        if (response){
            let json = JSON.parse(response);
            this.name = json.name;
            this.fullpath = json.fullpath;
        }
    }
}