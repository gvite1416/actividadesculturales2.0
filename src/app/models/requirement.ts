import { IRequirement } from '../interfaces/requirement';

export class Requirement implements IRequirement {
    id: number;
    name: string;
    role_id: number;
    active: boolean;
    has_file: boolean;
    deleted_at: string;
    upload_type: string;
    constructor (item?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            this.role_id = item.role_id;
            this.active = item.active;
            this.has_file = item.has_file;
            this.upload_type = item.upload_type;
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
