import * as moment from 'moment';
import { IEventAC } from '../interfaces/event';
import { EventRole } from './event-role';
import { Classroom } from './classroom';
import { EventCategory } from './event-category';
import { Offering } from './offering';
import { Showing } from './showing';
import { EventCampus } from './event-campus';
export class EventAC implements IEventAC {
    id: number;
    title: string;
    slug: string;
    image: string;
    image_url: string;
    thumbnail: string;
    thumbnail_url: string;
    ticketimage: string;
    ticketimage_url: string;
    description: string;
    all_campus: boolean;
    /*start: moment.Moment;
    finish: moment.Moment;*/
    inscription_start: moment.Moment;
    inscription_finish: moment.Moment;
    quota: number;
    event_category_id: number;
    classroom_id: number;
    event_category: EventCategory;
    classroom: Classroom;
    students: EventRole[];
    offerings: Offering[];
    showings: Showing[];
    campus: EventCampus[];
    has_user: boolean;
    deleted_at: string;
    constructor(item?, utc?) {
        if (item) {
            this.id = item.id;
            this.title = item.title;
            this.slug = item.slug;
            this.image = item.image;
            this.image_url = item.image_url;
            this.thumbnail = item.thumbnail;
            this.thumbnail_url = item.thumbnail_url;
            this.ticketimage = item.ticketimage;
            this.ticketimage_url = item.ticketimage_url;
            this.description = item.description;
            this.all_campus = item.all_campus === 1 ? true : false;
            this.has_user = item.has_user === true ? true : false;
            this.quota = item.quota;
            if ( utc === true) {
                /*this.start =  moment(item.start + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                this.finish =  moment(item.finish + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');*/
                this.inscription_start = item.inscription_start ? moment(
                    item.inscription_start + ' +0000', 'YYYY-MM-DD HH:mm:ss Z') : moment();
                this.inscription_finish = item.inscription_finish ? moment(
                    item.inscription_finish + ' +0000', 'YYYY-MM-DD HH:mm:ss Z') : moment();
            } else {
                /*this.start =  moment(item.start);
                this.finish =  moment(item.finish);*/
                this.inscription_start = item.inscription_start ? moment(item.inscription_start) : moment();
                this.inscription_finish = item.inscription_finish ? moment(item.inscription_finish) : moment();
            }
            this.event_category_id = item.event_category_id;
            this.classroom_id = item.classroom_id;
            this.classroom = new Classroom(item.classroom);
            this.event_category = new EventCategory(item.event_category);
            if (item.students !== undefined) {
                this.students = item.students.map(eventRole => new EventRole(eventRole));
            }
            if (item.campus !== undefined) {
                this.campus = item.campus.map(eventCampus => new EventCampus(eventCampus));
            }
            if (item.offerings !== undefined) {
                this.offerings = item.offerings.map(offering => new Offering(offering));
            }
            if (item.showings) {
                this.showings = item.showings.map( showing => new Showing(showing, utc));
            }
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
