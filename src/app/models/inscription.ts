import * as moment from 'moment';
import { IInscription } from '../interfaces/inscription';
import { Captureline } from './captureline';
import { User } from './user';
import { WorkshopPeriod } from './workshop-period';
export class Inscription implements IInscription {
    id: number;
    workshop_period_id: number;
    user_id: number;
    folio: number;
    ticket_folio: string;
    ticket_date: moment.Moment;
    quota: number;
    extra: number;
    status: number;
    scholarship: number;
    created_at: moment.Moment;
    updated_at: moment.Moment;
    qr: string;
    user: User;
    userValidate: User;
    workshopPeriod: WorkshopPeriod;
    captureline: Captureline;
    captureline_id: number;
    college_degree_option: boolean;
    constructor(item?, utc?) {
        if (item) {
            this.id = item.id;
            this.workshop_period_id = item.workshop_period_id;
            this.user_id = item.user_id;
            this.folio = item.folio;
            this.ticket_folio = item.ticket_folio;
            this.quota = item.quota;
            this.extra = item.extra ? item.extra : 0;
            this.status = item.status;
            this.qr = item.qr;
            this.captureline_id = item.captureline_id;
            this.scholarship = item.scholarship ? item.scholarship : 0;
            this.college_degree_option = item.college_degree_option;
            if ( utc === true) {
                if (item.ticket_date) {
                    this.ticket_date = moment(item.ticket_date + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                }
                this.created_at = moment(item.created_at + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                this.updated_at = moment(item.updated_at + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
            } else {
                if (item.ticket_date) {
                    this.ticket_date =  moment(item.ticket_date);
                }
                this.created_at =  moment(item.created_at);
                this.updated_at =  moment(item.updated_at);
            }
            if (item.user) {
                this.user = new User(item.user);
            }
            if(item.user_validate){
                this.userValidate = new User(item.user_validate);
            }
            if (item.workshop_period) {
                this.workshopPeriod = item.workshop_period;
            }
            if (item.captureline) {
                this.captureline = new Captureline(item.captureline);
            }
        }
    }
}
