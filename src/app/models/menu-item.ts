import { IMenuItem } from '../interfaces/menu-item';
export class MenuItem implements IMenuItem {
    name: string;
    link: string;
    queryParams: object;
    icon: string;
    children: MenuItem[];
    type: string;
    constructor(item?) {
        this.type = 'fa';
        if (item) {
            this.name = item.name;
            this.link = item.link;
            this.queryParams = item.queryParams || {};
            this.icon = item.icon;
            this.type = item.type || 'fa';
            if (item.children) {
                this.children = item.children.map(child => new MenuItem(child));
            } else {
                this.children = [];
            }
        }
    }
}
