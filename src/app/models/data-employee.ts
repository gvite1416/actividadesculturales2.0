import { IDataEmployee } from '../interfaces/data-employee';
import { Campus } from './campus';

export class DataEmployee implements IDataEmployee {
    id: number;
    campus_id: number;
    campus: Campus;
    workshift: string;
    area: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.campus_id = item.campus_id;
            this.workshift = item.workshift;
            this.area = item.area;
            this.campus = (item.campus) ? new Campus(item.campus) : new Campus();
        }
    }
}
