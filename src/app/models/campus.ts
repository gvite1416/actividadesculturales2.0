import { ICampus } from '../interfaces/campus';

export class Campus implements ICampus {
    id: number;
    name: string;
    code: string;
    deleted_at: string;
    college_degree_option: boolean;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            this.code = item.code;
            this.college_degree_option = item.college_degree_option;
            if (item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
