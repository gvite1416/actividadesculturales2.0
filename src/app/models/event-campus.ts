import { ICampus } from '../interfaces/campus';

export class EventCampus implements ICampus {
    id: number;
    name: string;
    code: string;
    pivot: {
        id: number;
        event_id: number;
        campus_id: number;
    };
    constructor (item?) {
        if (item) {
            this.id = +item.id;
            this.name = item.name;
            this.code = item.code;
            if (item.pivot) {
                this.pivot = {
                    id: item.pivot.id,
                    event_id: item.pivot.event_id,
                    campus_id: item.pivot.campus_id
                };
            }
        }
    }
}
