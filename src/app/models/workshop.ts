import { IWorkshop } from '../interfaces/workshop';
import { WorkshopPeriod } from './workshop-period';

export class Workshop implements IWorkshop {
    name: string;
    slug: string;
    objective: string;
    requirement: string;
    information: string;
    id: number;
    image: string;
    one_more_time: boolean;
    has_user: boolean;
    image_url: string;
    workshop_period?: WorkshopPeriod[];
    deleted_at: string;
    constructor(item? , utc?) {
        if (item) {
            this.name = item.name;
            this.slug = item.slug;
            this.objective = item.objective;
            this.requirement = item.requirement;
            this.information = item.information;
            this.image = item.image;
            this.id = item.id;
            this.one_more_time = item.one_more_time;
            this.image_url = item.image_url;
            this.has_user = item.has_user;
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
            this.deleted_at
            if (item.workshop_period) {
                this.workshop_period = item.workshop_period.map(wp => new WorkshopPeriod(wp, utc));
            }
        }
    }
}
