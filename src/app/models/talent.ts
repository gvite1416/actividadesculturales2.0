import { ITalent } from '../interfaces/talent';
import { Career } from './career';
import * as moment from 'moment';

export class Talent implements ITalent {
    id: number;
    name: string;
    career_id: number;
    semester: number;
    number_id: string;
    email: string;
    phone: string;
    arts: string;
    art_activity: string;
    band: string;
    gender: string;
    members: string;
    duration: number;
    celphone: string;
    equipment: string;
    scenography: string;
    audio_video: string;
    illumination: string;
    assembly_duration: number;
    deassembly_duration: number;
    career: Career;
    created_at: moment.Moment;
    constructor(item?, utc?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            this.career_id = item.career_id;
            this.semester = item.semester;
            this.number_id = item.number_id;
            this.email = item.email;
            this.phone = item.phone;
            this.arts = item.arts;
            this.art_activity = item.art_activity;
            this.band = item.band;
            this.gender = item.gender;
            this.members = item.members;
            this.duration = item.duration;
            this.celphone = item.celphone;
            this.equipment = item.equipment;
            this.scenography = item.scenography;
            this.audio_video = item.audio_video;
            this.illumination = item.illumination;
            this.assembly_duration = item.assembly_duration;
            this.deassembly_duration = item.deassembly_duration;
            this.career = new Career(item.career);
            const now = moment();
            if ( utc === true) {
                this.created_at =  moment(now.format('YYYY-MM-DD') + ' ' + item.created_at + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
            } else {
                this.created_at =  moment(now.format('YYYY-MM-DD') + ' ' + item.created_at);
            }
        }
    }
}
