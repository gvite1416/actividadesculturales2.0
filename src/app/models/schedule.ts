import * as moment from 'moment';
import { ISchedules } from '../interfaces/schedules';
export class Schedule implements ISchedules {
    id: number;
    workshop_period_id: number;
    day: number;
    start: moment.Moment;
    finish: moment.Moment;
    constructor(item?, utc?) {
        if (item) {
            this.id = item.id;
            this.workshop_period_id = item.workshop_period_id;
            this.day = item.day;
            const now = moment();
            if ( utc === true) {
                this.start =  moment(now.format('YYYY-MM-DD') + ' ' + item.start + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
                this.finish =  moment(now.format('YYYY-MM-DD') + ' ' + item.finish + ' +0000', 'YYYY-MM-DD HH:mm:ss Z');
            } else {
                this.start =  moment(now.format('YYYY-MM-DD') + ' ' + item.start);
                this.finish =  moment(now.format('YYYY-MM-DD') + ' ' + item.finish);
            }
        }
    }
}
