import { IRole } from '../interfaces/role';
import { Requirement } from './requirement';

export class Role implements IRole {
    id: number;
    name: string;
    slug: string;
    description: string;
    select: boolean;
    requirements?: Requirement[];
    constructor(item?) {
        if (item) {
            this.id = +item.id;
            this.name = item.name;
            this.slug = item.slug;
            this.description = item.description;
            if (item.requirements) {
                this.requirements = item.requirement.map(requirement => new Requirement(requirement));
            }
        }
    }
}
