import { IClassroom } from '../interfaces/classroom';

export class Classroom implements IClassroom {
    id: number;
    name: string;
    quota: number;
    deleted_at: string;
    constructor(item?) {
        if (item) {
            this.id = item.id;
            this.name = item.name;
            this.quota = item.quota;
            if(item.deleted_at){
                this.deleted_at = item.deleted_at;
            }
        }
    }
}
