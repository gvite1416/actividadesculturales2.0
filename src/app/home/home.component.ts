import { Component, OnInit, ViewChild } from '@angular/core';
import { SliderService } from '../services/slider.service';
import { Slider } from '../models/slider';
import { SwiperComponent } from '../material/swiper/swiper.component';
import { GLOBALS } from '../services/globals';
import { WorkshopService } from '../services/workshop.service';
import { Workshop } from '../models/workshop';
import { EventService } from '../services/event.service';
import { EventAC } from '../models/event';
import { Period } from '../models/period';
import { PeriodService } from '../services/period.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CountdownComponent } from 'ngx-countdown';
import * as moment from 'moment';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  sliders: Slider[];
  storageUrl = GLOBALS.storageUrl;
  workshops: Workshop[];
  events: EventAC[];
  currentPeriod: Period;
  isMobile: boolean;
  visible: boolean;
  private swiper: SwiperComponent;
  private swiperEvents: SwiperComponent;
  configCountdown: any;
  @ViewChild('counter', { static: false }) counter: CountdownComponent;
  constructor(
    private sliderService: SliderService,
    private workshopService: WorkshopService,
    private eventService: EventService,
    private periodService: PeriodService,
    private deviceService: DeviceDetectorService
  ) {
    this.workshops = [];
    this.sliders = [];
    this.events = [];
    this.isMobile = this.deviceService.isMobile();
    this.visible = false;
    this.configCountdown = {
      demand: true,
      leftTime: 0,
      template: '$!d!:$!h!:$!m!:$!s!'
    };
  }

  ngOnInit() {
    this.sliderService.getAll().subscribe(sliders => {
      this.sliders = sliders.filter(slide => !slide.deleted_at);
      if (this.sliders.length > 0) {
        setTimeout(() => {
          this.swiper.initSwiper();  
        }, 500);
      }
    });
    this.workshopService.getAllByCurrentPeriod().subscribe(workshops => {
      this.workshops = workshops.filter(w=>!w.deleted_at);
      this.visible = true;
    });
    this.eventService.getAll().subscribe(events => {
      // this.events = events;
      if (events.length > 0) {
        events.forEach((event) => {
          if (event.event_category_id !== 3) {
            this.events.push(event);
          }
        });
        if(this.events.length > 0){
          setTimeout(() => {
            this.swiperEvents.initSwiper();
          }, 500);
        }
      }
    });
    this.periodService.getCurrent().subscribe(period => {
      if (period) {
        this.currentPeriod = period;
        const now = moment();
        this.configCountdown.leftTime = period.inscription_start.diff(now, 'seconds');
        if (this.configCountdown.leftTime > 0) {
          setTimeout(() => {
            this.counter.begin();
          }, 0);
        }
      }
    });
  }
  getSwiper(swiper) {
    this.swiper = swiper;
  }
  getSwiperEvents(swiper) {
    this.swiperEvents = swiper;
  }

}
