import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Talent } from '../models/talent';
import { Router } from '@angular/router';
import { TalentService } from '../services/talent.service';
import { Career } from '../models/career';
import { CareerService } from '../services/career.service';
import { GLOBALS } from '../services/globals';

@Component({
  selector: 'app-talent',
  templateUrl: './talent.component.html',
  styleUrls: ['./talent.component.scss']
})
export class TalentComponent implements OnInit {
  createFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  talent: Talent;
  careers: Career[];
  arts: any[];
  urlPdf: string;
  token: string;
  constructor(
    public snackBar: MatSnackBar,
    private router: Router,
    private talentService: TalentService,
    private careerService: CareerService,
    private formBuilder: FormBuilder
  ) {
    this.careers = [];
    this.arts = [
      {name: 'Artes escénicas'},
      {name: 'Artes musicales'}
    ];
  }

  ngOnInit() {
    this.createForm();
    this.careerService.getAll().subscribe((careers) => {
      this.careers = careers;
    });
    this.urlPdf = GLOBALS.apiUrl + 'talent/pdf/';
  }

  private createForm() {
    this.createFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      career: new FormControl('', [Validators.required]),
      semester: new FormControl('', [Validators.required]),
      numberId: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', []),
      celphone: new FormControl('', [Validators.required]),
      arts: new FormControl('', [Validators.required]),
      artActivity: new FormControl('', []),
      band: new FormControl('', []),
      gender: new FormControl('', []),
      members: this.formBuilder.array([this.createMember()]),
      duration: new FormControl('', [Validators.required, Validators.min(30), Validators.max(60)]),
      equipments: this.formBuilder.array([this.createEquipment()]),
      scenography: new FormControl('', []),
      audioVideo: new FormControl('', []),
      illumination: new FormControl('', []),
      assemblyDuration: new FormControl('', [Validators.required]),
      deassemblyDuration: new FormControl('', [Validators.required]),
    });
  }
  private createMember(): FormGroup {
    return this.formBuilder.group({
      member: new FormControl('' , [])
    });
  }
  addMember() {
    const membersArray = this.createFormGroup.get('members') as FormArray;
    membersArray.push(this.createMember());
  }
  deleteMember(index) {
    const membersArray = this.createFormGroup.get('members') as FormArray;
    membersArray.removeAt(index);
  }
  private createEquipment(): FormGroup {
    return this.formBuilder.group({
      equipment: new FormControl('' , [])
    });
  }
  addEquipment() {
    const equipmentsArray = this.createFormGroup.get('equipments') as FormArray;
    equipmentsArray.push(this.createEquipment());
  }
  deleteEquipment(index) {
    const equipmentsArray = this.createFormGroup.get('equipments') as FormArray;
    equipmentsArray.removeAt(index);
  }
  add() {
    const equipmentArray =  this.createFormGroup.get('equipments').value;
    const equipmentsA = [];
    equipmentArray.forEach((e) => {
      equipmentsA.push(e.equipment);
    });
    const membersArray =  this.createFormGroup.get('members').value;
    const membersA = [];
    membersArray.forEach((e) => {
      membersA.push(e.member);
    });
    this.talent = new Talent();
    this.talent.name = this.createFormGroup.value.name;
    this.talent.career_id =  this.createFormGroup.value.career;
    this.talent.semester =  this.createFormGroup.value.semester;
    this.talent.number_id =  this.createFormGroup.value.numberId;
    this.talent.email =  this.createFormGroup.value.email;
    this.talent.phone =  this.createFormGroup.value.phone;
    this.talent.celphone =  this.createFormGroup.value.celphone;
    this.talent.arts =  this.createFormGroup.value.arts;
    this.talent.art_activity =  this.createFormGroup.value.artActivity;
    this.talent.band =  this.createFormGroup.value.band;
    this.talent.gender =  this.createFormGroup.value.gender;
    this.talent.members =  membersA.join(',');
    this.talent.duration =  this.createFormGroup.value.duration;
    this.talent.equipment =  equipmentsA.join(',');
    this.talent.scenography =  this.createFormGroup.value.scenography;
    this.talent.audio_video =  this.createFormGroup.value.audioVideo;
    this.talent.illumination =  this.createFormGroup.value.illumination;
    this.talent.assembly_duration =  this.createFormGroup.value.assemblyDuration;
    this.talent.deassembly_duration =  this.createFormGroup.value.deassemblyDuration;
    this.talentService.create(this.talent).subscribe((response) => {
      if (response.success) {
        this.token = response.token;
      } else {
        for (const index in response.error) {
          if (index) {
            const errors = response.error[index];
            errors.forEach((error) => {
              this.snackBar.open(error, '', {
                duration: 3000,
                verticalPosition: this.positionVertical
              });
            });
          }
        }
      }
    });
  }

}
