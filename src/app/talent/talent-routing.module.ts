import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TalentComponent } from './talent.component';
import { AnnouncementComponent } from './announcement/announcement.component';

const routes: Routes = [
  {
    path: '',
    component: TalentComponent
  },
  {
    path: 'convocatoria',
    component: AnnouncementComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TalentRoutingModule { }
