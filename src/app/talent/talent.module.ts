import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TalentRoutingModule } from './talent-routing.module';
import { TalentComponent } from './talent.component';
import { AnnouncementComponent } from './announcement/announcement.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { CareerService } from '../services/career.service';
import { TalentService } from '../services/talent.service';

@NgModule({
  imports: [
    CommonModule,
    TalentRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [TalentComponent, AnnouncementComponent],
  providers: [CareerService, TalentService]
})
export class TalentModule { }
