import * as moment from 'moment';
export class FormatDateHelper {
    constructor(){}
    public getHourNum (date: string, format?: string) : number{
        format = format || 'MM-DD-YYYY HH:mm';
        let day = moment(date, format);
        return day.hour();
    }
    public getMinNum (date: string, format?: string) : number{
        format = format || 'MM-DD-YYYY HH:mm';
        let day = moment(date, format);
        return day.minutes();
    }
    public getHourFormat(date: string, format: string): string {
        let day = moment(date, 'HHmm');
        return day.format(format);
    }
}