import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';
import { VerifyComponent } from './verify/verify.component';
import { ThankYouPageComponent } from './thank-you-page/thank-you-page.component';


const routes: Routes = [
  {
    path: '',
    component: RegisterComponent
  },
  {
    path: 'verifica-usuario/:code',
    component: VerifyComponent
  },
  {
    path: 'gracias',
    component: ThankYouPageComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }

export const RoutableComponents = [
  RegisterComponent
];
