import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ModalLoadComponent } from '../../material/modal-load/modal-load.component';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
  private code: string;
  public showResponce: boolean;
  public response;
  public dialogRef: any;
  constructor(private route: ActivatedRoute, public dialog: MatDialog, private userService: UserService) {
    this.showResponce = false;
    this.response = {
      success: false,
      message: ''
    };
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.code = params['code'];
      this.userService.verifyUser(this.code).subscribe( response => {
        this.showResponce = true;
        this.response = response;
        this.dialogRef.close();
      });
    });
    setTimeout(() => {
      this.dialogRef = this.dialog.open(ModalLoadComponent, {panelClass: 'load-modal', disableClose: true});
    });
    /*dialogRef.afterOpen().subscribe(() => {
      dialogRef.componentInstance.updateSwiper();
    });*/
  }

}
