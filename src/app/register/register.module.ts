import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule, RoutableComponents } from './register-routing.module';

import { MaterialModule } from '../material/material.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { VerifyComponent } from './verify/verify.component';
import { ThankYouPageComponent } from './thank-you-page/thank-you-page.component';
import { CampusService } from '../services/campus.service';
import { CareerService } from '../services/career.service';

@NgModule({
  declarations: [
    RoutableComponents,
    VerifyComponent,
    ThankYouPageComponent
  ],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  providers: [CampusService, CareerService]
})
export class RegisterModule { }
