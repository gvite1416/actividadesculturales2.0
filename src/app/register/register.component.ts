import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { CampusService } from '../services/campus.service';
import { Campus } from '../models/campus';
import { CareerService } from '../services/career.service';
import { Career } from '../models/career';
import { Occupation } from '../models/occupation';
import { OccupationService } from '../services/occupation.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user: User;
  loginForm: FormGroup;
  dataStudentForm: FormGroup;
  dataEmployeeForm: FormGroup;
  dataExternalForm: FormGroup;
  dataExstudentForm: FormGroup;
  step: number;
  userTypes: object[];
  campus: Campus[];
  careers: Career[];
  occupations: Occupation[];
  genders: any;
  systems: any;
  disableForm: boolean;
  @ViewChild('tabGroup', { static: false }) tabGroup;

  constructor(
    private userService: UserService,
    private campusService: CampusService,
    private careerService: CareerService,
    private occupationService: OccupationService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {
    this.step = 1;
    this.userTypes = [
      {value: 'student', name: 'Alumno'},
      {value: 'exstudent', name: 'Exalumno'},
      {value: 'employee', name: 'Trabajador UNAM'},
      {value: 'external', name: 'Externo'}
    ];
    this.user = new User({student_type: 'exstudent'});
    this.campusService.getAll().subscribe(campus => {
      this.campus = campus;
    });
    this.careerService.getAll().subscribe(careers => {
      this.careers = careers;
    });
    this.occupationService.getAll().subscribe( occupations => {
      this.occupations = occupations;
    });
    this.genders = [];
    this.genders.push({id: 1, name: 'Masculino'});
    this.genders.push({id: 2, name: 'Femenino'});
    this.genders.push({id: 3, name: 'Otro'});
    this.systems = [];
    this.systems.push({id: 'ESC', name: 'Escolarizado'});
    this.systems.push({id: 'SUA', name: 'SUA'});
    this.disableForm = false;
  }

  ngOnInit() {
    this.createFormLogin();
    this.createFormData();
  }

  private createFormLogin() {
    this.loginForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      firstname: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      surname: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.required, Validators.email], checkEmailValidator.bind(this)),
      confirmEmail: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
      studentType: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      genderOther: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
    }, passwordMatchValidator);
    function passwordMatchValidator(g: FormGroup) {
      let response = null;
      if (g.controls.password.value === g.controls.confirmPassword.value && g.controls.password.value !== '') {
        g.controls.password.setErrors(null);
        g.controls.confirmPassword.setErrors(null);
      } else {
        g.controls.password.setErrors({'passwordMatch': true});
        g.controls.confirmPassword.setErrors({'passwordMatch': true});
        response = {'mismatch': true};
      }
      if (g.controls.email.value === g.controls.confirmEmail.value && g.controls.email.value !== '') {
        g.controls.email.setErrors(null);
        g.controls.confirmEmail.setErrors(null);
      } else {
        g.controls.email.setErrors({'emailMatch': true});
        g.controls.confirmEmail.setErrors({'emailMatch': true});
        response = {'mismatch': true};
      }
      return response;
    }
    function checkEmailValidator(control: AbstractControl) {
      return this.userService.checkEmail(control.value);
    }
  }
  private createFormData() {
    this.dataStudentForm = new FormGroup({
      phone: new FormControl('', []),
      celphone: new FormControl('', [Validators.required]),
      numberId: new FormControl('', [Validators.required], checkNumberIdValidator.bind(this)),
      year: new FormControl('', [Validators.required, Validators.min(1900)]),
      semester: new FormControl('', [Validators.required, Validators.min(1)]),
      campus: new FormControl('', [Validators.required]),
      career: new FormControl('', [Validators.required]),
      system: new FormControl('', [Validators.required])
    });
    this.dataExstudentForm = new FormGroup({
      phone: new FormControl('', []),
      celphone: new FormControl('', [Validators.required]),
      numberId: new FormControl('', [Validators.required], checkNumberIdValidator.bind(this)),
      year: new FormControl('', [Validators.required, Validators.min(1900)]),
      campus: new FormControl('', [Validators.required]),
      career: new FormControl('', [Validators.required]),
      system: new FormControl('', [Validators.required])
    });
    this.dataEmployeeForm = new FormGroup({
      phone: new FormControl('', [Validators.required]),
      workshift: new FormControl('', [Validators.required]),
      area: new FormControl('', [Validators.required]),
      campus: new FormControl('', [Validators.required]),
      celphone: new FormControl('', [Validators.required]),
      numberId: new FormControl('', [Validators.required], checkNumberIdValidator.bind(this))
    });
    this.dataExternalForm = new FormGroup({
      phone: new FormControl('', [Validators.required]),
      celphone: new FormControl('', [Validators.required]),
      occupation: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required])
    });
    function checkNumberIdValidator(control: AbstractControl) {
      if (control.value.length >= 5) {
        return this.userService.checkNumberId(control.value);
      }
    }

  }

  onSubmitStudent() {
    this.user.phone = +this.dataStudentForm.value.phone;
    this.user.celphone = +this.dataStudentForm.value.celphone;
    this.user.number_id = +this.dataStudentForm.value.numberId;
    this.user.year = +this.dataStudentForm.value.year;
    this.user.career_id = +this.dataStudentForm.value.career;
    this.user.campus_id = +this.dataStudentForm.value.campus;
    this.user.semester = this.dataStudentForm.value.semester;
    this.user.system = this.dataStudentForm.value.system;
    this.registerUser();
  }
  onSubmitExstudent() {
    this.user.phone = +this.dataExstudentForm.value.phone;
    this.user.celphone = +this.dataExstudentForm.value.celphone;
    this.user.number_id = +this.dataExstudentForm.value.numberId;
    this.user.year = +this.dataExstudentForm.value.year;
    this.user.career_id = +this.dataExstudentForm.value.career;
    this.user.campus_id = +this.dataExstudentForm.value.campus;
    this.user.system = this.dataExstudentForm.value.system;
    this.registerUser();
  }
  onSubmitEmployee() {
    this.user.phone = +this.dataEmployeeForm.value.phone;
    this.user.celphone = +this.dataEmployeeForm.value.celphone;
    this.user.workshift = this.dataEmployeeForm.value.workshift;
    this.user.area = this.dataEmployeeForm.value.area;
    this.user.campus_id = +this.dataEmployeeForm.value.campus;
    this.user.number_id = +this.dataEmployeeForm.value.numberId;
    this.registerUser();
  }
  onSubmitExternal() {
    this.user.phone = +this.dataExternalForm.value.phone;
    this.user.celphone = +this.dataExternalForm.value.celphone;
    this.user.occupation_id = +this.dataExternalForm.value.occupation;
    this.user.address = this.dataExternalForm.value.address;
    this.registerUser();
  }
  private registerUser() {
    this.disableForm = true;
    this.userService.registerUser(this.user).subscribe(response => {
        this.router.navigate(['/registro/gracias/']);
    }, response => {
      if (response.status == 400){
        for (const index in response.error.message) {
          if (index) {
            response.error.message[index].forEach(error => {
              this.snackBar.open(error, '', {
                duration: 3000,
              });
            });
          }
        }
      }
      this.disableForm = false;
    });
  }
  nextForm() {
    this.user.name = this.loginForm.value.name;
    this.user.lastname = this.loginForm.value.firstname;
    this.user.surname = this.loginForm.value.surname;
    this.user.email = this.loginForm.value.email;
    this.user.email_confirmation = this.loginForm.value.confirmEmail;
    this.user.password = this.loginForm.value.password;
    this.user.password_confirmation = this.loginForm.value.confirmPassword;
    this.user.student_type = this.loginForm.value.studentType;
    this.user.gender = (this.loginForm.value.gender === 'Masculino' ||
    this.loginForm.value.gender === 'Femenino') ? this.loginForm.value.gender : this.loginForm.value.genderOther;
    this.tabGroup.selectedIndex = 1;
  }
  beforeForm() {
    this.tabGroup.selectedIndex = 0;
  }

}
