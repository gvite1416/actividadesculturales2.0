import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './events.component';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent
  },
  {
    path: 'conciertos',
    loadChildren: 'app/events/concerts/concerts.module#ConcertsModule'
  },
  {
    path: 'ofrendas',
    loadChildren: 'app/events/offerings/offerings.module#OfferingsModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
