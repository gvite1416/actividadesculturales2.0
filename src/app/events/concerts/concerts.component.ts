import { Component, OnInit } from '@angular/core';
import { EventAC } from '../../models/event';
import { GLOBALS } from '../../services/globals';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'app-concerts',
  templateUrl: './concerts.component.html',
  styleUrls: ['./concerts.component.scss']
})
export class ConcertsComponent implements OnInit {
  events: EventAC[];
  storageUrl: string = GLOBALS.storageUrl;
  constructor(private eventService: EventService) {
    this.events = [];
  }

  ngOnInit() {
    this.eventService.getByCategory(1).subscribe(events => {
      this.events = events;
    });
  }

}
