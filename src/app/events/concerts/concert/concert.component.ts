import { Component, OnInit } from '@angular/core';
import { EventService } from '../../../services/event.service';
import { EventAC } from '../../../models/event';
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBALS } from '../../../services/globals';
import { Classroom } from '../../../models/classroom';
import { Showing } from '../../../models/showing';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-concert',
  templateUrl: './concert.component.html',
  styleUrls: ['./concert.component.scss']
})
export class ConcertComponent implements OnInit {
  event: EventAC;
  disableAllBtn: boolean;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  storageUrl: string = GLOBALS.storageUrl;
  urlPdf: string;
  token: string;
  apiUrl: string;
  constructor(
    private eventService: EventService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {
    this.event = new EventAC();
    this.event.classroom = new Classroom();
    this.urlPdf = '';
    this.token = localStorage.getItem('token');
    this.apiUrl = GLOBALS.apiUrl;
    this.disableAllBtn = false;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.eventService.getBySlug(params['slug']).subscribe(event => {
        this.event = event;
        // this.urlPdf = GLOBALS.apiUrl + 'inscription/inscribe-pdf/' + params['id'] + '/?token=';
      });
    });
  }
  inscribe(showing: Showing) {
    this.disableAllBtn = true;
    this.eventService.inscribe(showing).subscribe(response => {
        this.snackBar.open('Se ha inscrito al evento correctamente', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        window.location.reload();
      this.disableAllBtn = false;
    },error => {
      this.snackBar.open('Ocurrió un error: ' + error.message, '', {
        duration: 3000,
        verticalPosition: this.positionVertical
      });
    });
  }

}
