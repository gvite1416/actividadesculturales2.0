import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConcertsRoutingModule } from './concerts-routing.module';
import { ConcertsComponent } from './concerts.component';
import { MaterialModule } from '../../material/material.module';
import { ConcertComponent } from './concert/concert.component';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    ConcertsRoutingModule,
    MaterialModule,
    PipesModule
  ],
  declarations: [ConcertsComponent, ConcertComponent]
})
export class ConcertsModule { }
