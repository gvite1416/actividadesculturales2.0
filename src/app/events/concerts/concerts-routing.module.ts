import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConcertsComponent } from './concerts.component';
import { ConcertComponent } from './concert/concert.component';

const routes: Routes = [
  {
    path: '',
    component: ConcertsComponent
  },
  {
    path: ':slug',
    component: ConcertComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConcertsRoutingModule { }
