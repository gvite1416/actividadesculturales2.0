import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import { EventAC } from '../models/event';
import { GLOBALS } from '../services/globals';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  events: EventAC[];
  storageUrl: string = GLOBALS.storageUrl;
  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.eventService.getAll().subscribe(events => {
      this.events = events;
    });
  }

}
