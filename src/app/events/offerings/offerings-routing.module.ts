import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfferingsComponent } from './offerings.component';
import { OfferingComponent } from './offering/offering.component';

const routes: Routes = [
  {
    path: '',
    component: OfferingsComponent
  },
  {
    path: ':slug',
    component: OfferingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferingsRoutingModule { }
