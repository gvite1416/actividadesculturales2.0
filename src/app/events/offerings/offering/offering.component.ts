import { Component, OnInit } from '@angular/core';
import { EventAC } from '../../../models/event';
import { EventService } from '../../../services/event.service';
import { ActivatedRoute } from '@angular/router';
import { GLOBALS } from '../../../services/globals';

@Component({
  selector: 'app-offering',
  templateUrl: './offering.component.html',
  styleUrls: ['./offering.component.scss']
})
export class OfferingComponent implements OnInit {
  event: EventAC;
  storageUrl: string = GLOBALS.storageUrl;
  constructor(
    private eventService: EventService,
    private route: ActivatedRoute
  ) {
    this.event = new EventAC();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.eventService.getOfferingBySlug(params['slug']).subscribe(event => {
        this.event = event;
      });
    });
  }

}
