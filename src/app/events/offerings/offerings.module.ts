import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferingsRoutingModule } from './offerings-routing.module';
import { OfferingsComponent } from './offerings.component';
import { MaterialModule } from '../../material/material.module';
import { OfferingComponent } from './offering/offering.component';

@NgModule({
  imports: [
    CommonModule,
    OfferingsRoutingModule,
    MaterialModule
  ],
  declarations: [OfferingsComponent, OfferingComponent]
})
export class OfferingsModule { }
