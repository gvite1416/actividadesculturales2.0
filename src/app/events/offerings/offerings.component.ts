import { Component, OnInit } from '@angular/core';
import { EventAC } from '../../models/event';
import { GLOBALS } from '../../services/globals';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'app-offerings',
  templateUrl: './offerings.component.html',
  styleUrls: ['./offerings.component.scss']
})
export class OfferingsComponent implements OnInit {
  events: EventAC[];
  storageUrl: string = GLOBALS.storageUrl;
  constructor(private eventService: EventService) {
    this.events = [];
  }

  ngOnInit() {
    this.eventService.getByCategory(2).subscribe(events => {
      this.events = events;
    });
  }

}
