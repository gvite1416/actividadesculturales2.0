import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { Campus } from '../../models/campus';
import { Career } from '../../models/career';
import { Occupation } from '../../models/occupation';
import { UserService } from '../../services/user.service';
import { CampusService } from '../../services/campus.service';
import { CareerService } from '../../services/career.service';
import { OccupationService } from '../../services/occupation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {

  user: User;
  editFormGroup: FormGroup;
  dataEmployeeForm: FormGroup;
  dataExternalForm: FormGroup;
  passFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  campus: Campus[];
  careers: Career[];
  occupations: Occupation[];
  genders: any;
  constructor(
    private userService: UserService,
    public snackBar: MatSnackBar,
    private campusService: CampusService,
    private careerService: CareerService,
    private occupationService: OccupationService,
    private router: Router
  ) {
    this.user = new User();
    this.genders = [];
    this.genders.push({id: 1, name: 'Masculino'});
    this.genders.push({id: 2, name: 'Femenino'});
    this.genders.push({id: 3, name: 'Otro'});
  }

  ngOnInit() {
    this.userService.getActive().subscribe(user => {
        this.user = user;
        this.editFormGroup.setValue({
          name: this.user.name,
          lastname: this.user.lastname,
          surname: this.user.surname,
          email: this.user.email,
          phone: this.user.phone,
          celphone: this.user.celphone,
          gender: (this.user.gender === 'Masculino' || this.user.gender === 'Femenino') ? this.user.gender : 'Otro',
          genderOther: (!(this.user.gender === 'Masculino' || this.user.gender === 'Femenino')) ? this.user.gender : ''
        });
        if (this.user.data_external) {
          this.dataExternalForm.setValue({
            occupation: this.user.data_external.occupation_id,
            address: this.user.data_external.address
          });
        }
        if (this.user.data_employee) {
          this.dataEmployeeForm.setValue({
            workshift: this.user.data_employee.workshift,
            area: this.user.data_employee.area,
            campus: this.user.data_employee.campus_id,
            numberId: this.user.number_id,
          });
        }
    });
    this.campusService.getAll().subscribe(campus => {
      this.campus = campus;
    });
    this.careerService.getAll().subscribe(careers => {
      this.careers = careers;
    });
    this.occupationService.getAll().subscribe( occupations => {
      this.occupations = occupations;
    });
    this.createForm();
    this.createFormData();
    this.createFormPassword();
  }

  private createForm() {
    this.editFormGroup = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      lastname: new FormControl('', [Validators.required, Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      surname: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      email: new FormControl('', [Validators.required, Validators.email], checkEmailValidator.bind(this)),
      gender: new FormControl('', [Validators.required]),
      genderOther: new FormControl('', [Validators.pattern(/[a-zA-Z áéíóúüñÁÉÍÓÚÜÑ]/g)]),
      phone: new FormControl('', []),
      celphone: new FormControl('', []),
    });
    function checkEmailValidator(control: AbstractControl) {
      return this.userService.checkEmail(control.value, this.user.id);
    }
  }
  private createFormPassword() {
    this.passFormGroup = new FormGroup({
      currentPassword: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
    }, passwordMatchValidator);
    function passwordMatchValidator(g: FormGroup) {
      if (g.controls.password.value === g.controls.confirmPassword.value && g.controls.password.value !== '') {
        g.controls.password.setErrors(null);
        g.controls.confirmPassword.setErrors(null);
        return null;
      } else {
        g.controls.password.setErrors({'passwordMatch': true});
        g.controls.confirmPassword.setErrors({'passwordMatch': true});
        return {'mismatch': true};
      }
    }
  }

  private createFormData() {
    this.dataEmployeeForm = new FormGroup({
      workshift: new FormControl('', [Validators.required]),
      area: new FormControl('', [Validators.required]),
      campus: new FormControl('', [Validators.required]),
      numberId: new FormControl('', [Validators.required], checkNumberIdValidator.bind(this))
    });
    this.dataExternalForm = new FormGroup({
      occupation: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required])
    });
    function checkNumberIdValidator(control: AbstractControl) {
      if (control.value.length >= 5) {
        return this.userService.checkNumberId(control.value, this.user.id);
      }
    }

  }
  updatePass() {
    this.userService.updatePass({
      password_current: this.passFormGroup.value.currentPassword,
      password: this.passFormGroup.value.password,
      password_confirmation: this.passFormGroup.value.confirmPassword
    }).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Contraseña actualizada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      } else {
        this.snackBar.open(response.message, '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }
  update() {
    this.user.name = this.editFormGroup.value.name ;
    this.user.lastname = this.editFormGroup.value.lastname ;
    this.user.surname = this.editFormGroup.value.surname ;
    this.user.email = this.editFormGroup.value.email ;
    this.user.phone = +this.editFormGroup.value.phone;
    this.user.celphone = +this.editFormGroup.value.celphone;
    this.user.gender = (this.editFormGroup.value.gender === 'Masculino' ||
      this.editFormGroup.value.gender === 'Femenino') ? this.editFormGroup.value.gender : this.editFormGroup.value.genderOther;
    this.userService.updateProfile(this.user).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Perfil actualizado', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
        this.router.navigate(['/perfil']);
      } else {
        this.snackBar.open('Ocurrió un error', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }
  onSubmitExternal() {
    this.user.data_external.occupation_id = +this.dataExternalForm.value.occupation;
    this.user.data_external.address = this.dataExternalForm.value.address;
    this.update();
  }

  onSubmitEmployee() {
    this.user.data_employee.campus_id = +this.dataEmployeeForm.value.campus;
    this.user.data_employee.area = this.dataEmployeeForm.value.area;
    this.user.data_employee.workshift = this.dataEmployeeForm.value.workshift;
    this.user.number_id = +this.dataEmployeeForm.value.numberId;
    this.update();
  }

}
