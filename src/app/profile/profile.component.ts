import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { PhotoDialogComponent } from '../dialogs/photo-dialog/photo-dialog.component';
import { MatDialog, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { GLOBALS } from '../services/globals';
import { Role } from '../models/role';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;
  dataSource: any[];
  positionVertical: MatSnackBarVerticalPosition = 'top';
  storageUrl: string = GLOBALS.storageUrl;
  urlPdf: string;
  token: string;
  constructor(
    private userService: UserService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.user = new User();
    this.user.role = new Role();
    this.dataSource = [];
    this.urlPdf = '';
    this.token = localStorage.getItem('token');
  }

  ngOnInit() {
    this.userService.getActive().subscribe(user => {
      /*this.userService.generateQr().subscribe(() => {
        this.user.id = this.user.id;
      });*/
      this.user = user;
      this.dataSource = [
        { fieldName: 'Correo', valueName: this.user.email },
        { fieldName: 'Teléfono', valueName: this.user.phone },
        { fieldName: 'Celular', valueName: this.user.celphone }
      ];
      switch (this.user.student_type) {
        case 'student':
          this.dataSource.push({ fieldName: 'No. Cta.', valueName: this.user.number_id });
          this.dataSource.push({ fieldName: 'Facultad', valueName: this.user.data_student.campus.name });
          this.dataSource.push({ fieldName: 'Carrera', valueName: this.user.data_student.career.name });
          this.dataSource.push({ fieldName: 'Semestre', valueName: this.user.data_student.semester });
          this.dataSource.push({ fieldName: 'Año de ingreso', valueName: this.user.data_student.year });
          break;
        case 'exstudent':
          this.dataSource.push({ fieldName: 'No. Cta.', valueName: this.user.number_id });
          this.dataSource.push({ fieldName: 'Facultad', valueName: this.user.data_student.campus.name });
          this.dataSource.push({ fieldName: 'Carrera', valueName: this.user.data_student.career.name });
          this.dataSource.push({ fieldName: 'Semestre', valueName: this.user.data_student.semester });
          this.dataSource.push({ fieldName: 'Año de egreso', valueName: this.user.data_student.year });
          break;
        case 'employee':
          this.dataSource.push({ fieldName: 'No. Trabajador', valueName: this.user.number_id });
          this.dataSource.push({ fieldName: 'Facultad', valueName: this.user.data_employee.campus.name });
          this.dataSource.push({ fieldName: 'Turno', valueName: this.user.data_employee.workshift });
          this.dataSource.push({ fieldName: 'Área', valueName: this.user.data_employee.area });
          break;
        case 'external':
          this.dataSource.push({ fieldName: 'Dirección', valueName: this.user.data_external.address });
          this.dataSource.push({ fieldName: 'Ocupación', valueName: this.user.data_external.occupation.name });
          break;
      }
      this.urlPdf = GLOBALS.apiUrl + 'users/profile-id-card/?token=' + localStorage.getItem('token');
    });
  }

  openModalImage() {
    const dialogRef = this.dialog.open(PhotoDialogComponent, {
      minHeight: '700px',
      width: '800px',
      data: {user: this.user , type: 'campus'}
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      if (result !== undefined) {
        this.userService.savePhoto(result).subscribe((response: any) => {
          this.user.photo_url = response.path;
        });
      }
    });
  }

}
