import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile-password',
  templateUrl: './profile-password.component.html',
  styleUrls: ['./profile-password.component.scss']
})
export class ProfilePasswordComponent implements OnInit {
  passFormGroup: FormGroup;
  positionVertical: MatSnackBarVerticalPosition = 'top';
  constructor(
    private userService: UserService,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.createFormPassword();
  }

  private createFormPassword() {
    this.passFormGroup = new FormGroup({
      currentPassword: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
    }, passwordMatchValidator);
    function passwordMatchValidator(g: FormGroup) {
      if (g.controls.password.value === g.controls.confirmPassword.value && g.controls.password.value !== '') {
        g.controls.password.setErrors(null);
        g.controls.confirmPassword.setErrors(null);
        return null;
      } else {
        g.controls.password.setErrors({'passwordMatch': true});
        g.controls.confirmPassword.setErrors({'passwordMatch': true});
        return {'mismatch': true};
      }
    }
  }

  updatePass() {
    this.userService.updatePass({
      password_current: this.passFormGroup.value.currentPassword,
      password: this.passFormGroup.value.password,
      password_confirmation: this.passFormGroup.value.confirmPassword
    }).subscribe( (response) => {
      if ( response.success ) {
        this.snackBar.open('Contraseña actualizada', '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      } else {
        this.snackBar.open(response.message, '', {
          duration: 3000,
          verticalPosition: this.positionVertical
        });
      }
    });
  }

}
