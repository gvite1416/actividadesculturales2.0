var gulp = require('gulp'),
  consolidate = require('gulp-consolidate'),
  iconfont = require('gulp-iconfont');
var runTimestamp = Math.round(Date.now()/1000);
gulp.task('iconfont', function () {
  return gulp.src('gulp/svg/*.svg')
    .pipe(iconfont({
      fontName: 'iconfontactcul',
      formats: ['ttf', 'eot', 'woff', 'woff2'],
      appendCodepoints: true,
      appendUnicode: false,
      normalize: true,
      fontHeight: 3000,
      centerHorizontally: true,
      timestamp: runTimestamp,
    }))
    .on('glyphs', function (glyphs, options) {
        console.log(options);
        gulp.src('gulp/template/iconfont.css')
            .pipe(consolidate('underscore', {
                glyphs: glyphs,
                fontName: options.fontName,
                fontDate: new Date().getTime()
            }))
            .pipe(gulp.dest('src/assets/fonts/icons'));
        gulp.src('gulp/template/index.html')
            .pipe(consolidate('underscore', {
                glyphs: glyphs,
                fontName: options.fontName
            }))
            .pipe(gulp.dest('src/assets/fonts/icons'));
    })
    .pipe(gulp.dest('src/assets/fonts/icons'));
});