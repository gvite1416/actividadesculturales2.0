# ActividadesCulturales2.0

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.25.

## Pre requirements

Install nvm to switch between version of node

- Node: 12.18.0
- Npm: 6.14.5
- Gulp: 4.0.2

## Development server

Run `npm i`
Run `gulp iconfont`
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --prod --delete-output-path=false` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Url API REST and resources

To change the configuration of the url to the API go to the file `/src/app/services/globals.ts` 